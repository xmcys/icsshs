//
//  销售分析数据库操作表
//  SalesAnalyseDatabase.h
//  icss-hs
//
//  Created by hsit on 11-10-11.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SalesAnalyseDatabase : NSObject {
    
}

//  创建销售分析表
- (BOOL)createTable;

//  保存数据
//  array : SalesAnalyse对象数组  date : 最后一次查询周期
- (BOOL)saveData:(NSArray *)array searchDate:(NSString *)date;

//  删除所有数据
- (BOOL)deleteAllData;

//  查询所有数据 
//  返回包含一条brandId为“最后一次查询周期”的记录
- (NSMutableArray *)queryAllData;

@end
