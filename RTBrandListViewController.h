//
//  卷烟列表视图控制器
//  RTBrandListViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RefreshHeaderView;   //  下拉刷新视图类
@class LoadMoreFooterView;  //  上拉加载视图类
@class SaleAnalyse;         //  销售分析类

//  自定义卷烟列表协议
@protocol RTBrandListViewDelegate <NSObject>

@optional

//  更改导航栏右侧查询按钮的文字和状态
- (void)changeNavigationRightBarButtonState:(NSString *)title style:(UIBarButtonItemStyle)style;

@end

@interface RTBrandListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIScrollViewDelegate, NSXMLParserDelegate> {
    NSMutableData      * brandListData;    //  卷烟列表网络数据
    NSMutableArray     * brandListArray;   //  卷烟列表
    NSURLConnection    * connection;       //  网络连接对象
    RefreshHeaderView  * headerView;       //  下拉刷新视图
    LoadMoreFooterView * footerView;       //  上拉加载视图
    SaleAnalyse        * saleAnalyse;      //  销售分析对象
    NSString           * aValue;           //  临时字符串对象，解析xml用
    
    BOOL isLoading;        //  判断是否正在加载
    BOOL isOverLoad;       //  判断是刷新还是加载更多
    int  loadCounts;       //  记录每次实际加载的数据量
    int  startPage;        //  开始记录位置
    int  pageSize;         //  每次最大加载记录数
    int  connectionTimes;  //  连接次数
}


@property (nonatomic, retain) IBOutlet UIActivityIndicatorView  * spinner;     //  加载指示器
@property (nonatomic, assign) id<RTBrandListViewDelegate>    delegate;         //  卷烟列表协议委托
@property (nonatomic, retain) IBOutlet UITableView         * brandListTable;   //  卷烟列表表视图
@property (nonatomic, retain) IBOutlet UISearchBar         * searchBar;        //  卷烟名称搜索栏
@property (nonatomic, retain) IBOutlet UILabel             * timeLabel;        //  查询时间标签

//  显示或隐藏搜索栏
- (void)showOrHideSerachBar;

//  关闭键盘
- (void)closeKeyboard;

//  加载卷烟列表
- (void)loadBrandListData;

//  取消加载
- (void)cancelLoading;

//  刷新表视图
- (void)reloadTable;

//  解析XML数据
- (void)parseXMLData;

@end
