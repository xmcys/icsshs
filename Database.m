//
//  通用数据库操作类
//  Database.m
//  icss-hs
//
//  Created by hsit on 11-10-11.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "Database.h"
#import "GlobalUtil.h"


@implementation Database

//  关闭数据库
- (void)closeDatabase{
    if (database != nil) {
        sqlite3_close(database);
    }
    NSLog(@"数据库已关闭");
}

//  打开数据库
- (BOOL)openDatabase{
    if (database == nil) {
        NSLog(@"连接数据库 》》》");
        if (sqlite3_open([[GlobalUtil getApplicationFilePath] UTF8String], &database) != SQLITE_OK){
            sqlite3_close(database);
            NSLog(@"数据库连接失败.");
            database = nil;
            return NO;
        }
    }
    NSLog(@"数据库已连接.");
    return YES;
}

//  创建表格
//  createSql : 相应表格的创建语句 
//  tableNameWithCHN : 相应表格的名称 （用于NSLog跟踪显示）
- (BOOL)createTable:(NSString *)createSql tableNameWithCHN:(NSString *)name{
    NSLog(@"%@", [NSString stringWithFormat:@"创建 %@ 表 》》》", name]);
    if (database != nil) {
        //  errMsg 用于显示错误信息
        char *errMsg;
        if (sqlite3_exec(database, [createSql UTF8String], NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"%@", [NSString stringWithFormat:@"%@ 表创建失败 : %s", name, errMsg]);
            return NO;
        }
        NSLog(@"%@", [NSString stringWithFormat:@"%@ 表创建成功 / 已存在", name]);
        return YES;
    }
    NSLog(@"%@", [NSString stringWithFormat:@"创建 %@ 表失败 ：数据连接失败。", name]);
    return NO;
}

//  保存数据
//  saveSql : 相应表格的保存语句
//  tableNameWithCHN : 相应表格的名称 （用于NSLog跟踪显示）
- (BOOL)saveData:(NSString *)saveSql tableNameWithCHN:(NSString *)name{
    NSLog(@"%@", [NSString stringWithFormat:@"添加 %@ 表中数据 》》》", name]);
    if (database != nil) {
        //  errMsg 用于显示错误信息
        char *errMsg;
        if (sqlite3_exec(database, [saveSql UTF8String], NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"%@", [NSString stringWithFormat:@"添加 %@ 表中数据失败 : %s", name, errMsg]);
            return NO;
        }
        NSLog(@"%@", [NSString stringWithFormat:@"添加 %@ 表中数据成功.", name]);
        return YES;
    }
    NSLog(@"%@", [NSString stringWithFormat:@"添加 %@ 表中数据失败 ：数据连接失败。", name]);
    return NO;
    
}

//  保存数据
//  saveSql : 相应表格的保存语句
//  tableNameWithCHN : 相应表格的名称 （用于NSLog跟踪显示）
- (BOOL)deleteData:(NSString *)deleteSql tableNameWithCHN:(NSString *)name{
    NSLog(@"%@", [NSString stringWithFormat:@"删除 %@ 表中数据 》》》", name]);
    if (database != nil) {
        //  errMsg 用于显示错误信息
        char *errMsg;
        if (sqlite3_exec(database, [deleteSql UTF8String], NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"%@", [NSString stringWithFormat:@"删除 %@ 表中数据失败 : %s", name, errMsg]);
            return NO;
        }
        NSLog(@"%@", [NSString stringWithFormat:@"删除 %@ 表中数据成功", name]);
        return YES;

    }
    
    NSLog(@"%@", [NSString stringWithFormat:@"删除 %@ 表中数据失败：数据库连接失败。", name]);
    return NO;
}

//  查询数据
//  querySql : 相应表格的查询语句
//  tableNameWithCHN : 相应表格的名称 （用于NSLog跟踪显示）
//  count : 相应表格的列数
//  返回 : NSMutableArray 相应对象数据数组 
- (NSMutableArray *)queryData:(NSString *)querySql tableNameWithCHN:(NSString *)name columnsCount:(int)count{
    NSLog(@"%@", [NSString stringWithFormat:@"检索 %@ 表中数据 》》》", name]);
    NSMutableArray * array = [[[NSMutableArray alloc] init] autorelease];
    if (database != nil) {
        sqlite3_stmt * stmt;
        if (sqlite3_prepare_v2(database, [querySql UTF8String], -1, &stmt, nil) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW) {
                NSMutableArray * item = [[NSMutableArray alloc] init];
                for (int i = 1; i < count; i++) {
                    char * data = (char *)sqlite3_column_text(stmt, i);
                    // 将C语言的char *类型，编码为NSString类型
                    NSString * obj = [[NSString alloc] initWithUTF8String:data];
                    [item addObject:obj];
                    [obj release];
                }
                [array addObject:item];
                [item release];
            }
            NSLog(@"%@", [NSString stringWithFormat:@"检索 %@ 表中数据成功.", name]);
        } else {
            NSLog(@"%@", [NSString stringWithFormat:@"检索 %@ 表中数据失败.", name]);
        }
        return array;
    }
    NSLog(@"%@", [NSString stringWithFormat:@"检索 %@ 表中数据失败：数据库连接失败。", name]);
    return array;
}

@end
