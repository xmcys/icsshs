//
//  销售分析 单条卷烟详情 视图控制器
//  SADetailTableViewController.h
//  icss-hs
//
//  Created by hsit on 11-9-23.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SaleAnalyse;                   //  销售分析 对象
@class SADetailCurveViewController;   //  销售分析 曲线视图对象

@interface SADetailTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate> {
    int connectionTimes;          //  连接次数
}

@property (nonatomic, retain) IBOutlet UITableView * tableView;            //  销售分析表
@property (nonatomic, retain) IBOutlet UILabel     * totalRequireLabel;    //  总计 需求量
@property (nonatomic, retain) IBOutlet UILabel     * totalOrderLabel;      //  总计 订单量
@property (nonatomic, retain) IBOutlet UILabel     * totalCashLabel;       //  总计 金额
@property (nonatomic, retain) IBOutlet UILabel     * totalRateLabel;       //  总计 满足率
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * spinner;  //  加载指示器
@property (nonatomic, retain) IBOutlet UIView  * bottomView;               //  放置所有控件的底层视图

@property (nonatomic, retain) NSMutableData        * data;                 //  网络数据
@property (nonatomic, retain) NSMutableArray       * array;                //  销售分析对象数组
@property (nonatomic, retain) NSURLRequest         * request;              //  请求对象
@property (nonatomic, retain) NSURLConnection      * connection;           //  连接对象

@property (nonatomic, retain) SaleAnalyse          * saleAnalyse;          //  销售分析对象
@property (nonatomic, retain) SaleAnalyse          * tmpSaleAnalyse;       //  临时销售分析对象
@property (nonatomic, retain) NSString             * tmpValue;             //  临时值（解析XML时用）
@property (nonatomic, retain) NSString             * startDate;            //  开始日期
@property (nonatomic, retain) NSString             * endDate;              //  结束日期
@property (nonatomic, retain) SADetailCurveViewController * curveViewController; // 曲线视图对象

//  初始化卷烟信息
- (void)initCigaretteInfo:(SaleAnalyse *)sa startDate:(NSString *)start endDate:(NSString *)end curveViewController:(SADetailCurveViewController * )curveView;

//  加载数据
- (void)loadData;

//  解析XML
- (void)parserXML;

//  取消加载
- (void)cancelLoading;

//  重载表视图
- (void)reloadTable;

@end
