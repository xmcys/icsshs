//
//  分类视图控制器
//  RTChartViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "RTChartViewController.h"
#import "RTBarChartViewController.h"
#import "RTPieChartViewController.h"
#import "GlobalUtil.h"

@implementation RTChartViewController

@synthesize classifySalesViewController;    //  卷烟分类列表视图控制器
@synthesize scrollView;                     //  可滚动视图
@synthesize pageControl;                    //  分页控制器
@synthesize barChartViewController;         //  柱图视图控制器
@synthesize pieChartViewController;         //  饼图视图控制器

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [scrollView release];
    [pageControl release];
    [classifySalesViewController release];
    [pieChartViewController release];
    [barChartViewController release];
    [super dealloc];
}

- (void)viewDidUnload
{
    scrollView = nil;
    pageControl = nil;
    classifySalesViewController = nil;
    pieChartViewController = nil;
    barChartViewController = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  改变显示页
- (IBAction)pageControlChangePage{
    CGRect frame = scrollView.frame;
    frame.origin.x = scrollView.frame.size.width * pageControl.currentPage;
    frame.origin.y = 0;
    //  将滚动视图滚动到显示页位置
    [scrollView scrollRectToVisible:frame animated:YES];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //  重定义滚动视图内容大小
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * 3, self.scrollView.frame.size.height);
    
    //  初始化卷烟列表视图控制器
    self.classifySalesViewController = [[RTClassifySalesViewController alloc] initWithNibName:@"RTClassifySalesViewController" bundle:nil];
    self.classifySalesViewController.view.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    self.classifySalesViewController.delegate = self;
    [self.scrollView addSubview:self.classifySalesViewController.view];

    //  初始化柱图视图控制器
    self.barChartViewController = [[RTBarChartViewController alloc] init];
    self.barChartViewController.view.frame = CGRectMake(self.scrollView.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.barChartViewController.view];
    
    //  初始化饼图视图控制器
    self.pieChartViewController = [[RTPieChartViewController alloc] init];
    self.pieChartViewController.view.frame = CGRectMake(self.scrollView.frame.size.width * 2, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.pieChartViewController.view];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - 
#pragma mark Scroll View Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView{
    if (aScrollView.contentOffset.x < aScrollView.frame.size.width) {
        pageControl.currentPage = 0;
    } else if (aScrollView.contentOffset.x < aScrollView.frame.size.width * 2) {
        pageControl.currentPage = 1;
    } else {
        pageControl.currentPage = 2;
    }
}

#pragma mark -
#pragma mark RTClassifySales View Delegate

//  分类视图控制器协议 ： 显示柱图
- (void)showBarChartView{
    if ([GlobalUtil isIphone] && [self.classifySalesViewController.classifyArray count] != 0) {
        //  iPhone
        [self.barChartViewController setAxisDatas:self.classifySalesViewController.classifyArray];
        [self.barChartViewController drawBarChart];
    }
}

//  分类视图控制器协议 ： 显示饼图
- (void)showPieChartView{
    if ([GlobalUtil isIphone] && [self.classifySalesViewController.classifyArray count] != 0) {
        //  iPhone
        [self.pieChartViewController setOrderArray:self.classifySalesViewController.classifyArray];
        [self.pieChartViewController drawGraph];
    }
}

@end
