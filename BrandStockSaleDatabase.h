// 
//  进销存数据库操作类 头文件
//  BrandStockSaleDatabase.h
//  icss-hs
//
//  Created by hsit on 11-10-11.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BrandStockSaleDatabase : NSObject {
    
}

//  创建进销存分析表
- (BOOL)createTable;

//  保存数据
//  array : 所有进销存数据，date : 查询周期
- (BOOL)saveData:(NSArray *)array searchDate:(NSString *)date;

//  删除所有进销存数据
- (BOOL)deleteAllData;

//  查询数据
//  返回所有进销存数据，包含一条组成为 “查询周期”“空”“空”……的进销存记录
- (NSMutableArray *)queryAllData;

@end
