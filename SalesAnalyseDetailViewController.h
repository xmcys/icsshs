//
//  销售分析 详情视图
//  SalesAnalyseDetailViewController.h
//  icss-hs
//
//  Created by hsit on 11-9-23.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

#define PagesCount 2

@class SaleAnalyse;  //  销售分析 类

@interface SalesAnalyseDetailViewController : UIViewController <UIScrollViewDelegate> {
    BOOL pageControlUsed;               //  标识分页控件是否正在使用
}

@property (nonatomic, retain) IBOutlet UIScrollView  * contentView;      //  可滚动内容视图
@property (nonatomic, retain) IBOutlet UIPageControl * pageControl;      //  分页控件
@property (nonatomic, retain) IBOutlet UIView        * firstBottomView;  //  详情视图底层视图 (iPad)
@property (nonatomic, retain) IBOutlet UIView        * secondBottomView; //  曲线视图底层视图 (iPad)
//  视图数组，用于放置分页视图：单个卷烟详情视图、曲线视图
@property (nonatomic, retain) NSMutableArray         * viewControllers;   

//  分页事件
- (IBAction) pageChanged:(id)sender;

//  加载各页视图
- (void)loadViewWithPage:(int)page;

//  初始化卷烟信息，并加载数据
- (void)initCigaretteInfo:(SaleAnalyse *)sa startDate:(NSString *)start endDate:(NSString *)end;

//  如果是iPhone，初始化视图
- (void)initForIPhone;

//  如果是iPad，初始化视图
- (void)initForIPad;
@end
