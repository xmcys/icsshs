//
//  客户信息类
//  Customer.h
//  icss-hs
//
//  Created by hsit on 11-10-19.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Customer : NSObject {
    
}

@property (nonatomic, retain) NSString * licenceCode;  //  许可证号
@property (nonatomic, retain) NSString * customerId;   //  客户代码
@property (nonatomic, retain) NSString * name;         //  客户名称
@property (nonatomic, retain) NSString * address;      //  地址
@property (nonatomic, retain) NSString * phone;        //  电话
@property (nonatomic, retain) NSString * level;        //  星级
@property (nonatomic, retain) NSString * mgrName;      //  客户经理
@property (nonatomic, retain) NSString * orderDate;    //  订货日
@property (nonatomic, retain) NSString * latitude;     //  经度
@property (nonatomic, retain) NSString * longitude;    //  纬度
@property (nonatomic, retain) NSString * distance;     //  距离
@property (nonatomic) int     index;   //  在数组中的位置

//  将属性转为字符串输出
- (NSString *)toString;

//  返回属性数量
+ (NSInteger)propertyCount;

@end
