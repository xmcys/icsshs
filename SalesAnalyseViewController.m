//
//  销售分析视图控制器 类
//  SalesAnalyseViewController.m
//  icss-hs
//
//  Created by hsit on 11-9-21.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "SalesAnalyseViewController.h"
#import "SalesAnalyseDetailViewController.h"
#import "GlobalUtil.h"
#import "icss_hsAppDelegate.h"
#import "SalesAnalyseDatabase.h"


@implementation SalesAnalyseViewController

@synthesize salesAnalyseArray;      //  销售分析对象数组
@synthesize salesAnalyseTable;      //  数据表视图
@synthesize searchView;             //  查询窗体  
@synthesize searchTable;            //  查询表视图
@synthesize searchButtonView;       //  查询表视图的footer
@synthesize datePickerView;         //  自定义日期选取器
@synthesize yearArray;              //  年份数组
@synthesize monthArray;             //  月份数组
@synthesize nameField;              //  卷烟名称 控件
@synthesize startDateField;         //  开始日期 控件
@synthesize endDateField;           //  结束日期 控件
@synthesize detailController;       //  详情视图
@synthesize headerView;             //  下拉刷新 视图
@synthesize headerImage;            //  下拉刷新 箭头
@synthesize headerLabel;            //  下拉刷新 文本
@synthesize headerSpinner;          //  下拉刷新 指示器
@synthesize request;                //  请求对象
@synthesize connection;             //  连接对象
@synthesize salesAnalyseData;       //  网络数据
@synthesize tmpValue;               //  临时值（解析XML）
@synthesize salesAnalyse;           //  销售分析 对象
@synthesize footerView;             //  上拉加载 视图
@synthesize footerLabel;            //  上拉加载 文本
@synthesize footerSpinner;          //  上拉加载 指示器
@synthesize footerImage;            //  上拉加载 箭头
@synthesize mainSpinner;            //  主加载指示器
@synthesize bottomLabel;            //  底部显示查询周期
@synthesize firstTitleLabel;        //  第一个标题
@synthesize firstBottomView;        //  卷烟名称标题(iPad)
@synthesize secondBottomView;       //  销售分析底层(iPad)
@synthesize brandNameTitleLabel;    //  单个卷烟底层(iPad)

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [salesAnalyseArray release];
    [salesAnalyseTable release];
    [searchView release];
    [searchTable release];
    [searchButtonView release];
    [yearArray release];
    [monthArray release];
    [datePickerView release];
    [nameField release];
    [detailController release];
    [headerLabel release];
    [headerImage release];
    [headerSpinner release];
    [headerView release];
    [request release];
    [connection release];
    [salesAnalyseData release];
    [tmpValue release];
    [salesAnalyse release];
    [footerView release];
    [footerLabel release];
    [footerImage release];
    [footerSpinner release];
    [mainSpinner release];
    [bottomLabel release];
    [db release];
    [firstTitleLabel release];
    [firstBottomView release];
    [secondBottomView release];
    [brandNameTitleLabel release];
    [super dealloc];
}

- (void)viewDidUnload
{
    self.salesAnalyseArray = nil;
    self.salesAnalyseTable = nil;
    self.searchView = nil;
    self.searchTable = nil;
    self.searchButtonView = nil;
    self.yearArray = nil;
    self.monthArray = nil;
    self.datePickerView = nil;
    self.nameField = nil;
    self.detailController = nil;
    self.headerView = nil;
    self.headerSpinner = nil;
    self.headerLabel = nil;
    self.headerImage = nil;
    self.request = nil;
    self.connection = nil;
    self.salesAnalyseData = nil;
    self.tmpValue = nil;
    self.salesAnalyse = nil;
    self.footerImage = nil;
    self.footerSpinner = nil;
    self.footerLabel = nil;
    self.footerView = nil;
    self.mainSpinner = nil;
    bottomLabel = nil;
    db = nil;
    firstTitleLabel = nil;
    firstBottomView = nil;
    secondBottomView = nil;
    brandNameTitleLabel = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewWillDisappear:(BOOL)animated{
    //  隐藏查询窗体
    self.searchView.frame = searchViewHideRect;
    searchViewIsHide = YES;
    self.navigationItem.rightBarButtonItem.title = @"查询";
    self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStylePlain;
    //  关闭键盘
    [self closeKeyBoard];
    //  重载表数据
    [self reloadTable];
}

//  错误日期格式提示
- (BOOL)showDateAlert{
    if([self.startDateField.text intValue] > [self.endDateField.text intValue]){
        [GlobalUtil showAlertWithTitle:@"格式错误" andMessage:@"开始日期不能大于结束日期"];
        return YES;
    }
    return NO;
}

//  显示或隐藏查询窗体
- (IBAction)showSearchView{
    [self closeKeyBoard];
    //  设置动画
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    if(searchViewIsHide){
        //  显示查询窗体
        self.searchView.frame = searchViewPopUpRect;
        searchViewIsHide = NO;
        self.navigationItem.rightBarButtonItem.title = @"关闭";
        self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStyleDone;
    }else{
        //  隐藏查询窗体
        self.searchView.frame = searchViewHideRect;
        searchViewIsHide = YES;
        self.navigationItem.rightBarButtonItem.title = @"查询";
        self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStylePlain;
    }
    [UIView commitAnimations];
}

//  重置查询窗体各控件 
-(IBAction)resetSearchView{
    //  重设各文本值
    self.nameField.text = @"";
    self.startDateField.text = [self initSearchDate:0];
    self.startDateField.textColor = [UIColor blackColor];
    self.endDateField.text = [self initSearchDate:1];
    self.endDateField.textColor = [UIColor blackColor];
    searchTableRow = -1;
    //  重设日期选取器
    [datePickerView reloadAllComponents];
    [datePickerView selectRow:0 inComponent:0 animated:YES];
    [datePickerView selectRow:0 inComponent:1 animated:YES];
    [self closeKeyBoard];
}

//  关闭键盘
- (void)closeKeyBoard{
    [self.nameField resignFirstResponder];
    [self.startDateField resignFirstResponder];
    [self.endDateField resignFirstResponder];
}

//  初始化查询周期  tag : 0 开始日期 1 结束日期
- (NSString *)initSearchDate:(NSInteger)tag{
    NSDate * date = [NSDate date];
    //  格式化日期
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyyMM"];
    [dateFormatter setFormatterBehavior:NSDateFormatterFullStyle];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString * dateString = [dateFormatter stringFromDate:date];
    [dateFormatter release];
    //  返回日期
    if(tag == 1){
        return dateString;
    }else{
        return [NSString stringWithFormat:@"%@01", [dateString substringToIndex:4]];
    }
}

//  设置日期选取器的值
- (void)setPickerSelectionWithDate:(NSString *)dateString{
    //  从文本控件中，获取年值和月值
    NSString * year = [dateString substringToIndex:4];
    NSString * month = [dateString substringFromIndex:4];
    NSInteger yearIndex = 0;
    NSInteger monthIndex = 0;
    //  获取当前年值在年份数组中的位置
    for(int i = 0; i < [self.yearArray count]; i++){
        if([year isEqualToString:[self.yearArray objectAtIndex:i]]){
            yearIndex = i;
            break;
        }
    }
    monthIndex = [month intValue] - 1;
    //  设置日期选取器的选中行
    [self.datePickerView selectRow:yearIndex inComponent:0 animated:YES];
    [self.datePickerView selectRow:monthIndex inComponent:1 animated:YES];
}

//  设置日期的颜色：黑色：普通状态  蓝色：正在编辑   红色：非法日期
- (void)setDateFieldColor{
    //  判断选中行
    if(searchTableRow == 0){
        //  开始日期
        if(self.startDateField.textColor != [UIColor redColor]){
            self.startDateField.textColor = [UIColor blueColor];
        }
        self.endDateField.textColor = [UIColor blackColor];
    }else if(searchTableRow == 1){
        //  结束日期
        if(self.startDateField.textColor != [UIColor redColor]){
            self.startDateField.textColor = [UIColor blackColor];
        }
        self.endDateField.textColor = [UIColor blueColor];
    }else{
        //  卷烟名称
        if(self.startDateField.textColor != [UIColor redColor]){
            self.startDateField.textColor = [UIColor blackColor];
        }
        self.endDateField.textColor = [UIColor blackColor];
    }
}

//  初始化下拉刷新视图
- (void)initHeaderView{
    //  初始化下拉刷新 视图
    headerView = [[UIView alloc]initWithFrame:CGRectMake(0, -35, salesAnalyseTable.frame.size.width, 35)];
    headerView.backgroundColor = [UIColor clearColor];
    
    //  初始化下拉刷新 文本
    headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, salesAnalyseTable.frame.size.width, 35)];
    headerLabel.textAlignment = UITextAlignmentCenter;
    headerLabel.text = @"向下拖动刷新数据...";
    headerLabel.textColor = [UIColor orangeColor];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont systemFontOfSize:fontSize];
    
    //  初始化下拉刷新 箭头
    headerImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_down.png"]];
    headerImage.frame = CGRectMake(25, 0, 32, 32);
    [headerImage setContentMode:UIViewContentModeScaleToFill];
    
    //  初始化下拉刷新 指示器
    headerSpinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    headerSpinner.frame = CGRectMake(columnHeight, 7, 20, 20);
    headerSpinner.hidesWhenStopped = YES;
    
    //  将各控件添加进 下拉刷新视图 中
    [headerView addSubview:headerLabel];
    [headerView addSubview:headerImage];
    [headerView addSubview:headerSpinner];
    //  将下拉刷新视图添加进表视图中
    [self.salesAnalyseTable addSubview:headerView];
}

//  初始化上拉加载视图
- (void)initFooterView{
    //  初始化 上拉加载 视图
    footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, salesAnalyseTable.frame.size.width, 35)];
    footerView.backgroundColor = [UIColor clearColor];
    
    //  初始化 上拉加载 文本
    footerLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, salesAnalyseTable.frame.size.width, 35)];
    footerLabel.textAlignment = UITextAlignmentCenter;
    footerLabel.text = @"向上拖动加载更多...";
    footerLabel.textColor = [UIColor orangeColor];
    footerLabel.backgroundColor = [UIColor clearColor];
    footerLabel.font = [UIFont systemFontOfSize:fontSize];
    
    //  初始化 上拉加载 箭头
    footerImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_down.png"]];
    footerImage.frame = CGRectMake(25, 0, 32, 32);
    [footerImage setContentMode:UIViewContentModeScaleToFill];
    [self.footerImage layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    
    //  初始化 上拉加载 指示器
    footerSpinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    footerSpinner.frame = CGRectMake(30, 7, 20, 20);
    footerSpinner.hidesWhenStopped = YES;
    
    //  将各控件添加进 上拉加载视图 中
    [footerView addSubview:footerLabel];
    [footerView addSubview:footerImage]; 
    [footerView addSubview:footerSpinner];
    footerView.tag = 99;
    //  将上拉加载视图添加进表视图中
    [self.salesAnalyseTable setTableFooterView:footerView];

}

//  点击查询按钮
- (IBAction)clickSearch{
    //  获取卷烟名称，去除前后空格
    NSString * cigaretteName = [self.nameField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    self.nameField.text = cigaretteName;
    if([cigaretteName length] != 0 && [GlobalUtil isHasNumberOnly:cigaretteName]){
        [GlobalUtil showAlertWithTitle:@"格式错误" andMessage:@"卷烟名称不能由纯数字组成"];
        return;
    }
    //  加载数据
    if(!isLoading){
        if([self showDateAlert]){
            return;
        }
        [self showSearchView];
        isOverLoad = YES;
        startRecord = 1;
        [self loadData];
    }
}

//  加载数据
- (void)loadData{
    BOOL isReachable = [GlobalUtil isReachable];
    if(!isReachable){
        [GlobalUtil showAlertWithTitle:@"网络故障" andMessage:@"当前网络不可用"];
    }else{
        if(!isLoading){
            // iPad  如果查询窗体仍然显示，关闭窗体。  iPad的屏幕较大，查询窗体不能覆盖整个窗体，可以点到父窗体
            if (![GlobalUtil isIphone] && !searchViewIsHide) {
                [self showSearchView];
            }
            [self changeButtonState:1];
            //  设置开始日期
            NSString * startDate = self.startDateField.text == nil ? [self initSearchDate:0] : self.startDateField.text;
            //  设置结束日期
            NSString * endDate = self.endDateField.text == nil ? [self initSearchDate:1] : self.endDateField.text;
            if ([startDate compare:endDate] == NSOrderedDescending) {
                startDate = endDate;
                self.startDateField.text = startDate;
                self.startDateField.textColor = [UIColor blackColor];
            }
            //  设置加载的最后一条记录位置
            int endRecord = startRecord + pageRecords - 1;
            if ([GlobalUtil isIphone]) {
                //  iPhone  查询周期提示
                bottomLabel.text = [NSString stringWithFormat:@"单位：数量（件）金额（万元）查询周期：%@ - %@", startDate, endDate];
            }else{
                //  iPad  查询周期提示
                bottomLabel.text = [NSString stringWithFormat:@"查询周期：%@ - %@", startDate, endDate];
            }
            //  获取卷烟名称
            NSString * cigaretteName = self.nameField.text;
            if([cigaretteName length] != 0 && [GlobalUtil isHasNumberOnly:cigaretteName]){
                [GlobalUtil showAlertWithTitle:@"格式错误" andMessage:@"卷烟名称不能由纯数字组成"];
                return;
            }
            [self changeButtonState:1];
            if(isOverLoad){
                //  设置下拉刷新视图状态
                self.headerLabel.text = @"正在获取数据..";
                self.headerImage.hidden = YES;
                [self.headerSpinner startAnimating];
                //  设置表视图内部偏移量
                self.salesAnalyseTable.contentInset = UIEdgeInsetsMake(self.headerView.frame.size.height, 0, 0, 0);
            }else{
                //  设置上拉加载视图状态
                self.footerLabel.text = @"正在获取数据..";
                self.footerImage.hidden = YES;
                [self.footerSpinner startAnimating];
            }
            loadRecord = 0;
            isLoading = YES;
            [self.mainSpinner startAnimating];
            if([cigaretteName length] == 0){
                //  卷烟名称为空，调用接口1
                NSString * url = nil;
                if ([icss_hsAppDelegate appDelegate].useInsideNet) {
                    url = [[NSString alloc]initWithFormat:@"http://10.188.50.238:9088/tmi/ws/rs/mobile/queryBrand/startMonth/%@/endMonth/%@/startPage/%d/endPage/%d?licence=123456789&key=25f9e794323b453885f5181f1b624d0b", startDate, endDate, startRecord, endRecord];
                } else {
                    url = [[NSString alloc]initWithFormat:@"http://xm.fj-tobacco.com:9088/tmi/ws/rs/mobile/queryBrand/startMonth/%@/endMonth/%@/startPage/%d/endPage/%d?licence=123456789&key=25f9e794323b453885f5181f1b624d0b", startDate, endDate, startRecord, endRecord];
                }
                request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:url]];
                connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
                [url release];
            }else{
                isOverLoad = YES;
                //  卷烟名称为不空，调用接口2
                NSString * url = nil;
                if ([icss_hsAppDelegate appDelegate].useInsideNet) {
                    url = [[NSString alloc]initWithFormat:@"http://10.188.50.238:9088/tmi/ws/rs/mobile/queryBrandName/startMonth/%@/endMonth/%@", startDate, endDate];
                } else {
                    url = [[NSString alloc]initWithFormat:@"http://xm.fj-tobacco.com:9088/tmi/ws/rs/mobile/queryBrandName/startMonth/%@/endMonth/%@", startDate, endDate];
                }
                request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:url]];
                [request setHTTPMethod:@"PUT"];
                [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
                NSString * name = [[NSString alloc] initWithFormat:@"<SqlStr><sqlStr>%@</sqlStr></SqlStr>", cigaretteName];
                [request setHTTPBody:[name dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]];
                connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
                [url release];
            }
        }
    }
}

//  重载表视图
- (void)reloadTable{
    isLoading = NO;
    //  释放connection对象
    if(self.connection != nil){
        [self.connection cancel];
        self.connection = nil;
        self.request = nil;
    }
    //  重载表视图数据
    [self.salesAnalyseTable reloadData];
    [self.mainSpinner stopAnimating];
    if(isOverLoad){
        //  更新下拉刷新视图状态
        self.headerLabel.text = @"向下拖动获取数据..";
        [self.headerImage layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
        self.headerImage.hidden = NO;
        [self.headerSpinner stopAnimating];
        if ([self.salesAnalyseArray count] != 0) {
            //  重设表视图偏移量
            self.salesAnalyseTable.contentInset = UIEdgeInsetsZero;
            //  将表视图滚回第一行
            [self.salesAnalyseTable setContentOffset:CGPointMake(0.0, 0.0)];
        }
    }else{
        //  更新上拉加载视图状态
        if(loadRecord == 0){
            self.footerLabel.text = @"已加载所有数据";
        }else{
            self.footerLabel.text = @"向上拖动加载更多..";
        }
        self.footerImage.hidden = NO;
        [self.footerSpinner stopAnimating];
        [self.footerImage layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    }
    [self changeButtonState:0];
}

//  取消加载
- (void)cancelLoading{
    loadRecord = -1;
    [self reloadTable];
}

//  解析xml数据
- (void)parserXMLData{
    NSLog(@"%@", [[[NSString alloc] initWithData:self.salesAnalyseData encoding:NSUTF8StringEncoding] autorelease]);
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:self.salesAnalyseData];
    [parser setDelegate:self];
    [parser parse];
    //  删除数据库表中所有数据
    [db deleteAllData];
    //  保存最后一次加载的数据，并将查询周期传入
    [db saveData:self.salesAnalyseArray searchDate:bottomLabel.text];
    [self reloadTable];
    [parser release];
}

//  重设导航栏右侧按钮状态  tag : 0 显示查询窗体  1  取消加载
- (void)changeButtonState:(int)tag{
    if (tag == 0) {
        UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] 
                                         initWithTitle:@"查询"
                                         style:UIBarButtonItemStylePlain 
                                         target:self
                                         action:@selector(showSearchView)];
        self.navigationItem.rightBarButtonItem = rightButton;
        [rightButton release];
    }else{
        UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] 
                                         initWithTitle:@"取消加载"
                                         style:UIBarButtonItemStylePlain 
                                         target:self
                                         action:@selector(cancelLoading)];
        self.navigationItem.rightBarButtonItem = rightButton;
        [rightButton release];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    //  根据设备定义字体大小、列宽高、查询窗体大小、位置
    if ([GlobalUtil isIphone]) {
        //  iPhone
        fontSize = 14.0;
        columnWidth = 60;
        columnHeight = 30;
        searchViewPopUpRect = CGRectMake(0, 0, 320, 416);
        searchViewHideRect = CGRectMake(0, 0, 320, 0);
    } else {
        //  iPad
        fontSize = 17.0;
        columnWidth = 150;
        columnHeight = 35;
        firstBottomView.layer.cornerRadius = 10.0f;
        secondBottomView.layer.cornerRadius = 10.0f;
        searchViewPopUpRect = CGRectMake(768 - 320 - 10, 10, 320, 416);
        searchViewHideRect = CGRectMake(768 - 320 - 10, 10, 320, 0);
    }
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    self.navigationItem.title = @"销售分析";
    [self changeButtonState:0];
    self.yearArray = [[NSArray alloc] initWithObjects:@"2000", @"2001", @"2002", @"2003", @"2004", @"2005", @"2006", @"2007", @"2008", @"2009", @"2010", @"2011", @"2012", @"2013", @"2014", nil];
    self.monthArray = [[NSArray alloc] initWithObjects:@"01", @"02", @"03", @"04", @"05", @"06", @"07", @"08", @"09", @"10", @"11", @"12", nil];
    //  添加查询窗体
    [self.view addSubview:self.searchView];
    //  隐藏超出屏幕的部分
    [self.searchView setClipsToBounds:YES];
    self.searchView.frame = searchViewHideRect;
    searchViewIsHide = YES;
    [self.searchView.layer setCornerRadius:10.0];
    searchTable.frame = CGRectMake(0, 0, 320, 200);
    searchButtonView.backgroundColor = searchTable.backgroundColor;
    searchTableRow = -1;
    //  添加下拉刷新 上拉加载视图
    [self initHeaderView];
    [self initFooterView];
    startRecord = 1;
    pageRecords = 50;
    isOverLoad = YES;
    isLoading = NO;
    //  初始化数据库对象，并创建表格
    db = [[SalesAnalyseDatabase alloc] init];
    [db createTable];
    //  查询数据
    NSMutableArray * array = [db queryAllData];
    self.salesAnalyseArray = [[NSMutableArray alloc] initWithArray:array];
    connectionTimes = 0;
    if ([self.salesAnalyseArray count] != 0) {
        //  数据库不为空
        //  获取最后一条记录 ： 最后一条记录是记录查询周期的，并不是真正的SalesAnalyse对象
        SaleAnalyse * sa = [self.salesAnalyseArray objectAtIndex:([self.salesAnalyseArray count] - 1)];
        bottomLabel.text = sa.brandId;
        //  移除最后一条记录
        [self.salesAnalyseArray removeObject:sa];
        loadRecord = [salesAnalyseArray count];
    } else {
        //  数据库为空
        [self loadData];
    }
    
    self.salesAnalyseTable.separatorColor = [UIColor darkGrayColor];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//  重写 UITableView 数据源
#pragma mark - 
#pragma mark Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.searchTable){
        //  查询窗体
        return 3;
    }else{
        //  销售分析数据表视图
        NSInteger count = [self.salesAnalyseArray count];
        if(count  > 0){
            return count;
        }else{
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.searchTable){
        //  查询窗体
        NSUInteger row = [indexPath row];
        static NSString * cellIdentifier = @"searchCell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if(cell == nil){
            //  定义单元格样式
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //  定义文本
            UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 75, 35)];
            label.tag = 10;
            label.font = [UIFont systemFontOfSize:14.0];
            label.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:label];
            [label release];
            
            //  定义文本框
            UITextField * field = [[UITextField alloc]initWithFrame:CGRectMake(90, 5, 200, 35)];
            field.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            field.tag = 11;
            field.backgroundColor = [UIColor clearColor];
            [field setDelegate:self];
            field.returnKeyType = UIReturnKeyDone;
            [field addTarget:self action:@selector(closeKeyBoard) forControlEvents:UIControlEventEditingDidEndOnExit];
            [cell.contentView addSubview:field];
            
            //  添加文本框关联
            if(row == 0){
                self.startDateField = field;
            }else if(row == 1){
                self.endDateField = field;
            }else{
                self.nameField = field;
            }
            [field release];
        }
        
        //  初始化查询窗体中，各控件值
        UILabel * label = (UILabel *)[cell viewWithTag:10];
        UITextField * field = (UITextField *) [cell viewWithTag:11];
        if (row == 0){
            label.text = @"开始日期：";
            field.enabled = NO;
            field.text = [self initSearchDate:0];
        }else if (row == 1){
            label.text = @"结束日期：";
            field.text = [self initSearchDate:1];
            field.enabled = NO;
        }else if (row == 2){
            label.text = @"卷烟名称：";
        }
        return cell;
    }else{
        //  销售分析数据表视图
        static NSString * cellIdentifier = @"cell";
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil){
            //  定义单元格样式（行）
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            
            cell.backgroundColor = [UIColor clearColor];
            
            //  定义 卷烟ID 文本（隐藏）
            UILabel * codeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
            codeLabel.tag = 1;
            [cell.contentView addSubview:codeLabel];
            [codeLabel release];
            
            //  定义 卷烟名称 文本
            UILabel * nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, firstTitleLabel.frame.size.width, columnHeight)];
            nameLabel.tag = 2;
            nameLabel.textAlignment = UITextAlignmentLeft;
            nameLabel.font = [UIFont systemFontOfSize:fontSize];
            nameLabel.textColor = [UIColor lightTextColor];
            nameLabel.backgroundColor = [UIColor clearColor];
            nameLabel.adjustsFontSizeToFitWidth = YES;
            nameLabel.minimumFontSize = fontSize - 3;
            [cell.contentView addSubview:nameLabel];
            [nameLabel release];
            
            //  定义 需求量 文本
            UILabel * requireLabel = [[UILabel alloc]initWithFrame:CGRectMake(firstTitleLabel.frame.size.width, 0, columnWidth, columnHeight)];
            requireLabel.tag = 3;
            requireLabel.textAlignment = UITextAlignmentCenter;
            requireLabel.font = [UIFont systemFontOfSize:fontSize];
            requireLabel.textColor = [UIColor lightTextColor];
            requireLabel.backgroundColor = [UIColor clearColor];
            requireLabel.adjustsFontSizeToFitWidth = YES;
            requireLabel.minimumFontSize = fontSize -3;
            [cell.contentView addSubview:requireLabel];
            [requireLabel release];
            
            //  定义 订单量 文本
            UILabel * orderLabel = [[UILabel alloc]initWithFrame:CGRectMake(columnWidth * 1 + firstTitleLabel.frame.size.width, 0, columnWidth, columnHeight)];
            orderLabel.tag = 4;
            orderLabel.textAlignment = UITextAlignmentCenter;
            orderLabel.font = [UIFont systemFontOfSize:fontSize];
            orderLabel.textColor = [UIColor lightTextColor];
            orderLabel.backgroundColor = [UIColor clearColor];
            orderLabel.adjustsFontSizeToFitWidth = YES;
            orderLabel.minimumFontSize = fontSize -3;
            [cell.contentView addSubview:orderLabel];
            [orderLabel release];
            
            //  重新计算列宽（主要是为iPhone服务）
            int newWidth = columnWidth;
            if (columnWidth != 150) {
                newWidth = columnWidth - 10;
            }
            //  定义 金额 文本
            UILabel * cashLabel = [[UILabel alloc]initWithFrame:CGRectMake(columnWidth * 2 + firstTitleLabel.frame.size.width, 0, newWidth, columnHeight)];
            cashLabel.tag = 5;
            cashLabel.textAlignment = UITextAlignmentCenter;
            cashLabel.font = [UIFont systemFontOfSize:fontSize];
            cashLabel.textColor = [UIColor lightTextColor];
            cashLabel.backgroundColor = [UIColor clearColor];
            cashLabel.adjustsFontSizeToFitWidth = YES;
            cashLabel.minimumFontSize = fontSize -3;
            [cell.contentView addSubview:cashLabel];
            [cashLabel release];
            
            //  定义 满足率 文本
            UILabel * rateLabel = [[UILabel alloc]initWithFrame:CGRectMake(columnWidth * 2 + firstTitleLabel.frame.size.width + newWidth, 0, newWidth, columnHeight)];
            rateLabel.tag = 6;
            rateLabel.textAlignment = UITextAlignmentCenter;
            rateLabel.font = [UIFont systemFontOfSize:fontSize];
            rateLabel.textColor = [UIColor lightTextColor];
            rateLabel.backgroundColor = [UIColor clearColor];
            rateLabel.adjustsFontSizeToFitWidth = YES;
            rateLabel.minimumFontSize = fontSize -3;
            [cell.contentView addSubview:rateLabel];
            [rateLabel release];
        }
        
        //  获取点击行
        NSUInteger row = [indexPath row];

        SaleAnalyse * item = [self.salesAnalyseArray objectAtIndex:row];
        
        //  设置扩展（主要是为iPad服务）
        NSString * expandString = nil;
        if ([GlobalUtil isIphone]) {
            expandString = @"";
        } else {
            expandString = @"     ";
        }
        
        //  设置各字段值
        UILabel * codeLabel = (UILabel *)[cell viewWithTag:1];
        codeLabel.text = item.brandId;
        
        UILabel * nameLabel = (UILabel *)[cell viewWithTag:2];
        nameLabel.text = [NSString stringWithFormat:@"%@%@", expandString, item.brandName];;
        
        UILabel * requireLabel = (UILabel *)[cell viewWithTag:3];
        requireLabel.text = [NSString stringWithFormat:@"%.2f%@", [item.require floatValue], expandString];
        
        UILabel * orderLabel = (UILabel *)[cell viewWithTag:4];
        orderLabel.text = [NSString stringWithFormat:@"%.2f%@", [item.order floatValue], expandString];
        
        UILabel * cashLabel = (UILabel *)[cell viewWithTag:5];
        cashLabel.text = [NSString stringWithFormat:@"%.2f%@", [item.cash floatValue], expandString];
        
        UILabel * rateLabel = (UILabel *)[cell viewWithTag:6];
        rateLabel.text = [NSString stringWithFormat:@"%@%%%@", item.rate, expandString];
        
        return cell;
    }
}

//  重写 UITableView 协议
#pragma mark - 
#pragma mark Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.searchTable){
        //  查询窗体
        NSUInteger row = [indexPath row];
        //  记录选中的行号
        searchTableRow = row;
        UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
        UITextField * field = (UITextField *)[cell viewWithTag:11];
        [self setDateFieldColor];
        if(row != 2){
            //  如果点击的是开始（结束）日期，则设置选取器的值
            [self closeKeyBoard];
            [self setPickerSelectionWithDate:field.text];
        }
    }
    if(tableView == self.salesAnalyseTable){
        //  销售分析数据表
        NSUInteger row = [indexPath row];
        //  定义一个详情控制器
        SalesAnalyseDetailViewController * saDetailViewController = nil;
        //  获取选中的卷烟信息
        SaleAnalyse * item = [self.salesAnalyseArray objectAtIndex:row];
        
        if ([GlobalUtil isIphone]) {
            //  iPhone  初始化详情视图
            saDetailViewController = [[SalesAnalyseDetailViewController alloc]initWithNibName:@"SalesAnalyseDetailViewController" bundle:nil];
            [self.navigationController pushViewController:saDetailViewController animated:YES];
            //  设置卷烟信息，并查询数据
            [saDetailViewController initCigaretteInfo:item startDate:self.startDateField.text endDate:self.endDateField.text];
        } else {
            //  iPad
            //  如果选中行已显示，则跳过
            if (![item.brandName isEqualToString:brandNameTitleLabel.text]) {
                //  移除底层窗体上的所有视图（详情视图）
                NSArray * views = [secondBottomView subviews];
                for (int i = 0; i < [views count]; i++) {
                    UIView * aView = [views objectAtIndex:i];
                    [aView removeFromSuperview];
                }
                //  初始化详情视图
                saDetailViewController = [[SalesAnalyseDetailViewController alloc]initWithNibName:@"SalesAnalyseDetailViewController-iPad" bundle:nil];
                //  设置卷烟标题
                brandNameTitleLabel.text = item.brandName;
                //  将详情视图添加到主视图中
                [secondBottomView addSubview:saDetailViewController.view];
                //  设置卷烟信息，查询数据
                [saDetailViewController initCigaretteInfo:item startDate:self.startDateField.text endDate:self.endDateField.text];
            }
        }
        [saDetailViewController release];
    }
}

//  取消选中行
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.searchTable){
        //  查询窗体
        //  设置日期字段的颜色
        [self setDateFieldColor];
    }
}

//  重写 UIPickerView 数据源
#pragma mark - 
#pragma mark PickerView Data Source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

//  组件行数
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component == 0){
        //  年组件
        return [self.yearArray count];
    }else{
        //  月组件
        return [self.monthArray count];
    }
}

//  组件行值
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(component == 0){
        return [self.yearArray objectAtIndex:row];
    }else{
        return [self.monthArray objectAtIndex:row];
    }
}

//  重写 UIPickerView 协议
#pragma mark PickerView Delegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    //  根据当前选中行号，修改相应日期字段值
    if(searchTableRow == 0){
        //  开始日期
        NSString * string  = self.startDateField.text;
        if(component == 0){
            //  年
            NSString * value = [self.yearArray objectAtIndex:row];
            self.startDateField.text = [NSString stringWithFormat:@"%@%@",
                                        value, [string substringFromIndex:4]];
        }else{
            //  月
            NSString * value = [self.monthArray objectAtIndex:row];
            self.startDateField.text = [NSString stringWithFormat:@"%@%@",
                                        [string substringToIndex:4], value];
        }
    }
    if(searchTableRow == 1){
        //  结束日期
        NSString * string  = self.endDateField.text;
        if(component == 0){
            //  年
            NSString * value = [self.yearArray objectAtIndex:row];
            self.endDateField.text = [NSString stringWithFormat:@"%@%@",
                                        value, [string substringFromIndex:4]];
        }else{
            //  月
            NSString * value = [self.monthArray objectAtIndex:row];
            self.endDateField.text = [NSString stringWithFormat:@"%@%@",
                                        [string substringToIndex:4], value];
        }
    }
    //  获取开始、结束日期
    NSString * start = self.startDateField.text;
    NSString * end  = self.endDateField.text;
    //  比较其值，设置颜色
    if([start compare:end] == NSOrderedDescending){
        self.startDateField.textColor = [UIColor redColor];
    }else{
        self.startDateField.textColor = [UIColor blueColor];
        [self setDateFieldColor];
    }
}

//  重写 UITextField 协议
#pragma mark - 
#pragma mark Text Field Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    //  开始编辑卷烟名称时，设定选中行号为 -1
    searchTableRow = -1;
    //  重设日期字段的颜色
    [self setDateFieldColor];
}

//  重写 UIScrollView 协议
#pragma mark -
#pragma mark ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (!isLoading){
        [UIView beginAnimations:nil context:nil];
        //  根据滚动距离，更新下拉刷新视图
        if(scrollView.contentOffset.y < -self.headerView.frame.size.height){
            self.headerLabel.text = @"释放刷新数据...";
            //  旋转箭头
            [self.headerImage layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
        }else if(scrollView.contentOffset.y < 0){
            self.headerLabel.text = @"向下拖动刷新数据...";
            [self.headerImage layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
        }
        //  计算 上拉 偏移量
        CGFloat offsetY = 0.0;
        if(self.salesAnalyseTable.contentSize.height > self.salesAnalyseTable.frame.size.height){
            offsetY = self.salesAnalyseTable.contentSize.height - self.salesAnalyseTable.frame.size.height;
        }
        //  根据上拉距离，更新上拉加载视图
        if(loadRecord != -1 && loadRecord < pageRecords){
            self.footerLabel.text = @"已加载所有数据";
        }else if(scrollView.contentOffset.y > 35.0 + offsetY){
            self.footerLabel.text = @"释放刷新数据...";
            [self.footerImage layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
        }else{
            self.footerLabel.text = @"向上拖动加载更多...";
            [self.footerImage layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
        }
        [UIView commitAnimations];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    //  判断距离，下拉刷新
    if (!isLoading && scrollView.contentOffset.y  <= -self.headerView.frame.size.height){
        isOverLoad = YES;
        startRecord = 1;
        [self loadData];
    }
    //  判断距离，上拉加载
    CGFloat offsetY = 0.0;
    if(self.salesAnalyseTable.contentSize.height > self.salesAnalyseTable.frame.size.height){
        offsetY = self.salesAnalyseTable.contentSize.height - self.salesAnalyseTable.frame.size.height;
    }
    //  如果已经全部加载，则跳过
    if (loadRecord != -1 && loadRecord < pageRecords) {
        return;
    }
    if(!isLoading && scrollView.contentOffset.y > 35.0 + offsetY){
        isOverLoad = NO;
        startRecord = [self.salesAnalyseArray count] + 1;
        [self loadData];
    }
}

//  重写 NSURLConnection 协议
#pragma mark - 
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"连接异常：%@", error);
    [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:@"请求数据发生异常。"];
    [self reloadTable];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *) response;
    if((httpURLResponse.statusCode / 100) != 2){
        if (httpURLResponse.statusCode == 500 && connectionTimes < 3) {
            [self.connection cancel];
            [self.connection release];
            self.connection = nil;
            connectionTimes++;
            NSLog(@"第 %d 次连接", connectionTimes);
            [self loadData];
        } else {
            connectionTimes = 0;
            [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:[NSString stringWithFormat:@"网络响应异常：%d", httpURLResponse.statusCode]];
            [self reloadTable];
        }
        NSLog(@"网络响应异常: %@", [[httpURLResponse allHeaderFields] description]);
    }else{
        //  释放原先的data
        if (self.salesAnalyseData != nil) {
            [self.salesAnalyseData release];
            self.salesAnalyseData = nil;
        }
        //  接受到响应时，初始化salesAnalyseData
        self.salesAnalyseData = [[NSMutableData alloc] init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.salesAnalyseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [self parserXMLData];
}

////  下面两个方法用于降低网络安全性，可以使用https协议
//- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
//    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]; 
//}
//
//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge { 
//    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
//        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge]; 
//    }
//    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge]; 
//}

//  重写 NSXMLParser 协议
#pragma mark -
#pragma mark XMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    //  xml 全局key开始
    if([elementName isEqualToString:@"Mobiles"]){
        //  初始化数组
        if(self.salesAnalyseArray == nil){
            self.salesAnalyseArray = [[NSMutableArray alloc] init];
        }
        //  如果是刷新，清空当前数据
        if(isOverLoad){
            [self.salesAnalyseArray removeAllObjects];
        }
    }
    //  xml 每个对象key开始
    if([elementName isEqualToString:@"Mobile"]){
        //  初始化销售分析对象
        self.salesAnalyse = [[SaleAnalyse alloc]init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    //  保存各值
    self.tmpValue = string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    //  根据值key，保存值
    if([elementName isEqualToString:@"amtOrder"]){
        self.salesAnalyse.cash = self.tmpValue;
    }else if([elementName isEqualToString:@"bkrTime"]){
        self.salesAnalyse.date = self.tmpValue;
    }else if([elementName isEqualToString:@"brandId"]){
        self.salesAnalyse.brandId = self.tmpValue;
    }else if([elementName isEqualToString:@"brandName"]){
        self.salesAnalyse.brandName = self.tmpValue;
    }else if([elementName isEqualToString:@"qtyDemand"]){
        self.salesAnalyse.require = self.tmpValue;
    }else if([elementName isEqualToString:@"qtyOrder"]){
        self.salesAnalyse.order = self.tmpValue;
    }else if([elementName isEqualToString:@"satisfaction"]){
        self.salesAnalyse.rate = self.tmpValue;
        //  将解析好的销售分析对象添加进数组中，在xml接口格式中，此key是最后一项，因此在这里添加
        [self.salesAnalyseArray addObject:self.salesAnalyse];
        loadRecord++;
        [self.salesAnalyse release];
        self.salesAnalyse = nil;
    }
    self.tmpValue = nil;
}

@end
