//
//  通用数据库操作类
//  Database.h
//  icss-hs
//
//  Created by hsit on 11-10-11.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface Database : NSObject {
    sqlite3 * database;
}

//  关闭数据库
- (void)closeDatabase;

//  打开数据库
- (BOOL)openDatabase;

//  创建表格
//  createSql : 相应表格的创建语句 
//  tableNameWithCHN : 相应表格的名称 （用于NSLog跟踪显示）
- (BOOL)createTable:(NSString *)createSql tableNameWithCHN:(NSString *)name;

//  保存数据
//  saveSql : 相应表格的保存语句
//  tableNameWithCHN : 相应表格的名称 （用于NSLog跟踪显示）
- (BOOL)saveData:(NSString *)saveSql tableNameWithCHN:(NSString *)name;

//  保存数据
//  saveSql : 相应表格的保存语句
//  tableNameWithCHN : 相应表格的名称 （用于NSLog跟踪显示）
- (BOOL)deleteData:(NSString *)deleteSql tableNameWithCHN:(NSString *)name;

//  查询数据
//  querySql : 相应表格的查询语句
//  tableNameWithCHN : 相应表格的名称 （用于NSLog跟踪显示）
//  count : 相应表格的列数
//  返回 : NSMutableArray 相应对象数据数组 
- (NSMutableArray *)queryData:(NSString *)querySql tableNameWithCHN:(NSString *)name columnsCount:(int)count;


@end
