//
//  实时订单视图控制器
//  RTSalesViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "RTSalesViewController.h"
#import "ClassifySales.h"
#import "RTChartViewController.h"
#import "GlobalUtil.h"


@implementation RTSalesViewController

@synthesize contentView;              //  内容视图
@synthesize tabBar;                   //  选项卡工具条
@synthesize classifyArray;            //  分类对象数组
@synthesize brandListArray;           //  卷烟对象数组
@synthesize brandListViewController;  //  卷烟列表视图控制器
@synthesize chartViewController;      //  分类视图控制器
@synthesize classifyView;             //  分类视图 （用于iPad）
@synthesize brandView;                //  卷烟视图 （用于iPad）
@synthesize barView;                  //  柱图视图 （用于iPad）
@synthesize pieView;                  //  饼图视图 （用于iPad）
@synthesize classifyViewController;   //  分类信息视图控制器（用于iPad）
@synthesize barChartViewController;   //  柱图视图控制器（用于iPad）
@synthesize pieChartViewController;   //  饼图视图控制器（用于iPad）

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [brandListArray release];
    [classifyArray release];
    [contentView release];
    [tabBar release];
    [brandListViewController release];
    [chartViewController release];
    [rightBarButton release];
    [brandView release];
    [classifyView release];
    [barView release];
    [pieView release];
    [classifyViewController release];
    [barChartViewController release];
    [pieChartViewController release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    brandListArray = nil;
    classifyArray = nil;
    contentView = nil;
    tabBar = nil;
    brandListViewController = nil;
    chartViewController = nil;
    rightBarButton = nil;
    brandView = nil;
    classifyView = nil;
    barView = nil;
    pieView = nil;
    classifyViewController = nil;
    barChartViewController = nil;
    pieChartViewController = nil;
    [super viewDidUnload];
}

//  更改显示的视图 ： 卷烟列表  分类列表
- (void)changeDisplayView:(int)index{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.contentView cache:YES];
    [UIView setAnimationDuration:0.5];
    if (index == 1) {
        //  移除分类视图，切换到卷烟视图
        [self.chartViewController.view removeFromSuperview];
        [self.contentView addSubview:self.brandListViewController.view];
    } else {
        //  移除卷烟视图，切换到分类视图
        [self.brandListViewController.view removeFromSuperview];
        [self.contentView addSubview:self.chartViewController.view];
    }
    [UIView commitAnimations];
    if (index == 1) {
        //  当视图为卷烟视图时，导航栏显示查询按钮。
        self.navigationItem.rightBarButtonItem = rightBarButton;
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

//  显示或隐藏查询框
- (IBAction)showSearchView{
    [self.brandListViewController showOrHideSerachBar];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"当日订单";
    if ([GlobalUtil isIphone]) {
        //  初始化卷烟列表视图控制器
        RTBrandListViewController * brandViewController = [[RTBrandListViewController alloc]initWithNibName:@"RTBrandListViewController" bundle:nil];
        brandViewController.delegate = self;
        self.brandListViewController = brandViewController;
        [brandViewController release];
        
        //  初始化分类视图控制器
        RTChartViewController * chartView = [[RTChartViewController alloc] init];
        self.chartViewController = chartView;
        [chartView release];
        
        //  设置当前显示页为卷烟列表
        currentViewIndex = 1;
        [self.tabBar setSelectedItem: [[self.tabBar items] objectAtIndex:0]];
        [self.contentView addSubview:self.brandListViewController.view];
        
        //  初始化导航栏右侧查询按钮
        rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"查询" style:UIBarButtonItemStylePlain target:self action:@selector(showSearchView)];
        self.navigationItem.rightBarButtonItem = rightBarButton;
    } else {
        
        //  初始化卷烟列表视图控制器
        RTBrandListViewController * brandViewController = [[RTBrandListViewController alloc]initWithNibName:@"RTBrandListViewController-iPad" bundle:nil];
        brandViewController.delegate = self;
        self.brandListViewController = brandViewController;
        [brandViewController release];
        [self.brandView addSubview:self.brandListViewController.view];
        
        //  初始化分类信息视图控制器
        RTClassifySalesViewController * classifyController = [[RTClassifySalesViewController alloc] initWithNibName:@"RTClassifySalesViewController-iPad" bundle:nil];
        classifyController.delegate = self;
        self.classifyViewController = classifyController;
        [self.classifyView addSubview:self.classifyViewController.view];
        [classifyController release];
        
        //  初始化柱图视图控制器
        RTBarChartViewController * barController = [[RTBarChartViewController alloc] init];
        barController.view.frame = CGRectMake(0, 0, 370, 465);
        barController.view.backgroundColor = [UIColor clearColor];
        barController.graphView.frame = CGRectMake(0, 0, 370, 465);
        self.barChartViewController = barController;
        [self.barView addSubview:self.barChartViewController.view];
        [barController release];
        
        //  初始化饼图视图控制器
        RTPieChartViewController * pieController = [[RTPieChartViewController alloc] init];
        pieController.view.frame = CGRectMake(0, 0, 370, 465);
        pieController.view.backgroundColor = [UIColor clearColor];
        pieController.graphView.frame = CGRectMake(0, 0, 370, 465);
        self.pieChartViewController = pieController;
        [self.pieView addSubview:self.pieChartViewController.view];
        [pieController release];
    }
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
}

- (void)viewWillDisappear:(BOOL)animated{
    //  取消卷烟列表加载
    [self.brandListViewController cancelLoading];
    //  取消分类列表加载
    [self.chartViewController.classifySalesViewController cancelLoaing];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Tab Bar Delegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if (currentViewIndex != item.tag) {
        //  如果选中的选项卡于当前选项卡不同，切换视图。
        currentViewIndex = item.tag;
        [self changeDisplayView:currentViewIndex];
    }
}

#pragma mark -
#pragma mark RTBrandList View Delegate
//  卷烟列表协议：更改导航栏右侧查询按钮的文字和状态
- (void)changeNavigationRightBarButtonState:(NSString *)title style:(UIBarButtonItemStyle)style{
    rightBarButton.title = title;
    rightBarButton.style = style;
}

#pragma mark -
#pragma mark RTClassifySales View Delegate

//  分类信息协议：显示柱图
- (void)showBarChartView{
    if (![GlobalUtil isIphone] && [self.classifyViewController.classifyArray count] != 0) {
        //  iPad
        [self.barChartViewController setAxisDatas:self.classifyViewController.classifyArray];
        [self.barChartViewController drawBarChart];
    }
}

//  分类信息协议：显示饼图
- (void)showPieChartView{
    if (![GlobalUtil isIphone] && [self.classifyViewController.classifyArray count] != 0) {
        //  iPad
        [self.pieChartViewController setOrderArray:self.classifyViewController.classifyArray];
        [self.pieChartViewController drawGraph];
    }
}

@end
