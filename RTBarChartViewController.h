//
//  分类柱图视图控制器
//  RTBarChartViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"


@interface RTBarChartViewController : UIViewController <CPTPlotSpaceDelegate, CPTPlotDataSource, 
CPTBarPlotDelegate> {
    CPTPlotSpaceAnnotation * requireAnnotation;  //  需求量 点标签
    CPTPlotSpaceAnnotation * orderAnnotation;    //  订单量 点标签
    
    CPTGraph               * graph;              //  图形区
    float                    xFloat;             //  x坐标最大值
    float                    yFloat;             //  y坐标最大值
    NSMutableArray         * requireAxis;        //  需求量坐标数组
    NSMutableArray         * orderAxis;          //  订单量坐标数组
}

@property (nonatomic, retain) IBOutlet UIView  * graphView;     //  绘区图底层视图

//  绘制柱图
- (void)drawBarChart;

//  设置坐标数组
- (void)setAxisDatas:(NSArray *)array;

@end
