//
//  销售分析曲线图视图控制器
//  SADetailCurveViewController.h
//  icss-hs
//
//  Created by hsit on 11-9-23.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface SADetailCurveViewController : UIViewController <CPTPlotDataSource, CPTPlotSpaceDelegate, CPTScatterPlotDelegate> {
    
    CPTPlotSpaceAnnotation * requireAnnotation;  //  需求量 点标签
    CPTPlotSpaceAnnotation * orderAnnotation;    //  订单量 点标签
    
    CPTGraph               * graph;              //  图形区
    float                    xFloat;             //  x坐标最大值
    float                    yFloat;             //  y坐标最大值
    
    NSMutableArray         * requireAxis;        //  需求量坐标数组
    NSMutableArray         * orderAxis;          //  订单量坐标数组
    NSMutableArray         * dateAxis;           //  日期坐标数组
    NSArray                * SAArray;            //  销售分析对象数组
    
}

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * spinner;  //  指示器
@property (nonatomic, retain) IBOutlet UILabel * dateLabel;     //  日期标签
@property (nonatomic, retain) IBOutlet UILabel * requireLabel;  //  需求量标签
@property (nonatomic, retain) IBOutlet UILabel * orderLabel;    //  订单量标签
@property (nonatomic, retain) IBOutlet UILabel * cashLabel;     //  金额标签
@property (nonatomic, retain) IBOutlet UILabel * rateLabel;     //  满足率标签
@property (nonatomic, retain) IBOutlet UIView  * graphView;     //  绘区图底层视图
@property (nonatomic, retain) IBOutlet UIView  * bottomView;    //  整个view的底层视图

//  设置各标签的值
- (void)initLabelsWithDate:(NSString *)date require:(NSString *)require order:(NSString *)order cash:(NSString *)cash rate:(NSString *)rate;

//  绘制曲线图
- (void)drawGraph;

//  设置坐标数组  参数 ： 销售分析对象数组
- (void)setAxisDatas:(NSArray *)array;

@end
