//
//  启动视图控制器
//  StartingViewController.h
//  icss-hs
//
//  程序启动类：用于检测网络，载入各项数据等功能。
//  Created by hsit on 11-9-21.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface StartingViewController : UIViewController {
    
}

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * activityIndicatorView; // 指示器
@property (nonatomic, retain) IBOutlet UIButton * checkButton;  //  重新检测 按钮

- (IBAction)checkAgain:(id)sender;  //  点击重新检测
- (void)prepareToStart;             //  准备各项配置、检查网络

@end
