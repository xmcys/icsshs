//
//  分类列表视图控制器
//  RTClassifySalesViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "RTClassifySalesViewController.h"
#import "ClassifySales.h"
#import "RefreshHeaderView.h"
#import "GlobalUtil.h"
#import "icss_hsAppDelegate.h"


@implementation RTClassifySalesViewController

@synthesize spinner;         //  加载指示器
@synthesize classifyTable;   //  分类表视图
@synthesize timeLabel;       //  显示查询时间
@synthesize delegate;        //  分类视图协议
@synthesize classifyArray;   //  分类数组

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [classifyArray release];
    [classifyTable release];
    [spinner release];
    [connection release];
    [aValue release];
    [classify release];
    [classifyData release];
    [headerView release];
    [timeLabel release];
    [delegate release];
    [super dealloc];
}

- (void)viewDidUnload
{
    self.classifyTable = nil;
    self.classifyArray = nil;
    spinner = nil;
    connection = nil;
    classify = nil;
    classifyData = nil;
    self.spinner = nil;
    headerView = nil;
    timeLabel = nil;
    delegate = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  加载分类数据
- (void)loadClassifyData{
    //  判断网络
    if (![GlobalUtil isReachable]) {
        [GlobalUtil showAlertWithTitle:@"网络故障" andMessage:@"当前网络不可用"];
        //  重设表视图偏移量
        if (classifyArray == nil || [classifyArray count] == 0) {
            self.classifyTable.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
        }else{
            self.classifyTable.contentInset = UIEdgeInsetsZero;
        }
        [headerView changeArrowAndLabelState:0];
        return;
    }
    isLoading = YES;
    [spinner startAnimating];
    NSString * url = nil;
    if ([icss_hsAppDelegate appDelegate].useInsideNet) {
        url = [NSString stringWithFormat:@"http://10.188.50.238:9088/tmi/ws/rs/mobile/queryPriceType?licence=123456789&key=25f9e794323b453885f5181f1b624d0b"];
    } else {
        url = [NSString stringWithFormat:@"http://xm.fj-tobacco.com:9088/tmi/ws/rs/mobile/queryPriceType?licence=123456789&key=25f9e794323b453885f5181f1b624d0b"];
    }
    NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

//  取消加载
- (void)cancelLoaing{
    if (connection != nil) {
        [connection cancel];
    }
    [connection release];
    connection = nil;
}

//  刷新表视图
- (void)reloadTable{
    [connection release];
    connection = nil;
    isLoading = NO;
    [spinner stopAnimating];
    [self.classifyTable reloadData];
    if ([classifyArray count] != 0) {
        self.classifyTable.contentInset = UIEdgeInsetsZero;
    }
    //  重设下拉刷新视图状态
    [headerView changeArrowAndLabelState:0];
}

//  解析XML数据
- (void)parseXMLData{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:classifyData];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
    //  显示本次查询时间
    self.timeLabel.text = [[GlobalUtil getCurrentFullTime] substringToIndex:10];
    [self sumClassifyArray];
    [self reloadTable];
    //  判断协议是否实现，若实现，则调用
    if ([delegate respondsToSelector:@selector(showBarChartView)]) {
        //  显示柱图
        [delegate showBarChartView];
    }
    if ([delegate respondsToSelector:@selector(showPieChartView)]) {
        //  显示饼图
        [delegate showPieChartView];
    }
}

//  计算各字段总量
- (void)sumClassifyArray{
    float demand = 0;
    float order = 0;
    float cash = 0;
    //  计算需求、订单、金额总量
    for (int i = 1; i < [classifyArray count]; i++) {
        ClassifySales * aClassify = [classifyArray objectAtIndex:i];
        demand += [aClassify.demand floatValue];
        order += [aClassify.order floatValue];
        cash += [aClassify.cash floatValue];
    }
    //  计算需求、订单、金额的占比
    for (int i = 1; i < [classifyArray count]; i++) {
        ClassifySales * aClassify = [classifyArray objectAtIndex:i];
        aClassify.demandPercent = [NSString stringWithFormat:@"%.2f", [aClassify.demand floatValue] / demand * 100];
        aClassify.orderPercent = [NSString stringWithFormat:@"%.2f", [aClassify.order floatValue] / order * 100];
        aClassify.cashPercent = [NSString stringWithFormat:@"%.2f", [aClassify.cash floatValue] / cash * 100];
    }
    if ([classifyArray count] != 0) {
        //  将“总计”定义为一个分类，添加进数组
        ClassifySales * aClassify = [[ClassifySales alloc] init];
        aClassify.classifyName = @"合计";
        aClassify.demand = [NSString stringWithFormat:@"%f", demand];
        aClassify.demandPercent = @"100%";
        aClassify.order = [NSString stringWithFormat:@"%f", order];
        aClassify.orderPercent = @"100%";
        aClassify.cash = [NSString stringWithFormat:@"%f", cash];
        aClassify.cashPercent = @"100%";
        [classifyArray addObject:aClassify];
        [aClassify release];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    connectionTimes = 0;
    //  初始化下拉刷新视图
    headerView = [[RefreshHeaderView alloc] initWithFrame:CGRectMake(0, -35, self.view.frame.size.width, 35)];
    [self.classifyTable addSubview:headerView];
    //  加载数据
    self.classifyTable.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
    [self loadClassifyData];
    self.classifyTable.separatorColor = [UIColor darkGrayColor];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [classifyArray count] - 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * identifier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //  分类名称
        UILabel * classifyLabel = nil;
        //  需求量
        UILabel * demandLabel = nil;
        //  订单量
        UILabel * orderLabel = nil;
        //  订单量占比
        UILabel * orderPercentLabel = nil;
        //  金额
        UILabel * cashLabel = nil;
        
        if ([GlobalUtil isIphone]) {
            //  iPhone
            classifyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 44)];
            demandLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 70, 44)];
            orderLabel = [[UILabel alloc] initWithFrame:CGRectMake(120, 0, 70, 44)];
            orderPercentLabel = [[UILabel alloc] initWithFrame:CGRectMake(190, 0, 60, 44)];
            cashLabel = [[UILabel alloc] initWithFrame:CGRectMake(250, 0, 70, 44)];
        } else {
            //  iPad
            classifyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 44)];
            demandLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 0, 70, 44)];
            orderLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, 70, 44)];
            orderPercentLabel = [[UILabel alloc] initWithFrame:CGRectMake(230, 0, 70, 44)];
            cashLabel = [[UILabel alloc] initWithFrame:CGRectMake(300, 0, 70, 44)];
        }
        //  分类名称
        classifyLabel.backgroundColor = [UIColor clearColor];
        classifyLabel.tag = 1;
        classifyLabel.textColor = [UIColor lightTextColor];
        classifyLabel.textAlignment = UITextAlignmentCenter;
        classifyLabel.font = [UIFont systemFontOfSize:14.0];
        [cell.contentView addSubview:classifyLabel];
        [classifyLabel release];
        
        //  需求量
        demandLabel.backgroundColor = [UIColor clearColor];
        demandLabel.tag = 2;
        demandLabel.textColor = [UIColor lightTextColor];
        demandLabel.textAlignment = UITextAlignmentCenter;
        demandLabel.font = [UIFont systemFontOfSize:14.0];
        demandLabel.minimumFontSize = 10.0;
        demandLabel.adjustsFontSizeToFitWidth = YES;
        [cell.contentView addSubview:demandLabel];
        [demandLabel release];
        
        //  订单量
        orderLabel.backgroundColor = [UIColor clearColor];
        orderLabel.tag = 3;
        orderLabel.textColor = [UIColor lightTextColor];
        orderLabel.textAlignment = UITextAlignmentCenter;
        orderLabel.font = [UIFont systemFontOfSize:14.0];
        orderLabel.minimumFontSize = 10.0;
        orderLabel.adjustsFontSizeToFitWidth = YES;
        [cell.contentView addSubview:orderLabel];
        [orderLabel release];
        
        //  订单量占比
        orderPercentLabel.backgroundColor = [UIColor clearColor];
        orderPercentLabel.tag = 4;
        orderPercentLabel.textColor = [UIColor lightTextColor];
        orderPercentLabel.textAlignment = UITextAlignmentCenter;
        orderPercentLabel.font = [UIFont systemFontOfSize:14.0];
        orderPercentLabel.minimumFontSize = 10.0;
        orderPercentLabel.adjustsFontSizeToFitWidth = YES;
        [cell.contentView addSubview:orderPercentLabel];
        [orderPercentLabel release];
        
        //  金额
        cashLabel.backgroundColor = [UIColor clearColor];
        cashLabel.tag = 5;
        cashLabel.textColor = [UIColor lightTextColor];
        cashLabel.textAlignment = UITextAlignmentCenter;
        cashLabel.font = [UIFont systemFontOfSize:14.0];
        cashLabel.minimumFontSize = 10.0;
        cashLabel.adjustsFontSizeToFitWidth = YES;
        [cell.contentView addSubview:cashLabel];
        [cashLabel release];
    }
    
    NSUInteger row = [indexPath row];
    ClassifySales * aClassify = [classifyArray objectAtIndex:row + 1];
    
    UILabel * classifyLabel = (UILabel *)[cell viewWithTag:1];
    if ([GlobalUtil isIphone]) {
        //  iPhone
        classifyLabel.text = aClassify.classifyName;
    } else {
        //  iPad
        classifyLabel.text = [NSString stringWithFormat:@"   %@", aClassify.classifyName];
    }
    
    UILabel * demandLabel = (UILabel *)[cell viewWithTag:2];
    demandLabel.text = [NSString stringWithFormat:@"%.2f", [aClassify.demand floatValue] / 50];
    
    UILabel * orderLabel = (UILabel *)[cell viewWithTag:3];
    orderLabel.text = [NSString stringWithFormat:@"%.2f", [aClassify.order floatValue] / 50];
    
    UILabel * orderPercentLabel = (UILabel *)[cell viewWithTag:4];
    orderPercentLabel.text = [NSString stringWithFormat:@"%.2f%%", [aClassify.orderPercent floatValue]];
    
    UILabel * cashLabel = (UILabel *)[cell viewWithTag:5];
    cashLabel.text = [NSString stringWithFormat:@"%.2f", [aClassify.cash floatValue] / 10000];
    
    return cell;
}

#pragma mark -
#pragma mark Table View Delegate

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (isLoading) {
        return;
    }
    //  改变下拉刷新视图状态
    if (scrollView.contentOffset.y < -35) {
        [headerView changeArrowAndLabelState:1];
    } else if (scrollView.contentOffset.y < 0){
        [headerView changeArrowAndLabelState:0];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    //  下拉刷新
    if (scrollView.contentOffset.y < -35) {
        self.classifyTable.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
        [headerView changeArrowAndLabelState:2];
        [self loadClassifyData];
    }
}

#pragma mark - 
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"连接异常：%@", error);
    [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:@"请求数据发生异常。"];
    [self reloadTable];
}

- (void)connection:(NSURLConnection *)conn didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *) response;
    if((httpURLResponse.statusCode / 100) != 2){
        if (httpURLResponse.statusCode == 500 && connectionTimes < 3) {
            [connection cancel];
            [connection release];
            connection = nil;
            connectionTimes++;
            NSLog(@"第 %d 次连接", connectionTimes);
            [self loadClassifyData];
        } else {
            connectionTimes = 0;
            [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:[NSString stringWithFormat:@"网络响应异常：%d", httpURLResponse.statusCode]];
            [self reloadTable];
        }
        NSLog(@"网络响应异常: %@", [[httpURLResponse allHeaderFields] description]);
    }else{
        //  释放原先的data
        if (classifyData != nil) {
            [classifyData release];
            classifyData = nil;
        }
        //  接受到响应时，初始化salesAnalyseData
        classifyData = [[NSMutableData alloc] init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [classifyData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [self parseXMLData];
}

//  重写 NSXMLParser 协议
#pragma mark -
#pragma mark XMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    //  xml 全局key开始
    if([elementName isEqualToString:@"BrandSaleDetails"]){
        //  初始化数组
        if(classifyArray == nil){
            classifyArray = [[NSMutableArray alloc] init];
        }else{
            [classifyArray removeAllObjects];
        }
    }
    //  xml 每个对象key开始
    if([elementName isEqualToString:@"BrandSaleDetail"]){
        //  初始化销售分析对象
        classify = [[ClassifySales alloc]init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    //  保存各值
    aValue = [NSString stringWithFormat:@"%@", string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    //  根据值key，保存值
    if([elementName isEqualToString:@"brandPriceTypeCode"]){
        classify.typeCode = aValue;
    }else if([elementName isEqualToString:@"brandPriceTypeName"]){
        classify.classifyName = aValue;
    }else if([elementName isEqualToString:@"qtyDemand"]){
        classify.demand = aValue;
    }else if([elementName isEqualToString:@"qtyOrder"]){
        classify.order = aValue;
    }else if([elementName isEqualToString:@"qtyPrice"]){
        classify.cash = aValue;
    }else if([elementName isEqualToString:@"ratioOrderSatisfaction"]){
        classify.rate = aValue;
        //  将解析好的销售分析对象添加进数组中，在xml接口格式中，此key是最后一项，因此在这里添加
        [classifyArray addObject:classify];
        [classify release];
        classify = nil;
    }
    aValue = nil;
}
@end
