//
//  客户地图大头针类
//  CustomerAnnotation.h
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CustomerAnnotation : NSObject <MKAnnotation>{
    
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;  //  大头针经纬度
@property (nonatomic, retain) NSString * title;                     //  大头针标题
@property (nonatomic, retain) NSString * subtitle;                  //  大头针副标题
@property (nonatomic) int     index;   //  在数组中的位置

//  初始化大头针， 参数：大头针经纬度
- (id)initWithCoordinate2D:(CLLocationCoordinate2D) coordinate2D; 

@end
