//
//  进销存分析可视列视图控制器
//  PSIVisibilityViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-9.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@interface PSIVisibilityViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {

}

@property (nonatomic, retain) IBOutlet UITableView * visibilityTable; //  可视列 表视图
@property (nonatomic, retain) NSMutableArray * visibilityArray;       //  可视列 标识数组

- (IBAction)clickRightButton:(id)sender;   //  点击导航栏全选/全不选按钮
- (IBAction)clickSwitchButton:(id)sender;  //  点击UISwitch开关
- (void)initVisibilityArray;               //  初始化可视列 标识数组 1：可视 0 ：不可视

@end
