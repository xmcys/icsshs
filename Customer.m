//
//  客户信息类
//  Customer.m
//  icss-hs
//
//  Created by hsit on 11-10-19.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "Customer.h"


@implementation Customer

@synthesize licenceCode;  //  许可证号
@synthesize customerId;   //  客户代码
@synthesize name;         //  客户名称
@synthesize phone;        //  电话
@synthesize address;      //  地址
@synthesize level;        //  星级
@synthesize mgrName;      //  客户经理
@synthesize orderDate;    //  订货日
@synthesize distance;     //  距离
@synthesize latitude;     //  经度
@synthesize longitude;    //  纬度
@synthesize index;        //  在数组中的位置

- (void)dealloc{
    [latitude release];
    [longitude release];
    [customerId release];
    [name release];
    [phone release];
    [address release];
    [level release];
    [distance release];
    [licenceCode release];
    [mgrName release];
    [orderDate release];
    [super dealloc];
}

//  将属性转为字符串输出
- (NSString *)toString{
    return [NSString stringWithFormat:@"\nCustomer Info:\nLatitude : %@\nLongitude : %@\nIndex: %d\nLicenceCode : %@\nCustomerID : %@\nName : %@\nPhone : %@\nAddress : %@\n\nLevel : %@\nDistance : %@\nMgrName : %@\nOrderDate : %@\n", latitude, longitude, index, licenceCode, customerId, name, phone, address, level, distance, mgrName, orderDate];
}

//  返回属性数量
+ (NSInteger)propertyCount{
    return 11 - 3; //  除去 ID、经度、纬度
}

@end
