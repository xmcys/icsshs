//
//  分类饼图视图控制器
//  RTPieChartViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "RTPieChartViewController.h"
#import "ClassifySales.h"


@implementation RTPieChartViewController

@synthesize graphView;  //  绘图区底层视图

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [graphView release];
    [graph release];
    [orderArray release];
    [super dealloc];
}

- (void)viewDidUnload
{
    graphView = nil;
    orderArray = nil;
    graph = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  设置订单量数组
- (void)setOrderArray:(NSArray *)array{
    //  初始化订单量数组
    if (orderArray == nil) {
        orderArray = [[NSMutableArray alloc] init];
    } else {
        [orderArray removeAllObjects];
    }
    //  添加数据
    for (int i = 1; i < [array count] - 1; i++) {
        ClassifySales * classify = [array objectAtIndex:i];
        [orderArray addObject:[NSNumber numberWithDouble:[classify.order doubleValue] / 50]];
    }
    ClassifySales * classify = [array objectAtIndex:[array count] - 1];
    maxValue = [classify.order floatValue] / 50;
}

//  绘制饼图
- (void)drawGraph{
    if (graph != nil) {
        //  刷新数据
        [graph release];
        graph = nil;
        
        for (UIView * subView in self.graphView.subviews) {
            [subView removeFromSuperview];
        }
    }
    
    // 创建CPTXYGraph对象，窗体大小为0
    graph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    
    // 为graph应用暗色主题
    CPTTheme * theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [graph applyTheme:theme];
    CPTGraphHostingView * hostingView = [[CPTGraphHostingView alloc] initWithFrame:self.graphView.bounds];
    [hostingView setHostedGraph:graph];
    [self.graphView addSubview:hostingView];
    [hostingView release];
    
    //  设置圆角
    graph.plotAreaFrame.cornerRadius = 10.0f;
    //  设置绘图区距离边界大小
    graph.paddingTop = 5.0f;
    graph.paddingBottom = 5.0f;
    graph.paddingLeft = 5.0f;
    graph.paddingRight = 5.0f;
    
    //  定义标题样式
    CPTMutableTextStyle * titleTextStyle = [CPTMutableTextStyle textStyle];
    titleTextStyle.fontSize = 16.0;
    titleTextStyle.color = [CPTColor cyanColor];
    graph.titleTextStyle = titleTextStyle;
    graph.title = @"各类烟订单量占总订单量比重";
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0f, -15.0f);
    
    //  设置坐标轴为空
    graph.axisSet = nil;
    
    //  定义颜色渐变模式
    CPTGradient * pieGradient = [[[CPTGradient alloc] init] autorelease];
    pieGradient.gradientType = CPTGradientTypeRadial;
    //  中心渐变
    pieGradient = [pieGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.0] atPosition:0.0];
    //  80%位置渐变
    pieGradient = [pieGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.4] atPosition:0.8];
    //  100位置渐变
    pieGradient = [pieGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.6] atPosition:1.0];
    
    //  定义饼图
    CPTPieChart * orderPieChart = [[CPTPieChart alloc] init];
    orderPieChart.dataSource = self;
    orderPieChart.delegate = self;
    //  定义半径
    orderPieChart.pieRadius = MIN(0.8 * (self.graphView.frame.size.height / 2 - 50), 
                             0.8 * (self.graphView.frame.size.width / 2 - 50));
    orderPieChart.identifier = @"orderPieChart";
    //  定义起始弧度
    orderPieChart.startAngle = M_PI_4;
    //  定义切片方向
    orderPieChart.sliceDirection = CPTPieDirectionCounterClockwise;
    //  定义渐变
    orderPieChart.overlayFill = [CPTFill fillWithGradient:pieGradient];
    [graph addPlot:orderPieChart];
    [orderPieChart release];
    
    //  定义图注文字样式
    CPTMutableTextStyle * legendTextStyle = [[[CPTMutableTextStyle alloc] init] autorelease];
    legendTextStyle.color = [CPTColor whiteColor];
    legendTextStyle.fontSize = 14.0;
    
    //  定义图注边框样式
    CPTMutableLineStyle * legendLineStyle = [[[CPTMutableLineStyle alloc] init] autorelease];
    legendLineStyle.lineWidth = 2.0;
    legendLineStyle.lineColor = [CPTColor whiteColor];
    
    //  定义图注
    CPTLegend * legend = [CPTLegend legendWithGraph:graph];
    //  图注列数
    legend.numberOfColumns = 2;
    legend.fill = [CPTFill fillWithColor:[CPTColor darkGrayColor]];
    legend.borderLineStyle = legendLineStyle;
    legend.cornerRadius = 5.0f;
    legend.textStyle = legendTextStyle;
    
    graph.legend = legend;
    graph.legendDisplacement = CGPointMake(-12, 12);
    graph.legendAnchor = CPTRectAnchorBottomRight;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - 
#pragma mark CPTPlot Data Source
- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot{
    return [orderArray count];
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSNumber *num;
    if (fieldEnum == CPTPieChartFieldSliceWidth) {
        //  返回切片大小
        num = [orderArray objectAtIndex:index];
    }
    else {
        return [NSNumber numberWithInt:index];
    }
    
    return num;
}

//  图注文字
-(NSString *)legendTitleForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index
{
    if (index == 0) {
        return @"一类烟";
    } else if (index == 1) {
        return @"二类烟";
    } else if (index == 2) {
        return @"三类烟";
    } else if (index == 3) {
        return @"四类烟";
    } else if (index == 4) {
        return @"五类烟";
    } else {
        return @"待定";
    }
}

//  饼图各色块周边文字提示
-(CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)index
{
    //  定义文字样式
    static CPTMutableTextStyle *whiteText = nil;
    if ( !whiteText ) {
        whiteText = [[CPTMutableTextStyle alloc] init];
        whiteText.color = [CPTColor whiteColor];
    }
    
    //  定义显示文字
    NSString * text = nil;
    float number = [[orderArray objectAtIndex:index] floatValue];
    if (index == 0) {
        text = [NSString stringWithFormat:@"%.1f%%\n%.2f件\n一类烟", number / maxValue * 100, number];
    } else if (index == 1) {
        text = [NSString stringWithFormat:@"%.1f%%\n%.2f件\n二类烟", number / maxValue * 100, number];
    } else if (index == 2) {
        text = [NSString stringWithFormat:@"%.1f%%\n%.2f件\n三类烟", number / maxValue * 100, number];
    } else if (index == 3) {
        text = [NSString stringWithFormat:@"%.1f%%\n%.2f件\n四类烟", number / maxValue * 100, number];
    } else if (index == 4) {
        text = [NSString stringWithFormat:@"%.1f%%\n%.2f件\n五类烟", number / maxValue * 100, number];
    } else {
        text = [NSString stringWithFormat:@"%.1f%%\n%.2f件\n待定", number / maxValue * 100, number];
    }
    
    CPTTextLayer *newLayer = [[[CPTTextLayer alloc] initWithText:text style:whiteText] autorelease];
    return newLayer;
}

#pragma mark -
#pragma mark CPTPieChart Delegate
//  各色块选中事件 ， 暂无用处。
- (void)pieChart:(CPTPieChart *)plot sliceWasSelectedAtRecordIndex:(NSUInteger)index{
    NSLog(@"%d", index);
}

@end
