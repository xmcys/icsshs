// 
//  进销存数据库操作类 实现
//  BrandStockSaleDatabase.m
//  icss-hs
//
//  Created by hsit on 11-10-11.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "BrandStockSaleDatabase.h"
#import "GlobalUtil.h"
#import "icss_hsAppDelegate.h"
#import "BrandStockSale.h"
#import "Database.h"

@implementation BrandStockSaleDatabase

//  创建进销存分析表
- (BOOL)createTable{
    //  使用 if not exists 语句，判断表是否存在
    NSString * createSQL = @"create table if not exists BRAND_STOCK_SALES (id integer primary key, brand_id text, brand_name text, qty_stock_begin text, qty_stock_buy text, qty_draw text, qty_stock_end text, qty_stock_sale text, qty_stock_withdraw text, qty_stock_tranout text, qty_stock_waste text)";
    return [[icss_hsAppDelegate appDelegate].database createTable:createSQL tableNameWithCHN:@"进销存分析"];
}

//  保存数据
//  array : 所有进销存数据，date : 查询周期
- (BOOL)saveData:(NSMutableArray *)array searchDate:(NSString *)date{
    BOOL succeed = NO;
    //  依次构造sql语句，并调用Database类的saveData方法保存数据
    for (int i = 0; i < [array count]; i++) {
        BrandStockSale * item = [array objectAtIndex:i];
        NSString * saveSql = [[NSString alloc] initWithFormat:@"insert into BRAND_STOCK_SALES values(%d, '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", i, item.brandId, item.brandName, item.qtyStockBeginOriginal, item.qtyStockBuyOriginal, item.qtyStockDrawOriginal, item.qtyStockEndOriginal, item.qtyStockSaleOriginal, item.qtyStockWithdrawOriginal, item.qtyStockTranOutOriginal, item.qtyStockWasteOriginal];
        succeed = [[icss_hsAppDelegate appDelegate].database saveData:saveSql tableNameWithCHN:@"进销存分析"]; 
        [saveSql release];
    }
    //  构造一条用于记录查询周期的虚拟进销存记录（为了节约一张表）
    NSString * saveSql = [[NSString alloc] initWithFormat:@"insert into BRAND_STOCK_SALES values(%d, '%@', '', '', '', '', '', '', '', '', '')", [array count], date];
    succeed = [[icss_hsAppDelegate appDelegate].database saveData:saveSql tableNameWithCHN:@"进销存分析"]; 
    [saveSql release];
    return succeed;
}

//  删除所有进销存数据
- (BOOL)deleteAllData{
    NSString * deleteSql = @"delete from BRAND_STOCK_SALES";
    return [[icss_hsAppDelegate appDelegate].database deleteData:deleteSql tableNameWithCHN:@"进销存分析"];
}

//  查询数据
//  返回所有进销存数据，包含一条组成为 “查询周期”“空”“空”……的进销存记录
- (NSMutableArray *)queryAllData{
    NSString * querySql = @"select * from BRAND_STOCK_SALES";
    NSMutableArray * array = [[[[icss_hsAppDelegate appDelegate].database queryData:querySql tableNameWithCHN:@"进销存分析" columnsCount:11] copy] autorelease];
    NSMutableArray * returnArray = [[[NSMutableArray alloc] init] autorelease];
    // 重新将NSArray对象数组封装成BrandStockSale对象数组
    for (int i = 0; i < [array count]; i++) {
        BrandStockSale * bss = [[BrandStockSale alloc] init];
        NSArray * item = [array objectAtIndex:i];
        bss.brandId = [item objectAtIndex:0];
        bss.brandName = [item objectAtIndex:1];
        bss.qtyStockBegin = [item objectAtIndex:2];
        bss.qtyStockBeginOriginal = [item objectAtIndex:2];
        bss.qtyStockBuy = [item objectAtIndex:3];
        bss.qtyStockBuyOriginal = [item objectAtIndex:3];
        bss.qtyStockDraw = [item objectAtIndex:4];
        bss.qtyStockDrawOriginal = [item objectAtIndex:4];
        bss.qtyStockEnd = [item objectAtIndex:5];
        bss.qtyStockEndOriginal = [item objectAtIndex:5];
        bss.qtyStockSale = [item objectAtIndex:6];
        bss.qtyStockSaleOriginal = [item objectAtIndex:6];
        bss.qtyStockWithdraw = [item objectAtIndex:7];
        bss.qtyStockWithdrawOriginal = [item objectAtIndex:7];
        bss.qtyStockTranOut = [item objectAtIndex:8];
        bss.qtyStockTranOutOriginal = [item objectAtIndex:8];
        bss.qtyStockWaste = [item objectAtIndex:9];
        bss.qtyStockWasteOriginal = [item objectAtIndex:9];
        [returnArray addObject:bss];
        [bss release];
    }
    return returnArray;
}


@end
