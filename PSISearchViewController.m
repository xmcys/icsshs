//
//  进销存分析搜索视图控制器
//  PSISearchViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-9.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "PSISearchViewController.h"
#import "GlobalUtil.h"
#import "icss_hsAppDelegate.h"


@implementation PSISearchViewController

@synthesize bottomView;     //  表视图的footerView
@synthesize searchTable;    //  显示搜索框的表视图
@synthesize btnReset;       //  重置按钮
@synthesize btnSearch;      //  查询按钮
@synthesize datePicker;     //  自定义选取器（日期）
@synthesize startDateField; //  开始日期控件
@synthesize endDateField;   //  结束日期控件
@synthesize nameField;      //  卷烟名称控件

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [bottomView release];
    [searchTable release];
    [btnReset release];
    [btnSearch release];
    [datePicker release];
    [startDateField release];
    [endDateField release];
    [nameField release];
    [dateArray release];
    [super dealloc];
}

- (void)viewDidUnload
{
    bottomView = nil;
    searchTable = nil;
    btnReset = nil;
    btnSearch = nil;
    datePicker = nil;
    startDateField = nil;
    endDateField = nil;
    nameField = nil;
    dateArray = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  关闭键盘
- (void)closeKeyBoard{
    //  重置所有文本框控件的响应状态（关闭键盘）
    [nameField resignFirstResponder];
    [startDateField resignFirstResponder];
    [endDateField resignFirstResponder];
}

//  初始化年月日数组
- (void)initDateArray{
    dateArray = [[NSMutableArray alloc] init];
    NSArray * year = [[NSArray alloc] initWithObjects:@"2000", @"2001", @"2002", @"2003", @"2004", @"2005", @"2006", @"2007", @"2008", @"2009", @"2010", @"2011", @"2012", @"2013", @"2014", nil];
    [dateArray addObject:year];
    [year release];
    NSArray * month = [[NSArray alloc] initWithObjects:@"01", @"02", @"03", @"04", @"05", @"06", @"07", @"08", @"09", @"10", @"11", @"12", nil];
    [dateArray addObject:month];
    [month release];
    NSArray * day = [[NSArray alloc] initWithObjects:@"01", @"02", @"03", @"04", @"05", @"06", @"07", @"08", @"09", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30", @"31", nil];
    [dateArray addObject:day];
    [day release];
}

//  设置日期的颜色：黑色：普通状态  蓝色：正在编辑   红色：非法日期
- (void)setDateFieldColor{
    //  如果开始日期 大于 结束日期，设置开始日期为红色
    if ([startDateField.text intValue] > [endDateField.text intValue]) {
        startDateField.textColor = [UIColor redColor];
    } else {
        startDateField.textColor = [UIColor blueColor];
    }
    //  根据选中的行设置日期颜色
    if(fieldIndex == 0){
        if(startDateField.textColor != [UIColor redColor]){
            startDateField.textColor = [UIColor blueColor];
        }
        endDateField.textColor = [UIColor blackColor];
    }else if(fieldIndex == 1){
        if(startDateField.textColor != [UIColor redColor]){
            startDateField.textColor = [UIColor blackColor];
        }
        endDateField.textColor = [UIColor blueColor];
    }else{
        if(startDateField.textColor != [UIColor redColor]){
            startDateField.textColor = [UIColor blackColor];
        }
        endDateField.textColor = [UIColor blackColor];
    }
}

//  根据日期选取器选择的值和年月日数组，设置开始和结束日期
- (void)setDateField{
    //  年月日数组
    NSArray * yearArray = [dateArray objectAtIndex:0];
    NSArray * monthArray = [dateArray objectAtIndex:1];
    NSArray * dayArray = [dateArray objectAtIndex:2];
    
    //  选取器选中的值
    NSString * year = [yearArray objectAtIndex:[datePicker selectedRowInComponent:0]];
    NSString * month = [monthArray objectAtIndex:[datePicker selectedRowInComponent:1]];
    NSString * day = [dayArray objectAtIndex:[datePicker selectedRowInComponent:2]];
    //  设置开始日期 或 结束日期 控件的文本值
    if (fieldIndex == 0) {
        startDateField.text = [NSString stringWithFormat:@"%@%@%@", year, month, day];
    } else if (fieldIndex == 1){
        endDateField.text = [NSString stringWithFormat:@"%@%@%@", year, month, day];
    }
    //  设置日期颜色
    [self setDateFieldColor];
}

//  当点击开始（结束）日期控件时，设置选取器的值
- (void)setPickerValue:(NSString *)string{
    //  解析年
    NSString * year = [string substringToIndex:4];
    //  解析月
    NSRange range = NSRangeFromString(string);
    range.location = 4;
    range.length = 2;
    NSString * month = [string substringWithRange:range];
    //  解析日
    NSString * day = [string substringFromIndex:6];
    //  获取年数组
    NSArray * yearArray = [dateArray objectAtIndex:0];
    
    NSInteger yearIndex = 0;
    NSInteger monthIndex = [month intValue] - 1;
    NSInteger dayIndex = [day intValue] - 1;
    //  判断改年值在数组中的位置
    for (int i = 0; i < [yearArray count]; i++) {
        if ([[yearArray objectAtIndex:i] isEqualToString:year]) {
            yearIndex = i;
            break;
        }
    }
    //  设置选取器的值
    [datePicker selectRow:yearIndex inComponent:0 animated:YES];
    [datePicker selectRow:monthIndex inComponent:1 animated:YES];
    [datePicker selectRow:dayIndex inComponent:2 animated:YES];
}

//  点击重置按钮
- (IBAction)clickReset{
    startDateField.text = [GlobalUtil initSearchDate:0];
    endDateField.text = [GlobalUtil initSearchDate:1];
    nameField.text = @"";
    fieldIndex = -1;
    [self closeKeyBoard];
    [self setDateFieldColor];
    [datePicker selectRow:0 inComponent:0 animated:YES];
    [datePicker selectRow:0 inComponent:1 animated:YES];
    [datePicker selectRow:0 inComponent:2 animated:YES];
}

//  点击搜索按钮
- (IBAction)clickSearch{
    if ([startDateField.text intValue] > [endDateField.text intValue]) {
        [GlobalUtil showAlertWithTitle:@"格式错误" andMessage:@"开始日期不能大于结束日期"];
        return;
    }
    if ([GlobalUtil isHasNumberOnly:nameField.text ]) {
        [GlobalUtil showAlertWithTitle:@"格式错误" andMessage:@"卷名名称不能由纯数字组成"];
        return;
    }
    //  去除卷烟名称前后空格
    nameField.text = [nameField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    //  将搜索标识设为1，返回时，根据该值判断是否进行搜索。  1 搜索   0  不搜索
    btnSearch.tag = 1;
    //  判断设备是否iPhone
    if ([GlobalUtil isIphone]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        // iPad
        // 调用全局应用委托中的搜索方法
        [[icss_hsAppDelegate appDelegate] rearchPurcheaseSalesInventoryData];
    }
}


#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated{
    btnSearch.tag = 0;
}

- (void)viewWillDisappear:(BOOL)animated{
    [self closeKeyBoard];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    bottomView.backgroundColor = searchTable.backgroundColor;
    //  初始化开始日期控件 ： 不可编辑，只能通过选取器改变值
    startDateField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 200, 44)];
    startDateField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    startDateField.enabled = NO;
    //  初始化结束日期控件 ： 不可编辑，只能通过选取器改变值
    endDateField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 200, 44)];
    endDateField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    endDateField.enabled = NO;
    //  初始化卷烟名称控件
    nameField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 200, 44)];
    nameField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    //  初始化日期数组
    [self initDateArray];
    //  将选中行设为 - 1
    fieldIndex = -1;
    self.navigationItem.title = @"进销存查询";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//  重写 UITableView 数据源
#pragma mark -
#pragma mark Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSUInteger row = [indexPath row];
    if (cell == nil) {
        //  初始化单元格，设置单元格样式
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0];
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        if (row == 0) {
            //  将开始日期控件添加进第一行
            cell.textLabel.text = @"开始日期：";
            cell.accessoryView = startDateField;
        } else if (row == 1) {
            //  将开始日期控件添加进第二行
            cell.textLabel.text = @"结束日期：";
            cell.accessoryView = endDateField;
        } else{
            //  将开始日期控件添加进第三行
            cell.textLabel.text = @"卷烟名称：";
            cell.accessoryView = nameField;
            nameField.delegate = self;
        }
    }
    //  初始化各文本控件的值
    if (row == 0) {
        startDateField.text = [GlobalUtil initSearchDate:0] ;
    } else if (row == 1) {
        endDateField.text = [GlobalUtil initSearchDate:1];
    } else{
        nameField.text = @"";
    }
    
    return cell;
}

//  重写 UITableView 协议
#pragma mark - 
#pragma mark Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self closeKeyBoard];
    //  记录选中行
    fieldIndex = [indexPath row];
    //  设置选取器的值
    if (fieldIndex == 0) {
        [self setPickerValue:startDateField.text];
    } else if (fieldIndex == 1){
        [self setPickerValue:endDateField.text];
    }
    //  设置日期颜色
    [self setDateFieldColor];
}

//  重写 UIPickerView 数据源
#pragma mark - 
#pragma mark Picker View Data Source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return [dateArray count];
}

//  返回各组件长度
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    //  年
    if (component == 0) {
        NSArray * item = [dateArray objectAtIndex:0];
        return [item count];
    } else if (component == 1) {
        //  月
        return 12;
    } else {
        //  日
        return 31;
    }
}

//  重写 UIPickerView 协议
#pragma mark - 
#pragma mark Picker View Delegate
//  设置各组件值
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSArray * item = nil;
    //  年
    if (component == 0) {
        item = [dateArray objectAtIndex:0];
    } else if (component == 1) {
        //  月
        item = [dateArray objectAtIndex:1];
    } else {
        //  日
        item = [dateArray objectAtIndex:2];
    }
    return [item objectAtIndex:row];
}

//  选取器选中事件
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    //  引用年月日各数组
    NSArray * yearArray = [dateArray objectAtIndex:0];
    NSArray * monthArray = [dateArray objectAtIndex:1];
    NSArray * dayArray = [dateArray objectAtIndex:2];
    
    //  获得各个选中行在各数组中对应的值
    NSInteger year = [[yearArray objectAtIndex:[pickerView selectedRowInComponent:0]] intValue];
    NSInteger month = [[monthArray objectAtIndex:[pickerView selectedRowInComponent:1]] intValue];
    NSInteger day = [[dayArray objectAtIndex:[pickerView selectedRowInComponent:2]] intValue];
    
    //  闰年、2月、小月判断
    if (month == 2 && [GlobalUtil isLeapYear:year] && day > 29) {
        [pickerView selectRow:28 inComponent:2 animated:YES];
    } else if (month == 2 && ![GlobalUtil isLeapYear:year] && day > 28){
        [pickerView selectRow:27 inComponent:2 animated:YES];
    } else if ([GlobalUtil isSmallMonth:month] && day > 30){
        [pickerView selectRow:29 inComponent:2 animated:YES];
    }
    
    //  延迟设置开始（结束）日期控件的值（上面那个年月日判断会引起动画，动画需要时间，因此延迟，不然会不准确）
    [self performSelector:@selector(setDateField) withObject:nil afterDelay:0.3];
}

//  重写 UITextField 协议
#pragma mark -
#pragma mark TextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //  将选中行设为2（卷烟名称）
    fieldIndex = 2;
    [self setDateFieldColor];
}

@end
