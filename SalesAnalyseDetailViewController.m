//
//  销售分析 详情视图
//  SalesAnalyseDetailViewController.m
//  icss-hs
//
//  Created by hsit on 11-9-23.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "SalesAnalyseDetailViewController.h"
#import "SADetailTableViewController.h"
#import "SADetailCurveViewController.h"
#import "SaleAnalyse.h"
#import "GlobalUtil.h"


@implementation SalesAnalyseDetailViewController

@synthesize contentView, pageControl;

@synthesize viewControllers;
@synthesize firstBottomView, secondBottomView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [contentView release];
    [pageControl release];
    [viewControllers release];
    [firstBottomView release];
    [secondBottomView release];
    [super dealloc];
}

- (void)viewDidUnload
{
    self.contentView = nil;
    self.pageControl = nil;
    self.viewControllers = nil;
    firstBottomView = nil;
    secondBottomView = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  在视图即将消失时
- (void)viewWillDisappear:(BOOL)animated{
    //  取消 单个卷烟详情视图的数据加载
    SADetailTableViewController * sad = (SADetailTableViewController *)[viewControllers objectAtIndex:0];
    [sad cancelLoading];
}

//  初始化卷烟信息，并加载数据
- (void)initCigaretteInfo:(SaleAnalyse *)sa startDate:(NSString *)start endDate:(NSString *)end{
    self.navigationItem.title = sa.brandName;
    SADetailTableViewController * sad = (SADetailTableViewController *)[viewControllers objectAtIndex:0];
    SADetailCurveViewController * curve = (SADetailCurveViewController *)[viewControllers objectAtIndex:1];
    //  设置单个卷烟信息
    [sad initCigaretteInfo:sa startDate:start endDate:end curveViewController:curve];
    //  加载单个卷烟销售分析数据
    [sad loadData];
}

//  加载各页视图
- (void)loadViewWithPage:(int)page{
    if(page < 0 || page >= PagesCount){
        return;
    }
    //  初始化各视图
    UIViewController * controller = [viewControllers objectAtIndex:page];
    if((NSNull *)controller == [NSNull null]){
        if(page == 0){
            //  详情视图
            controller = [[SADetailTableViewController alloc]init];
        }else{
            //  曲线视图
            controller = [[SADetailCurveViewController alloc]init];
        }
        [viewControllers replaceObjectAtIndex:page withObject:controller];
        [controller release];
    }
    //  设置各视图的父层
    if (controller.view.superview == nil){
        CGRect frame = contentView.frame;
        frame.origin.x = contentView.frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [contentView addSubview:controller.view];
    }
}

//  分页事件 
- (IBAction)pageChanged:(id)sender{
    pageControlUsed = YES;
    CGRect frame = contentView.frame;
    //  计算偏移量
    frame.origin.x = frame.size.width * pageControl.currentPage;
    frame.origin.y = 0;
    //  设置偏移
    [contentView scrollRectToVisible:frame animated:YES];
}

//  如果是iPhone，初始化视图
- (void)initForIPhone{
    //  起始页
    pageControl.currentPage = 0;
    //  总页数
    pageControl.numberOfPages = 2;
    //  重设内容视图大小
    CGRect frame = contentView.frame;
    contentView.contentSize = CGSizeMake(frame.size.width * PagesCount, frame.size.height);
    //  初始化视图数组
    self.viewControllers = [[NSMutableArray alloc] init];
    for(int i = 0; i < PagesCount; i++){
        [self.viewControllers addObject:[NSNull null]];
    }
    //  加载第1、第2页视图
    [self loadViewWithPage:0];
    [self loadViewWithPage:1];
}

//  如果是iPad，初始化视图
- (void)initForIPad{
    //  初始化视图数组
    self.viewControllers = [[NSMutableArray alloc] init];
    //  初始化单个卷烟销售分析详情视图，并加入视图数组
    SADetailTableViewController * sadTableView = [[SADetailTableViewController alloc] init];
    sadTableView.bottomView.backgroundColor = [UIColor clearColor];
    [viewControllers addObject:sadTableView];
    [sadTableView release];
    //  初始化单个卷烟销售分析曲线视图，并加入视图数组
    SADetailCurveViewController * curveView = [[SADetailCurveViewController alloc] init];
    curveView.bottomView.backgroundColor = [UIColor clearColor];
    [viewControllers addObject:curveView];
    [curveView release];
    //  将个视图放入相应的底层视图
    [firstBottomView addSubview:((UIViewController *)[viewControllers objectAtIndex:0]).view];
    [secondBottomView addSubview:((UIViewController *)[viewControllers objectAtIndex:1]).view];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([GlobalUtil isIphone]) {
        // iPhone
        [self initForIPhone];
    } else {
        //  iPad
        [self initForIPad];
    }
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//  重写 UIScrollView 协议
#pragma mark -
#pragma mark ScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    pageControlUsed = NO;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    pageControlUsed = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //  如果是使用分页控件滚动的，则跳过。
    if(pageControlUsed){
        return;
    }
    //  计算下一页位置，并设置下一页
    CGFloat pageWidth = contentView.frame.size.width;
    int page = floor((contentView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
    //  加载上一页、本页、下一页视图
    [self loadViewWithPage:page - 1];
    [self loadViewWithPage:page];
    [self loadViewWithPage:page + 1];
}

@end
