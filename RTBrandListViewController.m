//
//  卷烟列表视图控制器
//  RTBrandListViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "RTBrandListViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "GlobalUtil.h"
#import "RefreshHeaderView.h"
#import "LoadMoreFooterView.h"
#import "SaleAnalyse.h"
#import "GlobalUtil.h"
#import "icss_hsAppDelegate.h"


@implementation RTBrandListViewController

@synthesize brandListTable;  //  卷烟列表表视图
@synthesize searchBar;       //  卷烟名称搜索栏
@synthesize delegate;        //  卷烟列表协议委托
@synthesize spinner;         //  加载指示器
@synthesize timeLabel;       //  查询时间标签

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [brandListArray release];
    [brandListTable release];
    [searchBar release];
    [delegate release];
    [connection release];
    [spinner release];
    [brandListData release];
    [headerView release];
    [footerView release];
    [saleAnalyse release];
    [aValue release];
    [timeLabel release];
    [super dealloc];
}

- (void)viewDidUnload
{
    brandListTable = nil;
    brandListArray = nil;
    searchBar = nil;
    delegate = nil;
    connection = nil;
    spinner = nil;
    headerView = nil;
    footerView = nil;
    brandListData = nil;
    saleAnalyse = nil;
    aValue = nil;
    timeLabel = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  关闭键盘
- (void)closeKeyboard{
    [self.searchBar resignFirstResponder];
}

//  显示或隐藏搜索栏
- (void)showOrHideSerachBar{
    CATransition * animation = [CATransition animation];
    animation.duration = 0.3;
    if (self.searchBar.hidden) {
        //  显示搜索栏
        self.searchBar.hidden = NO;
        //  判断卷烟协议是否有实现，有实现则调用
        if ([delegate respondsToSelector:@selector(changeNavigationRightBarButtonState:style:)]) {
            //  更改查询按钮状态
            [delegate changeNavigationRightBarButtonState:@"关闭" style:UIBarButtonItemStyleDone];
        }
        [self.searchBar becomeFirstResponder];
    } else {
        //  隐藏搜素栏
        self.searchBar.hidden = YES;
        [self.searchBar resignFirstResponder];
        if ([delegate respondsToSelector:@selector(changeNavigationRightBarButtonState:style:)]) {
            [delegate changeNavigationRightBarButtonState:@"查询" style:UIBarButtonItemStylePlain];
        }
    }
    [[self.searchBar layer] addAnimation:animation forKey:@"animation"];
}

//  加载卷烟列表
- (void)loadBrandListData{
    //  检查网络
    if (![GlobalUtil isReachable]) {
        [GlobalUtil showAlertWithTitle:@"网络故障" andMessage:@"当前网络不可用"];
        //  重设表视图偏移量
        if (brandListArray == nil || [brandListArray count] == 0) {
            self.brandListTable.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
        }else{
            self.brandListTable.contentInset = UIEdgeInsetsZero;
        }
        //  更改下拉刷新视图状态
        [headerView changeArrowAndLabelState:0];
        return;
    }
    //  获取卷烟名称
    NSString * brandName = self.searchBar.text == nil? @"" : self.searchBar.text;
    if ([GlobalUtil isHasNumberOnly:brandName]) {
        self.searchBar.text = @"";
        brandName = @"";
    }
    isLoading = YES;
    loadCounts = 0;
    [spinner startAnimating];
    NSLog(@"BrandName : %@", brandName);
    if ([brandName length] == 0) {
        //  卷烟名称为空
        NSString * url = nil;
        if ([icss_hsAppDelegate appDelegate].useInsideNet) {
            url = [NSString stringWithFormat:@"http://10.188.50.238:9088/tmi/ws/rs/mobile/queryBrandSale/startPage/%d/endPage/%d?licence=123456789&key=25f9e794323b453885f5181f1b624d0b", startPage, startPage + pageSize -1];
        } else {
            url = [NSString stringWithFormat:@"http://xm.fj-tobacco.com:9088/tmi/ws/rs/mobile/queryBrandSale/startPage/%d/endPage/%d?licence=123456789&key=25f9e794323b453885f5181f1b624d0b", startPage, startPage + pageSize -1];
        }
        NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    } else {
        isOverLoad = YES;
        //  卷烟名称不为空
        NSString * url = nil;
        if ([icss_hsAppDelegate appDelegate].useInsideNet) {
            url = [NSString stringWithFormat:@"http://10.188.50.238:9088/tmi/ws/rs/mobile/queryBrandSaleName"];
        } else {
            url = [NSString stringWithFormat:@"http://xm.fj-tobacco.com:9088/tmi/ws/rs/mobile/queryBrandSaleName"];
        }
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"PUT"];
        //  设置名称参数
        [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
        NSString * name = [[NSString alloc] initWithFormat:@"<SqlStr><sqlStr>%@</sqlStr></SqlStr>", brandName];
        [request setHTTPBody:[name dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]];
        connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [name release];
    }
}

//  取消加载
- (void)cancelLoading{
    if (connection != nil) {
        [connection cancel];
        loadCounts = -1;
    }
    [connection release];
    connection = nil;
}

//  解析XML数据
- (void)parseXMLData{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:brandListData];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
    //  显示查询时间
    self.timeLabel.text = [[GlobalUtil getCurrentFullTime] substringToIndex:10];
    [self reloadTable];
}

//  刷新表视图
- (void)reloadTable{
    [connection release];
    connection = nil;
    isLoading = NO;
    [spinner stopAnimating];
    [self.brandListTable reloadData];
    if (isOverLoad) {
        //  重设表视图偏移量
        if ([brandListArray count] != 0) {
            self.brandListTable.contentInset = UIEdgeInsetsZero;
            [self.brandListTable setContentOffset:CGPointMake(0.0, 0.0)];
        }
        [headerView changeArrowAndLabelState:0];
        
    } else {
        //  重设上拉加载视图状态
        if (loadCounts != -1 && loadCounts < pageSize - 1) {
            [footerView changeArrowAndLabelState:3];
        } else {
            [footerView changeArrowAndLabelState:0];
        }
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    isLoading = NO;
    isOverLoad = YES;
    loadCounts = -1;
    startPage = 1;
    pageSize = 50;
    if ([GlobalUtil isIphone]) {
        //  iPhone
        self.searchBar.hidden = YES;
    } else {
        //  iPad
        self.searchBar.layer.cornerRadius = 10.0f;
    }
    //  初始化下拉刷新视图
    headerView = [[RefreshHeaderView alloc] initWithFrame:CGRectMake(0, -35, self.brandListTable.frame.size.width, 35)];
    [self.brandListTable addSubview:headerView];
    
    //  初始化上拉加载视图
    footerView = [[LoadMoreFooterView alloc] initWithFrame:CGRectMake(0, 0, self.brandListTable.frame.size.width, 35)];
    [self.brandListTable setTableFooterView:footerView];
    
    self.brandListTable.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
    connectionTimes = 0;
    [self loadBrandListData];
    
    self.brandListTable.separatorColor = [UIColor darkGrayColor];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [brandListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * identifier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //  卷烟名称
        UILabel * nameLabel = nil;
        //  需求量
        UILabel * demandLabel = nil;
        //  订单量
        UILabel * orderLabel = nil;
        //  金额
        UILabel * cashLabel = nil;
        //  满足率
        UILabel * satisfyLabel = nil;
        if ([GlobalUtil isIphone]) {
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
            demandLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 60, 30)];
            orderLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, 60, 30)];
            cashLabel = [[UILabel alloc] initWithFrame:CGRectMake(220, 0, 50, 30)];
            satisfyLabel = [[UILabel alloc] initWithFrame:CGRectMake(270, 0, 50, 30)];
        } else {
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 130, 30)];
            demandLabel = [[UILabel alloc] initWithFrame:CGRectMake(130, 0, 60, 30)];
            orderLabel = [[UILabel alloc] initWithFrame:CGRectMake(190, 0, 60, 30)];
            cashLabel = [[UILabel alloc] initWithFrame:CGRectMake(250, 0, 60, 30)];
            satisfyLabel = [[UILabel alloc] initWithFrame:CGRectMake(310, 0, 60, 30)];
        }
        //  卷烟名称
        nameLabel.textAlignment = UITextAlignmentLeft;
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.textColor = [UIColor lightTextColor];
        nameLabel.font = [UIFont systemFontOfSize:14.0];
        nameLabel.minimumFontSize = 10.0;
        nameLabel.adjustsFontSizeToFitWidth = YES;
        nameLabel.tag = 1;
        [cell.contentView addSubview:nameLabel];
        [nameLabel release];
        
        //  需求量
        demandLabel.textAlignment = UITextAlignmentCenter;
        demandLabel.backgroundColor = [UIColor clearColor];
        demandLabel.textColor = [UIColor lightTextColor];
        demandLabel.font = [UIFont systemFontOfSize:14.0];
        demandLabel.minimumFontSize = 10.0;
        demandLabel.adjustsFontSizeToFitWidth = YES;
        demandLabel.tag = 2;
        [cell.contentView addSubview:demandLabel];
        [demandLabel release];
        
        //  订单量
        orderLabel.textAlignment = UITextAlignmentCenter;
        orderLabel.backgroundColor = [UIColor clearColor];
        orderLabel.textColor = [UIColor lightTextColor];
        orderLabel.font = [UIFont systemFontOfSize:14.0];
        orderLabel.minimumFontSize = 10.0;
        orderLabel.adjustsFontSizeToFitWidth = YES;
        orderLabel.tag = 3;
        [cell.contentView addSubview:orderLabel];
        [orderLabel release];
        
        //  金额
        cashLabel.textAlignment = UITextAlignmentCenter;
        cashLabel.backgroundColor = [UIColor clearColor];
        cashLabel.textColor = [UIColor lightTextColor];
        cashLabel.font = [UIFont systemFontOfSize:14.0];
        cashLabel.minimumFontSize = 10.0;
        cashLabel.adjustsFontSizeToFitWidth = YES;
        cashLabel.tag = 4;
        [cell.contentView addSubview:cashLabel];
        [cashLabel release];
        
        //  满足率
        satisfyLabel.textAlignment = UITextAlignmentCenter;
        satisfyLabel.backgroundColor = [UIColor clearColor];
        satisfyLabel.textColor = [UIColor lightTextColor];
        satisfyLabel.font = [UIFont systemFontOfSize:14.0];
        satisfyLabel.minimumFontSize = 10.0;
        satisfyLabel.adjustsFontSizeToFitWidth = YES;
        satisfyLabel.tag = 5;
        [cell.contentView addSubview:satisfyLabel];
        [satisfyLabel release];
    }
    
    
    NSUInteger row = [indexPath row];
    SaleAnalyse * sa = [brandListArray objectAtIndex:row];
    
    UILabel * nameLabel = (UILabel *)[cell viewWithTag:1];
    if ([GlobalUtil isIphone]) {
        //  iPhone
        nameLabel.text = sa.brandName;
    } else {
        //  iPad
        nameLabel.text = [NSString stringWithFormat:@"  %@", sa.brandName];
    }
    
    UILabel * demandLabel = (UILabel *)[cell viewWithTag:2];
    demandLabel.text = [NSString stringWithFormat:@"%.2f", [sa.require floatValue] / 50];
    
    UILabel * orderLabel = (UILabel *)[cell viewWithTag:3];
    orderLabel.text = [NSString stringWithFormat:@"%.2f", [sa.order floatValue] / 50];
    
    UILabel * cashLabel = (UILabel *)[cell viewWithTag:4];
    cashLabel.text = [NSString stringWithFormat:@"%.2f", [sa.cash floatValue] / 10000];
    
    UILabel * satisfyLabel = (UILabel *)[cell viewWithTag:5];
    satisfyLabel.text = [NSString stringWithFormat:@"%.0f%%", [sa.rate floatValue] * 100];
    
    return cell;
}

#pragma mark -
#pragma mark Search Bar Delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)aSearchBar{
    // 去掉前后空格
    NSString * text = [aSearchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    if ([GlobalUtil isHasNumberOnly:text]) {
        [GlobalUtil showAlertWithTitle:@"格式错误" andMessage:@"卷烟名称不能由纯数字组成"];
        return;
    }
    self.searchBar.text = text;
    [self closeKeyboard];
    if ([GlobalUtil isIphone]) {
        //  iPhone
        [self showOrHideSerachBar];
    } else {
        //  iPad
    }
    
    //  取消当前数据加载
    if (isLoading) {
        [self cancelLoading];
    }
    //  加载数据
    self.brandListTable.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
    [self loadBrandListData];
}

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (isLoading) {
        return;
    }
    //  根据拖动距离，改变下拉刷新视图
    if (scrollView.contentOffset.y < -35) {
        [headerView changeArrowAndLabelState:1];
    } else if (scrollView.contentOffset.y < 0){
        [headerView changeArrowAndLabelState:0];
    }
    //  计算内容窗体和表窗体差距
    CGFloat offsetY = self.brandListTable.contentSize.height - self.brandListTable.frame.size.height;
    if (offsetY < 0) {
        offsetY = 0;
    }
    //  根据拖动距离，改变上拉刷新视图
    if (loadCounts != -1 && loadCounts < pageSize) {
        [footerView changeArrowAndLabelState:3];
    } else if (scrollView.contentOffset.y > 35 + offsetY) {
        [footerView changeArrowAndLabelState:1];
    } else {
        [footerView changeArrowAndLabelState:0];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    //  下拉刷新
    if (scrollView.contentOffset.y < -35) {
        isOverLoad = YES;
        startPage = 1;
        self.brandListTable.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
        [headerView changeArrowAndLabelState:2];
        [self loadBrandListData];
    }
    CGFloat offsetY = self.brandListTable.contentSize.height - self.brandListTable.frame.size.height;
    if (offsetY < 0) {
        offsetY = 0;
    }
    //  上拉加载
    if (loadCounts != -1 && loadCounts == pageSize &&
        scrollView.contentOffset.y > 35 + offsetY) {
        [footerView changeArrowAndLabelState:2];
        startPage += pageSize;
        isOverLoad = NO;
        [self loadBrandListData];
        NSLog(@"123");
    }
}

#pragma mark - 
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"连接异常：%@", error);
    [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:@"请求数据发生异常。"];
    [self reloadTable];
}

- (void)connection:(NSURLConnection *)conn didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *) response;
    if((httpURLResponse.statusCode / 100) != 2){
        if (httpURLResponse.statusCode == 500 && connectionTimes < 3) {
            [connection cancel];
            [connection release];
            connection = nil;
            connectionTimes++;
            NSLog(@"第 %d 次连接", connectionTimes);
            [self loadBrandListData];
        } else {
            connectionTimes = 0;
            [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:[NSString stringWithFormat:@"网络响应异常：%d", httpURLResponse.statusCode]];
            [self reloadTable];
        }
        NSLog(@"网络响应异常: %@", [[httpURLResponse allHeaderFields] description]);
    }else{
        //  释放原先的data
        if (brandListData != nil) {
            [brandListData release];
            brandListData = nil;
        }
        //  接受到响应时，初始化salesAnalyseData
        brandListData = [[NSMutableData alloc] init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [brandListData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [self parseXMLData];
}

//  重写 NSXMLParser 协议
#pragma mark -
#pragma mark XMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    //  xml 全局key开始
    if([elementName isEqualToString:@"BrandSaleDetails"]){
        //  初始化数组
        if(brandListArray == nil){
            brandListArray = [[NSMutableArray alloc] init];
        }
        //  如果是刷新，清空当前数据
        if(isOverLoad){
            [brandListArray removeAllObjects];
        }
    }
    //  xml 每个对象key开始
    if([elementName isEqualToString:@"BrandSaleDetail"]){
        //  初始化销售分析对象
        saleAnalyse = [[SaleAnalyse alloc]init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    //  保存各值
    aValue = [NSString stringWithFormat:@"%@", string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    //  根据值key，保存值
    if([elementName isEqualToString:@"brandId"]){
        saleAnalyse.brandId = aValue;
    }else if([elementName isEqualToString:@"brandName"]){
        saleAnalyse.brandName = aValue;
    }else if([elementName isEqualToString:@"qtyDemand"]){
        saleAnalyse.require = aValue;
    }else if([elementName isEqualToString:@"qtyOrder"]){
        saleAnalyse.order = aValue;
    }else if([elementName isEqualToString:@"qtyPrice"]){
        saleAnalyse.cash = aValue;
    }else if([elementName isEqualToString:@"ratioOrderSatisfaction"]){
        saleAnalyse.rate = aValue;
        //  将解析好的销售分析对象添加进数组中，在xml接口格式中，此key是最后一项，因此在这里添加
        [brandListArray addObject:saleAnalyse];
        loadCounts++;
        [saleAnalyse release];
        saleAnalyse = nil;
    }
    aValue = nil;
}

@end
