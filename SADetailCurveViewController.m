//
//  销售分析曲线图视图控制器
//  SADetailCurveViewController.m
//  icss-hs
//
//  Created by hsit on 11-9-23.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "SADetailCurveViewController.h"
#import "SaleAnalyse.h"
#import "GlobalUtil.h"

@implementation SADetailCurveViewController

@synthesize dateLabel, rateLabel, requireLabel, orderLabel, cashLabel, spinner;
@synthesize graphView, bottomView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [dateLabel release];
    [rateLabel release];
    [requireLabel release];
    [orderLabel release];
    [cashLabel release];
    [spinner release];
    [requireAnnotation release];
    [requireAxis release];
    [orderAxis release];
    [orderAnnotation release];
    [graphView release];
    [dateAxis release];
    [SAArray release];
    [bottomView release];
    [super dealloc];
}

- (void)viewDidUnload
{
    self.dateLabel = nil;
    self.rateLabel = nil;
    self.requireLabel = nil;
    self.orderLabel = nil;
    self.cashLabel = nil;
    self.spinner = nil;
    self.graphView = nil;
    requireAnnotation = nil;
    requireAxis = nil;
    orderAnnotation = nil;
    orderAxis = nil;
    dateAxis = nil;
    SAArray = nil;
    bottomView = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  设置各标签的值
- (void)initLabelsWithDate:(NSString *)date require:(NSString *)require order:(NSString *)order cash:(NSString *)cash rate:(NSString *)rate{
    self.dateLabel.text = date;
    self.requireLabel.text = require;
    self.orderLabel.text = order;
    self.cashLabel.text = cash;
    self.rateLabel.text = rate;
}

//  设置坐标数组  参数 ： 销售分析对象数组
- (void)setAxisDatas:(NSArray *)array{
    
    if (requireAxis == nil) {
        requireAxis = [[NSMutableArray alloc] init];
    } else {
        [requireAxis removeAllObjects];
    }
    
    if (orderAxis == nil) {
        orderAxis = [[NSMutableArray alloc] init];
    } else {
        [orderAxis removeAllObjects];
    }
    
    if (dateAxis == nil) {
        dateAxis = [[NSMutableArray alloc] init];
    } else {
        [dateAxis removeAllObjects];
    }
    
    double maxValueY = 0.0;
    
    SAArray = [array copy];
    for(int i = 0; i < [array count]; i++){
        SaleAnalyse * item = [array objectAtIndex:i];
        
        //  添加日期数组
        [dateAxis addObject:item.date];
        
        //  设置需求量坐标数组
        id x1 = [NSDecimalNumber numberWithDouble:i];
        id y1 = [NSDecimalNumber numberWithDouble:[item.require doubleValue]];
        [requireAxis addObject:[NSDictionary dictionaryWithObjectsAndKeys:x1, @"x", y1, @"y", nil]];
        if(maxValueY < [item.require doubleValue]){
            maxValueY = [item.require doubleValue];
        }
        
        //  设置订单量坐标数组
        id x2 = [NSDecimalNumber numberWithDouble:i];
        id y2 = [NSDecimalNumber numberWithDouble:[item.order doubleValue]];
        [orderAxis addObject:[NSDictionary dictionaryWithObjectsAndKeys:x2, @"x", y2, @"y", nil]];
        if(maxValueY < [item.order doubleValue]){
            maxValueY = [item.order doubleValue];
        }
    }
    
    //  根据日期数组的对象数量设置x轴的最大值
    xFloat = [dateAxis count];
    
    //  根据订单量/需求量的最大值设置y轴的最大值
    maxValueY = maxValueY * 1.1;
    
    if(maxValueY < 10){
        yFloat = 10.0f;
    }else{
        yFloat = maxValueY;
    }
}


- (void)drawGraph{
    if (graph != nil) {
        //  刷新数据
        [graph release];
        graph = nil;
        
        if (requireAnnotation != nil) {
            [requireAnnotation release];
            requireAnnotation = nil;
        }
        
        if (orderAnnotation != nil) {
            [orderAnnotation release];
            orderAnnotation = nil;
        }
        
        for (UIView * subView in self.graphView.subviews) {
            [subView removeFromSuperview];
        }
    }
    
    // 创建CPTXYGraph对象，窗体大小为0
    graph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    
    // 为graph应用暗色主题
    CPTTheme * theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [graph applyTheme:theme];
    
    // 将视图上用于显示图形的UIView控件转换为CPTGraphHostingView类型，并将graph加入其中
    CPTGraphHostingView * hostingView = [[[CPTGraphHostingView alloc] initWithFrame:self.graphView.bounds] autorelease];
    [hostingView setHostedGraph:graph];
    [self.graphView addSubview:hostingView];
    
    // 设置绘图区边框及（坐标）边界
    //    graph.plotAreaFrame.borderLineStyle = nil;
    graph.plotAreaFrame.cornerRadius = 10.0f;
    graph.plotAreaFrame.paddingTop = 20.0f;
    graph.plotAreaFrame.paddingBottom = 90.0f;
    graph.plotAreaFrame.paddingRight = 10.0f;
    graph.plotAreaFrame.paddingLeft = 50.0f;
    
    // 设置graph距离hostingView的边界
    graph.paddingTop = 5.0f;
    graph.paddingBottom = 5.0f;
    graph.paddingLeft = 5.0f;
    graph.paddingRight = 5.0f;
    
    // 设置标题样式
    CPTMutableTextStyle * textStyle = [CPTMutableTextStyle textStyle];
    textStyle.fontSize = 16.0f;
    textStyle.color = [CPTColor cyanColor];
    graph.title = @"需求/订单量曲线";
    graph.titleTextStyle = textStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0f, -20.0f); // 以TOP而言, -20f
    
    // 设置绘图空间
    CPTXYPlotSpace * plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(xFloat)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(yFloat)];
    
    // 设置线样式
    CPTMutableLineStyle * requirePlotLineStyle = [CPTMutableLineStyle lineStyle];
    requirePlotLineStyle.lineWidth = 2.0f;
    requirePlotLineStyle.lineColor = [[CPTColor blueColor] colorWithAlphaComponent:1];
    CPTMutableLineStyle * orderPlotLineStyle = [CPTMutableLineStyle lineStyle];
    orderPlotLineStyle.lineWidth = 2.0f;
    orderPlotLineStyle.lineColor = [[CPTColor redColor] colorWithAlphaComponent:1];
    
    // 设置坐标轴
    CPTXYAxisSet * axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    CPTXYAxis * xAxis = axisSet.xAxis;
    // x轴标题显示位置，以PlotSpace的xRange值为基础
    xAxis.titleLocation = CPTDecimalFromFloat(xFloat / 2);
    xAxis.titleOffset = 20;
    // x轴大刻度的长度
    xAxis.majorTickLength = 5;
    // 大刻度的间隔
    xAxis.majorIntervalLength = CPTDecimalFromFloat(1);
    // 不显示小刻度
    xAxis.minorTickLineStyle = nil;
    // 设定x轴坐标显示的文字为空（为了自定义）
    xAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
    
    // 定义x轴文字样式
    CPTMutableTextStyle * axisTextStyle = [[[CPTMutableTextStyle alloc] init] autorelease];
    axisTextStyle.color = [CPTColor whiteColor];
    axisTextStyle.fontSize = 10.0;
    
    // 创建x轴每一个坐标文字
    NSMutableSet * newAxisSet = [[[NSMutableSet alloc] init] autorelease];
    for (int i = 0; i < [dateAxis count]; i++) {
        CPTAxisLabel * axisLabel = [[CPTAxisLabel alloc] initWithText:[NSString stringWithFormat:@"%@", [dateAxis objectAtIndex:i]] textStyle:axisTextStyle];
        axisLabel.tickLocation = CPTDecimalFromInt(i);
        axisLabel.offset = 5;
        axisLabel.rotation = M_PI / 4;
        [newAxisSet addObject:axisLabel];
        [axisLabel release];
    }
    // 设定新x轴坐标
    xAxis.axisLabels = newAxisSet;
    
    // 设置y轴样式
    CPTXYAxis * yAxis = axisSet.yAxis;
    yAxis.titleLocation = CPTDecimalFromFloat(yFloat / 2);
    yAxis.majorIntervalLength = CPTDecimalFromFloat(yFloat / 5);
    yAxis.majorTickLength = 5;
    yAxis.minorTickLineStyle = nil;
    
    //  定义需求量曲线
    CPTScatterPlot * requireScatterPlot = [[[CPTScatterPlot alloc] init] autorelease];
    requireScatterPlot.identifier = @"需求量";
    requireScatterPlot.dataLineStyle = requirePlotLineStyle;
    requireScatterPlot.dataSource = self;
    
    [graph addPlot:requireScatterPlot];
    
    //  定义订单量曲线
    CPTScatterPlot * orderScatterPlot = [[[CPTScatterPlot alloc] init] autorelease];
    orderScatterPlot.identifier = @"订单量";
    orderScatterPlot.dataLineStyle = orderPlotLineStyle;
    orderScatterPlot.dataSource = self;
    
    [graph addPlot:orderScatterPlot];
    
    //  定义曲线上的折点边线样式
    CPTMutableLineStyle * symbolLineStyle = [CPTMutableLineStyle lineStyle];
    symbolLineStyle.lineColor = [CPTColor greenColor];
    symbolLineStyle.lineWidth = 1.0f;
    //  定义曲线上的折点样式
    CPTPlotSymbol * plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    plotSymbol.fill = [CPTFill fillWithColor:[CPTColor greenColor]];
    plotSymbol.lineStyle = symbolLineStyle;
    plotSymbol.size = CGSizeMake(5.0f, 5.0f);
    //  需求量曲线添加折点
    requireScatterPlot.plotSymbol = plotSymbol;
    requireScatterPlot.plotSymbolMarginForHitDetection = 5.0f;
    requireScatterPlot.delegate = self;
    //  订单量曲线添加折点
    orderScatterPlot.plotSymbol = plotSymbol;
    orderScatterPlot.plotSymbolMarginForHitDetection = 5.0f;
    orderScatterPlot.delegate = self;
    
    //  定义曲线说明标签边框样式
    CPTMutableLineStyle * legendLineStyle = [[[CPTMutableLineStyle alloc] init] autorelease];
    legendLineStyle.lineWidth = 2.0;
    legendLineStyle.lineColor = [CPTColor blueColor];
    
    //  定义曲线说明标签文字样式
    CPTMutableTextStyle * legendTextStyle = [[[CPTMutableTextStyle alloc] init] autorelease];
    legendTextStyle.color = [CPTColor whiteColor];
    legendTextStyle.fontSize = 14.0;
    
    //  定义曲线说明标签
    graph.legend = [CPTLegend legendWithGraph:graph];
	graph.legend.textStyle = legendTextStyle;
	graph.legend.fill = [CPTFill fillWithColor:[CPTColor darkGrayColor]];
	graph.legend.borderLineStyle = legendLineStyle;
	graph.legend.cornerRadius = 5.0;
	graph.legend.swatchSize = CGSizeMake(25.0, 25.0);
	graph.legendAnchor = CPTRectAnchorBottom;
	graph.legendDisplacement = CGPointMake(0.0, 12.0);
    
    [spinner stopAnimating];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [spinner startAnimating];
    if (![GlobalUtil isIphone]) {
        // iPad
        bottomView.backgroundColor = [UIColor clearColor];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Plot Data Source Methods
//  x轴数据量
- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot{
    return [dateAxis count];
}

//  绘制曲线各点
- (NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index{
    NSDecimalNumber * num = nil;
    if(fieldEnum == CPTScatterPlotFieldX){
        //  x轴
        num = [[requireAxis objectAtIndex:index] objectForKey:@"x"];
    }else if([plot.identifier isEqual:@"需求量"]){
        //  y轴 需求量
        num = [[requireAxis objectAtIndex:index] objectForKey:@"y"];
    }else{
        //  y轴 订单量
        num = [[orderAxis objectAtIndex:index] objectForKey:@"y"];
    }
    return num;
}


#pragma mark - 
#pragma mark CPTScatterPlot Delegate Methods
//  曲线折点点击事件
- (void)scatterPlot:(CPTScatterPlot *)plot plotSymbolWasSelectedAtRecordIndex:(NSUInteger)index{
    
    
    //  需求量 移除上一个被点击的折点的点标签
    if(requireAnnotation){
        [graph.plotAreaFrame.plotArea removeAnnotation:requireAnnotation];
        [requireAnnotation release];
        requireAnnotation = nil;
    }
    //  订单量 移除上一个被点击的折点的点标签
    if(orderAnnotation){
        [graph.plotAreaFrame.plotArea removeAnnotation:orderAnnotation];
        [orderAnnotation release];
        orderAnnotation = nil;
    }
    //  定义文字标签样式
    CPTMutableTextStyle * annotationTextStyle = [CPTMutableTextStyle textStyle];
    annotationTextStyle.color = [CPTColor yellowColor];
    annotationTextStyle.fontSize = 14.0f;
    
    //  需求量 获取点击的折点的坐标值
    NSNumber * x1 = [[requireAxis objectAtIndex:index] valueForKey:@"x"];
    NSNumber * y1 = [[requireAxis objectAtIndex:index] valueForKey:@"y"];
    //  订单量 获取点击的折点的坐标值
    NSNumber * x2 = [[orderAxis objectAtIndex:index] valueForKey:@"x"];
    NSNumber * y2 = [[orderAxis objectAtIndex:index] valueForKey:@"y"];
    //  将坐标值封装成NSArray格式
    NSArray  * array1 = [NSArray arrayWithObjects:x1, y1, nil];
    NSArray  * array2 = [NSArray arrayWithObjects:x2, y2, nil];
    
    //  将y值转为字符串
    NSNumberFormatter * formater = [[[NSNumberFormatter alloc] init] autorelease];
    [formater setMaximumFractionDigits:2];
    NSString * y1String  = [formater stringFromNumber:y1];
    NSString * y2String  = [formater stringFromNumber:y2];
    
    //  需求量 定义文字标签
    CPTTextLayer * textLayer1 = [[[CPTTextLayer alloc] initWithText:y1String style:annotationTextStyle] autorelease];
    //  订单量 定义文字标签
    CPTTextLayer * textLayer2 = [[[CPTTextLayer alloc] initWithText:y2String style:annotationTextStyle] autorelease];
    //  需求量 定义点标签，将文字标签加入点标签
    requireAnnotation = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:graph.defaultPlotSpace anchorPlotPoint:array1];
    requireAnnotation.contentLayer = textLayer1;
    //  设置点标签相对于改坐标点的偏移量
    requireAnnotation.displacement = CGPointMake(0.0f, 15.0f);
    [graph.plotAreaFrame.plotArea addAnnotation:requireAnnotation];
    //  订单量 定义点标签，将文字标签加入点标签 
    orderAnnotation = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:graph.defaultPlotSpace anchorPlotPoint:array2];
    orderAnnotation.contentLayer = textLayer2;
    //  设置点标签相对于改坐标点的偏移量
    orderAnnotation.displacement = CGPointMake(0.0f, -15.0f);
    [graph.plotAreaFrame.plotArea addAnnotation:orderAnnotation];
    
    //  设置底部的文字
    SaleAnalyse * item = [SAArray objectAtIndex:index];
    self.dateLabel.text = item.date;
    self.requireLabel.text = item.require;
    self.orderLabel.text = item.order;
    self.cashLabel.text = item.cash;
    self.rateLabel.text = [NSString stringWithFormat:@"%@%%", item.rate];
}

@end
