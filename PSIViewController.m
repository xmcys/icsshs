//
//  进销存分析视图控制器 头文件
//  PSIViewController.m
//  icss-hs
//
//  Created by hsit on 11-9-columnHeight.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "PSIViewController.h"
#import "PSIVisibilityViewController.h"
#import "PSISearchViewController.h"
#import "GlobalUtil.h"
#import "BrandStockSaleDatabase.h"
#import "icss_hsAppDelegate.h"

@implementation PSIViewController

@synthesize titleView;         //  可滚动列视图
@synthesize bottomView;        //  底部周期视图
@synthesize firstScrollView;   //  固定列滚动视图
@synthesize secondScrollView;  //  可滚动列滚动视图
@synthesize firstTableView;    //  固定列 表视图
@synthesize secondTableView;   //  可滚动列 表视图
@synthesize mainSpinner;       //  主加载指示器
@synthesize settingsButton;    //  可视列视图按钮
@synthesize unitView;          //  单位选择视图
@synthesize unitButton;        //  单位选择按钮

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [mainSpinner release];
    [titleView release];
    [firstScrollView release];
    [secondScrollView release];
    [firstTableView release];
    [secondTableView release];
    [bottomView release];
    [psiArray release];
    [footerFirstView release];
    [headerSecondView release];
    [headerImageView release];
    [headerSpinner release];
    [headerLabel release];
    [footerFirstView release];
    [footerSecondView release];
    [footerImageView release];
    [footerSpinner release];
    [footerLabel release];
    [settingsButton release];
    [visibilityViewController release];
    [searchViewController release];
    [psiData release];
    [aBrand release];
    [aValue release];
    [connection release];
    [db release];
    [popoverController release];
    [unitView release];
    [unitButton release];
    [super dealloc];
}

- (void)viewDidUnload
{
    mainSpinner = nil;
    titleView = nil;
    firstScrollView = nil;
    secondScrollView = nil;
    firstTableView = nil;
    secondTableView = nil;
    bottomView = nil;
    psiArray = nil;
    headerFirstView = nil;
    headerSecondView = nil;
    headerImageView = nil;
    headerSpinner = nil;
    headerLabel = nil;
    footerFirstView = nil;
    footerSecondView = nil;
    footerImageView = nil;
    footerSpinner = nil;
    footerLabel = nil;
    settingsButton = nil;
    visibilityViewController = nil;
    searchViewController = nil;
    psiData = nil;
    aBrand = nil;
    aValue = nil;
    connection = nil;
    db = nil;
    popoverController= nil;
    unitButton = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  视图即将显示时的操作
- (void)viewWillAppear:(BOOL)animated{
    //  判断可视列控制器是否为空
    if (visibilityViewController != nil) {
        //  可视列标识数组
        NSMutableArray * array = visibilityViewController.visibilityArray;
        //  用于记录有多少列是可视的，控制每个标题的显示位置
        int count = -1;
        //  重新定义标题栏每个标题的大小和显示位置
        for (int i = 0; i < [array count]; i++) {
            UILabel * label = (UILabel *)[titleView viewWithTag:(i + 1)];
            int value = [[[array objectAtIndex:i] objectAtIndex:1] intValue];
            if (value == 1) {
                count++;
            }
            label.frame = CGRectMake(columnWidth * count, 0, columnWidth * value, columnHeight);
            //  重定义可滚动视图的内容大小
            secondScrollView.contentSize = CGSizeMake(columnWidth * count + columnWidth, columnHeight);
        }
        //  重定义表视图的可视列的大小和显示位置
        [secondTableView reloadData];
        [firstTableView reloadData];
    }
    //  判断搜索视图控制器是否为空， 搜索标识是否为1，为1则搜索，为0则不搜索
    if (searchViewController != nil && searchViewController.btnSearch.tag == 1) {
        //  设置表视图的内部偏移量，显示下拉刷新视图
        firstTableView.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
        secondTableView.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
        //  隐藏下拉刷新的图标
        headerImageView.hidden = YES;
        //  显示下拉刷新的加载指示器
        [headerSpinner startAnimating];
        headerLabel.text = @"正在加载数据，请稍后...";
        isLoadMore = NO;
        startPage = 1;
        searchViewController.btnSearch.tag = 0;
        //  加载数据
        [self loadData];
    }
    if (unitView.frame.size.height != 0) {
        unitView.frame = unitViewRectHide;
    }
}

//  初始化表视图顶部的下拉刷新视图 与 底部的上拉加载视图
- (void)initHeaderAndFooterView{
    //  初始化左侧图标视图
    headerFirstView = [[UIView alloc] initWithFrame:CGRectMake(0, -35, firstScrollView.frame.size.width, 35)];
    //  clearColor 清除背景色（透明）
    headerFirstView.backgroundColor = [UIColor clearColor];
    
    //  初始化右侧文字视图
    headerSecondView = [[UIView alloc] initWithFrame:CGRectMake(0, -35, titleView.frame.size.width, 35)];
    headerSecondView.backgroundColor = [UIColor clearColor];
    
    //  初始化图标，并添加进headerFirstView中
    headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_down.png"]];
    headerImageView.frame = CGRectMake((firstScrollView.frame.size.width - 32) / 2, 0, 32, 32);
    //  设置图像为长宽缩放
    [headerImageView setContentMode:UIViewContentModeScaleToFill];
    [headerFirstView addSubview:headerImageView];
    
    //  初始化左侧加载指示器，并添加进headerFirstView中
    headerSpinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((firstScrollView.frame.size.width - 20) / 2, 8, 20, 20)];
    [headerSpinner setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    headerSpinner.hidesWhenStopped = YES;
    [headerFirstView addSubview:headerSpinner];
    
    //  初始化右侧文字，并添加进headerFirstView中
    headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, secondScrollView.frame.size.width, 35)];
    headerLabel.textColor = [UIColor orangeColor];
    headerLabel.font = [UIFont systemFontOfSize:14.0];
    headerLabel.text = @"向下拖动刷新数据...";
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerSecondView addSubview:headerLabel];
    
    //  将下拉刷新视图添加进表视图中，作为表视图的headerView
    [firstTableView addSubview:headerFirstView];
    [secondTableView addSubview:headerSecondView];
    
    //  初始化上拉加载左侧视图
    footerFirstView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, firstScrollView.frame.size.width, 35)];
    footerFirstView.backgroundColor = [UIColor clearColor];
    
    //  初始化上拉加载右侧视图
    footerSecondView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, titleView.frame.size.width, 35)];
    footerSecondView.backgroundColor = [UIColor clearColor];
    
    //  初始化上拉加载左侧图标视图，并添加进footerSecondView中
    footerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_down.png"]];
    footerImageView.frame = CGRectMake((firstScrollView.frame.size.width - 32) / 2, 0, 32, 32);
    footerImageView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    [footerImageView setContentMode:UIViewContentModeScaleToFill];
    [footerFirstView addSubview:footerImageView];
    
    //  初始化上拉加载左侧加载指示器视图，并添加进footerSecondView中
    footerSpinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((firstScrollView.frame.size.width - 20) / 2, 8, 20, 20)];
    [footerSpinner setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    footerSpinner.hidesWhenStopped = YES;
    [footerFirstView addSubview:footerSpinner];
    
    //  初始化上拉加载右侧文字视图，并添加进footerSecondView中
    footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, secondScrollView.frame.size.width, 35)];
    footerLabel.textColor = [UIColor orangeColor];
    footerLabel.font = [UIFont systemFontOfSize:14.0];
    footerLabel.text = @"向上拖动加载更多...";
    footerLabel.backgroundColor = [UIColor clearColor];
    [footerSecondView addSubview:footerLabel];
    
    //  将上拉加载视图添加进表视图中，作为表视图的footerView
    [firstTableView setTableFooterView:footerFirstView];
    [secondTableView setTableFooterView:footerSecondView];
    
}

//  可视列视图按钮点击事件
- (IBAction)clickSettingsButton:(id)sender{
    if (visibilityViewController == nil) {
        //  初始化可视列视图控制器
        visibilityViewController = [[PSIVisibilityViewController alloc] init];
    }
    //  显示可视列视图控制器
    [self.navigationController pushViewController:visibilityViewController animated:YES];
}

//  导航栏右侧按钮点击事件：搜索
- (IBAction)clickSearchButton:(id)sender{
    if (searchViewController == nil) {
        //  初始化搜索视图控制器
        searchViewController = [[PSISearchViewController alloc] init];
    } 
    //  判断设备是否iPhone
    if ([GlobalUtil isIphone]) {
        [self.navigationController pushViewController:searchViewController animated:YES];
    } else {
        //  iPad
        if (popoverController ==  nil) {
            searchViewController.contentSizeForViewInPopover = CGSizeMake(320, 416);
            popoverController = [[UIPopoverController alloc] initWithContentViewController:searchViewController];
            popoverController.delegate = self;
        }
        //  如果搜索视图为可视状态，则隐藏，并更新按钮状态
        if ([popoverController isPopoverVisible]) {
            [popoverController dismissPopoverAnimated:YES];
            [self viewWillAppear:YES];
            self.navigationItem.rightBarButtonItem.title = @"查询";
        }else{
            [popoverController presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            self.navigationItem.rightBarButtonItem.title = @"关闭";
        }
    }
}

//  加载数据
- (void)loadData{
    if (isLoading) {
        return;
    }
    //  判断网络是否可用
    if (![GlobalUtil isReachable]) {
        [GlobalUtil showAlertWithTitle:@"网络故障" andMessage:@"当前网络不可用"];
        return;
    }
    isLoading = YES;
    loadRecords = 0;
    //  判断是加载更多还是刷新
    if (!isLoadMore) {
        firstTableView.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
        secondTableView.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
        headerImageView.hidden = YES;
        [headerSpinner startAnimating];
        headerLabel.text = @"正在加载数据，请稍后...";
    } else {
        footerLabel.text = @"正在加载数据，请稍后...";
        footerImageView.hidden = YES;
        [footerSpinner startAnimating];
    }
    [mainSpinner startAnimating];
    //  获取搜索开始日期
    NSString * startDate = searchViewController != nil ? searchViewController.startDateField.text : [GlobalUtil initSearchDate:0];
    //  获取搜索结束日期
    NSString * endDate = searchViewController != nil ? searchViewController.endDateField.text : [GlobalUtil initSearchDate:1];
    //  获取搜索卷烟名称
    NSString * brandName = searchViewController != nil ? searchViewController.nameField.text : nil;
    //  设置搜索记录结束位置
    int endPage = startPage + pageCount - 1;
    //  如果卷烟名称为空，调用接口1 
    if (brandName == nil || [brandName length] == 0) {
        NSString * url = nil;
        if ([icss_hsAppDelegate appDelegate].useInsideNet) {
            url = [[NSString alloc] initWithFormat:@"http://10.188.50.238:9088/tmi/ws/rs/mobile/queryStockSale/startDay/%@/endDay/%@/startPage/%d/endPage/%d?licence=123456789&key=25f9e794323b453885f5181f1b624d0b", startDate, endDate, startPage, endPage];
        } else {
            url = [[NSString alloc] initWithFormat:@"http://xm.fj-tobacco.com:9088/tmi/ws/rs/mobile/queryStockSale/startDay/%@/endDay/%@/startPage/%d/endPage/%d?licence=123456789&key=25f9e794323b453885f5181f1b624d0b", startDate, endDate, startPage, endPage];
        }
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
        connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [request release];
        [url release];
    } else {
        isLoadMore = NO;
        // 卷烟名称不为空，调用搜索接口2
        NSString * url = nil;
        if ([icss_hsAppDelegate appDelegate].useInsideNet) {
            url = [[NSString alloc]initWithFormat:@"http://10.188.50.238:9088/tmi/ws/rs/mobile/queryStockSaleName/startDay/%@/endDay/%@", startDate, endDate];
        } else {
            url = [[NSString alloc]initWithFormat:@"http://xm.fj-tobacco.com:9088/tmi/ws/rs/mobile/queryStockSaleName/startDay/%@/endDay/%@", startDate, endDate];
        }
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:url]];
        //  根据接口2要求，设置request为PUT方法
        [request setHTTPMethod:@"PUT"];
        [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
        NSString * name = [[NSString alloc] initWithFormat:@"<SqlStr><sqlStr>%@</sqlStr></SqlStr>", brandName];
        [request setHTTPBody:[name dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]];
        connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        [request release];
        [url release];
    }
    //  设置导航栏右侧搜索按钮状态
    [self changeRightButtonState:1];
    //  显示底部搜索周期
    bottomView.text = [NSString stringWithFormat:@"查询周期：%@ - %@", startDate, endDate];
}

// 解析XML网络数据 
- (void)parseXMLData{
    //  释放连接器
    [connection release];
    connection = nil;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:psiData];
    [parser setDelegate:self];
    //  解析数据
    [parser parse];
    //  删除数据库中所有数据
    [db deleteAllData];
    //  保存所有数据
    [db saveData:psiArray searchDate:bottomView.text];
    //  刷新表单
    [self reloadTable];
    NSLog(@"%d, %d, %d", startPage, loadRecords, startPage + pageCount - 1);
    [parser release];
}

// 刷新表格
- (void)reloadTable{
    isLoading = NO;
    [mainSpinner stopAnimating];
    [firstTableView reloadData];
    [secondTableView reloadData];
    [self changeRightButtonState:0];
    unitButton.titleLabel.text = @"单位: 条[更改]";
    if (isLoadMore) {
        footerImageView.hidden = NO;
        footerLabel.text = @"向上拖动加载更多...";
        //  改变箭头方向
        footerImageView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
        [footerSpinner stopAnimating];
        if (loadRecords != -1 && loadRecords < pageCount) {
            footerLabel.text = @"数据已全部加载";
        }
    } else {
        headerImageView.hidden = NO;
        headerLabel.text = @"向下拖动刷新数据...";
        headerImageView.layer.transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
        [headerSpinner stopAnimating];
        if ([psiArray count] != 0) {
            //  设置表视图内部偏移量为0
            firstTableView.contentInset = UIEdgeInsetsZero;
            secondTableView.contentInset = UIEdgeInsetsZero;
            [headerSpinner stopAnimating];
            //  将表视图滚动到顶部
            [firstTableView setContentOffset:CGPointMake(0.0, 0.0)];
            [secondTableView setContentOffset:CGPointMake(0.0, 0.0)];
        }
    }
}

//  设置导航栏右侧按钮状态 ：1 为取消加载  0 为查询
- (void)changeRightButtonState:(int)tag{
    if (tag == 0) {
        UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] 
                                         initWithTitle:@"查询"
                                         style:UIBarButtonItemStylePlain
                                         target:self action:@selector(clickSearchButton:)];
        self.navigationItem.rightBarButtonItem = rightButton;
        [rightButton release];
    } else {
        UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] 
                                         initWithTitle:@"取消加载"
                                         style:UIBarButtonItemStylePlain
                                         target:self action:@selector(cancelLoading)];
        self.navigationItem.rightBarButtonItem = rightButton;
        [rightButton release];
    }
}

//  取消加载
- (void)cancelLoading{
    if (connection != nil) {
        [connection cancel];
    }
    [connection release];
    loadRecords = -1;
    connection = nil;
    [self reloadTable];
    [self changeRightButtonState:0];
}

- (IBAction)showOrHideUnitView:(id)sender{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    if (unitView.frame.size.height == 0) {
        unitView.frame = unitViewRectShow;
    } else {
        unitView.frame = unitViewRectHide;
    }
    [UIView commitAnimations];
}

- (IBAction)changeUnit:(id)sender{
    UIButton * aButton = (UIButton *)sender;
    unit = aButton.tag;
    int diviend = 0;
    NSString * title = nil;
    if (unit == 2) {
        diviend = 50;
        title = @"单位: 件[更改]";
    } else if (unit == 3){
        diviend = 5 * 50;
        title = @"单位: 箱[更改]";
    } else {
        diviend = 1;
        title = @"单位: 条[更改]";
    }
    unitButton.titleLabel.text = title;
    for (int i = 0; i < [psiArray count]; i++) {
        aBrand = [psiArray objectAtIndex:i];
        aBrand.qtyStockBegin = [NSString stringWithFormat:@"%.1f", [aBrand.qtyStockBeginOriginal floatValue] / diviend];
        aBrand.qtyStockBuy = [NSString stringWithFormat:@"%.1f", [aBrand.qtyStockBuyOriginal floatValue] / diviend];
        aBrand.qtyStockDraw = [NSString stringWithFormat:@"%.1f", [aBrand.qtyStockDrawOriginal floatValue] / diviend];
        aBrand.qtyStockEnd = [NSString stringWithFormat:@"%.1f", [aBrand.qtyStockEndOriginal floatValue] / diviend];
        aBrand.qtyStockSale = [NSString stringWithFormat:@"%.1f", [aBrand.qtyStockSaleOriginal floatValue] / diviend];
        aBrand.qtyStockTranOut = [NSString stringWithFormat:@"%.1f", [aBrand.qtyStockTranOutOriginal floatValue] / diviend];
        aBrand.qtyStockWaste = [NSString stringWithFormat:@"%.1f", [aBrand.qtyStockWasteOriginal floatValue] / diviend];
        aBrand.qtyStockWithdraw = [NSString stringWithFormat:@"%.1f", [aBrand.qtyStockWithdrawOriginal floatValue] / diviend];
        aBrand = nil;
    }
    [firstTableView reloadData];
    [secondTableView reloadData];
    [self showOrHideUnitView:nil];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    //  根据设备设置字体大小和列宽高
    if ([GlobalUtil isIphone]) {
        fontSize = 14.0f;
        columnWidth = 70;
        columnHeight = 30;
        unitViewRectShow = CGRectMake(16, 242, 60, 150);
        unitViewRectHide = CGRectMake(16, 392, 60, 0);
    } else {
        fontSize = 16.0f;
        columnWidth = 80;
        columnHeight = 40;
        unitViewRectShow = CGRectMake(16, 780, 60, 150);
        unitViewRectHide = CGRectMake(16, 930, 60, 0);
    }
    self.unitView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"unit_bg.jpg"]];
    self.navigationItem.title = @"进销存分析";
    //  重设滚动视图的内容窗体大小
    secondScrollView.contentSize = titleView.frame.size;
    //  将标题视图添加进滚动视图中
    [secondScrollView addSubview:titleView];
    secondTableView.frame = CGRectMake(0, columnHeight, titleView.frame.size.width, secondTableView.frame.size.height);
    //  初始化下拉刷新和上拉加载视图
    [self initHeaderAndFooterView];
    //  添加导航栏右侧按钮
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] 
                                     initWithTitle:@"查询"
                                     style:UIBarButtonItemStylePlain
                                     target:self action:@selector(clickSearchButton:)];
    self.navigationItem.rightBarButtonItem = rightButton;
    [rightButton release];
    isLoadMore = NO;
    isLoading = NO;
    startPage = 1;
    pageCount = 50;
    //  初始化数据库操作对象
    db = [[BrandStockSaleDatabase alloc] init];
    //  创建数据库表
    [db createTable];
    //  查询数据库数据
    NSMutableArray * array = [db queryAllData];
    connectionTimes = 0;
    if ([array count] != 0) {
        psiArray = [[NSMutableArray alloc] initWithArray:array];
        //  获取数据最后一条数据，最后一条数据不是进销存分析对象，而是一个被封装成BrandStockSale的查询周期
        BrandStockSale * item = [psiArray objectAtIndex:([psiArray count] - 1)];
        //  显示查询周期
        bottomView.text = item.brandId;
        //  移除最后一条数据
        [psiArray removeObject:item];
        //  将加载记录数设置为数组大小，用于判断能否【加载更多】
        loadRecords = [psiArray count];
    }else{
        //  如果数据库中无数据，则刷新
        [self loadData];
    }
    unitView.frame = unitViewRectHide;
    [unitView.layer setCornerRadius:10.0f];
    [unitView setClipsToBounds:YES];
    [self.view addSubview:unitView];
    unitButton.titleLabel.text = @"单位: 条[更改]";
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    self.firstTableView.separatorColor = [UIColor darkGrayColor];
    self.secondTableView.separatorColor = [UIColor darkGrayColor];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //  如果是iPhone，则支持横竖屏模式，如果是iPad，则不支持
    if ([GlobalUtil isIphone]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    //  调整横竖屏UI控件的显示位置
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait
        || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        firstScrollView.frame = CGRectMake(0, 0, 110, 392);
        secondScrollView.frame = CGRectMake(110, 0, 210, 392);
        bottomView.frame = CGRectMake(92, 392, 205, 24);
        mainSpinner.frame = CGRectMake(142, 190, 37, 37);
        settingsButton.frame = CGRectMake(297, 392, 18, 19);
        unitButton.frame = CGRectMake(0, 392, 92, 24);
        unitViewRectShow = CGRectMake(16, 242, 60, 150);
        unitViewRectHide = CGRectMake(16, 392, 60, 0);
    } else {
        firstScrollView.frame = CGRectMake(0, 0, 130, 243);
        secondScrollView.frame = CGRectMake(130, 0, 350, 243);
        bottomView.frame = CGRectMake(100, 243, 380, 24);
        mainSpinner.frame = CGRectMake(222, 115, 37, 37);
        settingsButton.frame = CGRectMake(445, 243, 18, 19);
        unitButton.frame = CGRectMake(0, 243, 100, 24);
        unitViewRectShow = CGRectMake(16, 93, 60, 150);
        unitViewRectHide = CGRectMake(16, 243, 60, 0);
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [popoverController dismissPopoverAnimated:NO];
}

//  重写 TableView 数据源
#pragma mark -
#pragma mark Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [psiArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSUInteger row = [indexPath row];
    BrandStockSale * item = [psiArray objectAtIndex:row];
    //  firstTableView为显示卷烟名称的固定列
    if (tableView == firstTableView) {
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
            
            //  将单元格的选中样式设为无
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //  定义 卷烟名称 文字样式
            UILabel * brandName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, firstScrollView.frame.size.width, columnHeight)];
            brandName.font = [UIFont systemFontOfSize:fontSize];
            brandName.minimumFontSize = fontSize - 3;
            brandName.textAlignment = UITextAlignmentLeft;
            brandName.adjustsFontSizeToFitWidth = YES;
            brandName.textColor = [UIColor lightTextColor];
            brandName.backgroundColor = [UIColor clearColor];
            brandName.tag = 1;
            [cell.contentView addSubview:brandName];
            [brandName release];
        }
        //  显示卷烟名称
        UILabel * brandName = (UILabel *)[cell viewWithTag:1];
        brandName.text = item.brandName;
    } else {
        //  secondTableView为可滚动的其余列 表视图
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //  定义 期初库存 文字样式
            UILabel * qtyStockBegin = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, columnWidth, columnHeight)];
            qtyStockBegin.font = [UIFont systemFontOfSize:fontSize];
            qtyStockBegin.minimumFontSize = fontSize - 3;
            qtyStockBegin.textAlignment = UITextAlignmentRight;
            qtyStockBegin.adjustsFontSizeToFitWidth = YES;
            qtyStockBegin.textColor = [UIColor lightTextColor];
            qtyStockBegin.backgroundColor = [UIColor clearColor];
            qtyStockBegin.tag = 2;
            [cell.contentView addSubview:qtyStockBegin];
            [qtyStockBegin release];
            
            //  定义 购进入库 文字样式
            UILabel * qtyStockBuy = [[UILabel alloc] initWithFrame:CGRectMake(columnWidth, 0, columnWidth, columnHeight)];
            qtyStockBuy.font = [UIFont systemFontOfSize:fontSize];
            qtyStockBuy.minimumFontSize = fontSize - 3;
            qtyStockBuy.textAlignment = UITextAlignmentRight;
            qtyStockBuy.adjustsFontSizeToFitWidth = YES;
            qtyStockBuy.textColor = [UIColor lightTextColor];
            qtyStockBuy.backgroundColor = [UIColor clearColor];
            qtyStockBuy.tag = 3;
            [cell.contentView addSubview:qtyStockBuy];
            [qtyStockBuy release];
            
            //  定义 领货出库 文字样式
            UILabel * qtyStockDraw = [[UILabel alloc] initWithFrame:CGRectMake(columnWidth * 2, 0, columnWidth, columnHeight)];
            qtyStockDraw.font = [UIFont systemFontOfSize:fontSize];
            qtyStockDraw.minimumFontSize = fontSize - 3;
            qtyStockDraw.textAlignment = UITextAlignmentRight;
            qtyStockDraw.adjustsFontSizeToFitWidth = YES;
            qtyStockDraw.textColor = [UIColor lightTextColor];
            qtyStockDraw.backgroundColor = [UIColor clearColor];
            qtyStockDraw.tag = 4;
            [cell.contentView addSubview:qtyStockDraw];
            [qtyStockDraw release];
            
            //  定义 期末库存 文字样式
            UILabel * qtyStockEnd = [[UILabel alloc] initWithFrame:CGRectMake(columnWidth * 3, 0, columnWidth, columnHeight)];
            qtyStockEnd.font = [UIFont systemFontOfSize:fontSize];
            qtyStockEnd.minimumFontSize = fontSize - 3;
            qtyStockEnd.textAlignment = UITextAlignmentRight;
            qtyStockEnd.adjustsFontSizeToFitWidth = YES;
            qtyStockEnd.textColor = [UIColor lightTextColor];
            qtyStockEnd.backgroundColor = [UIColor clearColor];
            qtyStockEnd.tag = 5;
            [cell.contentView addSubview:qtyStockEnd];
            [qtyStockEnd release];
            
            //  定义 退货出库 文字样式
            UILabel * qtyStockWithDraw = [[UILabel alloc] initWithFrame:CGRectMake(columnWidth * 4, 0, columnWidth, columnHeight)];
            qtyStockWithDraw.font = [UIFont systemFontOfSize:fontSize];
            qtyStockWithDraw.minimumFontSize = fontSize - 3;
            qtyStockWithDraw.textAlignment = UITextAlignmentRight;
            qtyStockWithDraw.adjustsFontSizeToFitWidth = YES;
            qtyStockWithDraw.textColor = [UIColor lightTextColor];
            qtyStockWithDraw.backgroundColor = [UIColor clearColor];
            qtyStockWithDraw.tag = 6;
            [cell.contentView addSubview:qtyStockWithDraw];
            [qtyStockWithDraw release];
            
            //  定义 销售出库 文字样式
            UILabel * qtyStockSale = [[UILabel alloc] initWithFrame:CGRectMake(columnWidth * 5, 0, columnWidth, columnHeight)];
            qtyStockSale.font = [UIFont systemFontOfSize:fontSize];
            qtyStockSale.minimumFontSize = fontSize - 3;
            qtyStockSale.textAlignment = UITextAlignmentRight;
            qtyStockSale.adjustsFontSizeToFitWidth = YES;
            qtyStockSale.textColor = [UIColor lightTextColor];
            qtyStockSale.backgroundColor = [UIColor clearColor];
            qtyStockSale.tag = 7;
            [cell.contentView addSubview:qtyStockSale];
            [qtyStockSale release];
            
            //  定义 移库出库 文字样式
            UILabel * qtyStockTranOut = [[UILabel alloc] initWithFrame:CGRectMake(columnWidth * 6, 0, columnWidth, columnHeight)];
            qtyStockTranOut.font = [UIFont systemFontOfSize:fontSize];
            qtyStockTranOut.minimumFontSize = fontSize - 3;
            qtyStockTranOut.textAlignment = UITextAlignmentRight;
            qtyStockTranOut.adjustsFontSizeToFitWidth = YES;
            qtyStockTranOut.textColor = [UIColor lightTextColor];
            qtyStockTranOut.backgroundColor = [UIColor clearColor];
            qtyStockTranOut.tag = 8;
            [cell.contentView addSubview:qtyStockTranOut];
            [qtyStockTranOut release];
            
            //  定义 本期溢耗 文字样式
            UILabel * qtyStockWaste = [[UILabel alloc] initWithFrame:CGRectMake(columnWidth * 7, 0, columnWidth, columnHeight)];
            qtyStockWaste.font = [UIFont systemFontOfSize:fontSize];
            qtyStockWaste.minimumFontSize = fontSize - 3;
            qtyStockWaste.textAlignment = UITextAlignmentRight;
            qtyStockWaste.adjustsFontSizeToFitWidth = YES;
            qtyStockWaste.textColor = [UIColor lightTextColor];
            qtyStockWaste.backgroundColor = [UIColor clearColor];
            qtyStockWaste.tag = 9;
            [cell.contentView addSubview:qtyStockWaste];
            [qtyStockWaste release];
        }
        
        //  显示 期初库存 值
        UILabel * qtyStockBegin = (UILabel *)[cell viewWithTag:2];
        qtyStockBegin.text = [NSString stringWithFormat:@"%@  ", item.qtyStockBegin];
        
        //  显示 购进入库 值
        UILabel * qtyStockBuy = (UILabel *)[cell viewWithTag:3];
        qtyStockBuy.text = [NSString stringWithFormat:@"%@  ", item.qtyStockBuy];
        
        //  显示 领货出库 值
        UILabel * qtyStockDraw = (UILabel *)[cell viewWithTag:4];
        qtyStockDraw.text = [NSString stringWithFormat:@"%@  ", item.qtyStockDraw];
        
        //  显示 期末库存 值
        UILabel * qtyStockEnd = (UILabel *)[cell viewWithTag:5];
        qtyStockEnd.text = [NSString stringWithFormat:@"%@  ", item.qtyStockEnd];
        
        //  显示 退货入库 值
        UILabel * qtyStockWithDraw = (UILabel *)[cell viewWithTag:6];
        qtyStockWithDraw.text = [NSString stringWithFormat:@"%@  ", item.qtyStockWithdraw];
        
        //  显示 销售出库 值
        UILabel * qtyStockSale = (UILabel *)[cell viewWithTag:7];
        qtyStockSale.text = [NSString stringWithFormat:@"%@  ", item.qtyStockSale];;
        
        //  显示 移库出库 值
        UILabel * qtyStockTranOut = (UILabel *)[cell viewWithTag:8];
        qtyStockTranOut.text = [NSString stringWithFormat:@"%@  ", item.qtyStockTranOut];
        
        //  显示 本期溢耗 值
        UILabel * qtyStockWaste = (UILabel *)[cell viewWithTag:9];
        qtyStockWaste.text = [NSString stringWithFormat:@"%@  ", item.qtyStockWaste];
    }
    
    return cell;
}

//  重写 TableView 协议
#pragma mark - 
#pragma mark Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //  设置两个表格同步选中
    [firstTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    [secondTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    //  如果用户没有点击过可视列选择按钮，则跳过次步骤
    if (visibilityViewController == nil) {
        return;
    }
    //  判断用户选择可视列，重新调整单元格显示的列大小和显示位置
    NSMutableArray * array = visibilityViewController.visibilityArray;
    int count = -1;
    for (int i = 0; i < [array count]; i++) {
        UILabel * label = (UILabel *)[cell viewWithTag:(i + 2)];
        int value = [[[array objectAtIndex:i] objectAtIndex:1] intValue];
        if (value == 1) {
            count++;
        }
        label.frame = CGRectMake(columnWidth * count, 0, columnWidth * value, columnHeight);
    }
}

//  重写 UIScrollView 协议
#pragma mark -
#pragma mark Scroll View Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    //  记录开始拖动的位置，用于判断每次可拖动的最小距离
    scrollStart = scrollView.contentOffset.x;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //  同步两个表视图的上下拖动偏移量
    if (scrollView == firstTableView) {
        secondTableView.contentOffset = firstTableView.contentOffset;
    }else{
        firstTableView.contentOffset = secondTableView.contentOffset;
        if (scrollView.contentOffset.x > 0) {
            //  重新设定 footerLabel的显示位置
            footerLabel.frame = CGRectMake(scrollView.contentOffset.x, 0, secondScrollView.frame.size.width, 35);
        }
    }
    if (isLoading) {
        return;
    }
    //  动画开始
    [UIView beginAnimations:nil context:nil];
    
    //  下拉刷新的动画
    if (scrollView.contentOffset.y < -35) {
        headerLabel.text = @"释放刷新数据...";
        //  旋转箭头
        headerImageView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    } else if (scrollView.contentOffset.y < 0) {
        headerLabel.text = @"向下拖动刷新数据...";
        headerImageView.layer.transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
    
    //  上拉加载的动画
    //  计算上拉偏移量 ： 表内容窗体 - 表窗体  （内容窗体不一定等于表窗体）
    CGFloat offset = secondTableView.contentSize.height - secondTableView.frame.size.height;
    if (offset < 0) {
        offset = 0;
    }
    //  如果数据已全部加载，则显示已全部加载。  loadRecords = -1 代表用户点击了【取消加载】
    if (loadRecords != -1 && loadRecords < pageCount) {
        footerLabel.text = @"数据已全部加载";
        footerImageView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    } else if (scrollView.contentOffset.y > 35 + offset) {
        footerLabel.text = @"释放刷新数据...";
        footerImageView.layer.transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    } else {
        footerLabel.text = @"向上拖动加载更多...";
        footerImageView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    }
    // 提交动画
    [UIView commitAnimations];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //  设置headerLabel的显示位置
    if (scrollView != firstTableView && scrollView.contentOffset.x > 0) {
        headerLabel.frame = CGRectMake(scrollView.contentOffset.x, 0, secondScrollView.frame.size.width, 35);
    }
    //  控制每次可滚动的大小 ： 至少为 n * 列宽 ， n >= 0
    if (scrollView.contentOffset.x > 0) {
        //  计算滚动到哪一列
        int column = (int)scrollView.contentOffset.x / columnWidth;
        //  取模，计算当前位置位于该列的偏移量
        int distance = abs((int)(scrollView.contentOffset.x - scrollStart) % columnWidth);
        //  如果偏移量大于开始滚动的位置，则左移
        if (scrollView.contentOffset.x >= scrollStart) {
            // 偏移量大于20，则左移1列
            if (distance > 20) {
                column += 1;
            }
        } else { //  如果偏移量小于开始滚动的位置，则右移
            // 偏移量大于0 小于20，则左移1列
            if (distance > 0 && distance < 20) {
                column += 1;
            }
        }
        //  控制偏移量
        [secondScrollView scrollRectToVisible:CGRectMake(columnWidth * column, 0, secondScrollView.frame.size.width, secondScrollView.frame.size.height) animated:YES];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    //  设置headerLabel的显示位置
    if (scrollView != firstTableView && scrollView.contentOffset.x > 0) {
        headerLabel.frame = CGRectMake(scrollView.contentOffset.x, 0, secondScrollView.frame.size.width, 35);
    }
    //  如果视图尚停止滚动：手指放开了不代表视图滚动就停止了，其余注释看上面。
    if (!decelerate && scrollView.contentOffset.x > 0) {
        int column = (int)scrollView.contentOffset.x / columnWidth;
        int distance = abs((int)(scrollView.contentOffset.x - scrollStart) % columnWidth);
        if (scrollView.contentOffset.x >= scrollStart) {
            if (distance > 20) {
                column += 1;
            }
        } else {
            if (distance > 0 && distance < 20) {
                column += 1;
            }
        }
        [secondScrollView scrollRectToVisible:CGRectMake(columnWidth * column, 0, secondScrollView.frame.size.width, secondScrollView.frame.size.height) animated:YES];
    }
    if (isLoading) {
        return;
    }
    //  下拉刷新
    if (scrollView.contentOffset.y < -35) {
        startPage = 1;
        isLoadMore = NO;
        [self loadData];
    }
    //  上拉加载
    CGFloat offset = secondTableView.contentSize.height - secondTableView.frame.size.height;
    if (offset < 0) {
        offset = 0;
    }
    //  数据已全部加载
    if (loadRecords != -1 && loadRecords < pageCount) {
        return;
    }
    //  加载更多
    if (scrollView.contentOffset.y > 35 + offset) {
        startPage = [psiArray count] + 1;
        isLoadMore = YES;
        [self loadData];
    }
}

//  重写 NSURLConnection 协议
#pragma mark - 
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"连接异常：%@", error);
    [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:@"请求网络数据发生异常。"];
    [self reloadTable];
}

- (void)connection:(NSURLConnection *)aConnection didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *) response;
    if((httpURLResponse.statusCode / 100) != 2){
        if (httpURLResponse.statusCode == 500 && connectionTimes < 3) {
            [connection cancel];
            [connection release];
            connection = nil;
            connectionTimes++;
            NSLog(@"第 %d 次连接", connectionTimes);
            [self loadData];
        } else {
            connectionTimes = 0;
            [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:[NSString stringWithFormat:@"网络响应异常：%d", httpURLResponse.statusCode]];
            [self reloadTable];
        }
        NSLog(@"网络响应异常: %@", [[httpURLResponse allHeaderFields] description]);
    }else{
        //  如果psiData对象不为空，释放它。
        if (psiData != nil) {
            [psiData release];
            psiData = nil;
        }
        //  接收到服务器响应时，初始化网络数据psiData对象
        psiData = [[NSMutableData alloc] init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    //  保存网络数据，因为数据可能分段加载，所以使用appendData
    [psiData appendData:data];
//    NSLog(@"%@", [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    //  解析网络数据
    [self parseXMLData];
}

////  以下两个方法用于降低网络连接安全性，可使用https协议
//- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
//    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]; 
//}
//
//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge { 
//    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
//        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge]; 
//    }
//    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge]; 
//}

//  重写 NSXMLParser 协议
#pragma mark -
#pragma mark XMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    //  解析XML文件，遇到全局KEY，则初始化进销存分析对象数组
    if([elementName isEqualToString:@"BrandStockSales"]){
        if(psiArray == nil){
            psiArray = [[NSMutableArray alloc] init];
        }
        if(!isLoadMore){
            [psiArray removeAllObjects];
        }
    }
    //  遇到对象KEY，则初始化进销存分析对象
    if([elementName isEqualToString:@"BrandStockSale"]){
        aBrand = [[BrandStockSale alloc]init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    //  保存对象每个值KEY
    if (aValue == nil) {
        aValue = [[NSString alloc] initWithString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    //  根据对象值KEY，对对象赋值
    if ([elementName isEqualToString:@"brandId"]) {
        aBrand.brandId = aValue;
    }
    if ([elementName isEqualToString:@"brandName"]) {
        aBrand.brandName = aValue;
    }
    if ([elementName isEqualToString:@"qtyStockBegin"]) {
        aBrand.qtyStockBegin = aValue;
        aBrand.qtyStockBeginOriginal = aValue;
    }
    if ([elementName isEqualToString:@"qtyStockBuy"]) {
        aBrand.qtyStockBuy = aValue;
        aBrand.qtyStockBuyOriginal = aValue;
    }
    if ([elementName isEqualToString:@"qtyStockDraw"]) {
        aBrand.qtyStockDraw = aValue;
        aBrand.qtyStockDrawOriginal = aValue;
    }
    if ([elementName isEqualToString:@"qtyStockEnd"]) {
        aBrand.qtyStockEnd = aValue;
        aBrand.qtyStockEndOriginal = aValue;
    }
    if ([elementName isEqualToString:@"qtyStockSale"]) {
        aBrand.qtyStockSale = aValue;
        aBrand.qtyStockSaleOriginal = aValue;
    }
    if ([elementName isEqualToString:@"qtyStockTranOut"]) {
        aBrand.qtyStockTranOut = aValue;
        aBrand.qtyStockTranOutOriginal = aValue;
    }
    if ([elementName isEqualToString:@"qtyStockWaste"]) {
        aBrand.qtyStockWaste = aValue;
        aBrand.qtyStockWasteOriginal = aValue;
    }
    if ([elementName isEqualToString:@"qtyStockWithdraw"]) {
        aBrand.qtyStockWithdraw = aValue;
        aBrand.qtyStockWithdrawOriginal = aValue;
        //  将对象添加进数组中，因为XML格式中qtyStockWithdraw是最后一个对象值KEY
        [psiArray addObject:aBrand];
        loadRecords++;
        [aBrand release];
        aBrand = nil;
    }
    [aValue release];
    aValue = nil;
}

//  重写 UIPopoverController 协议
#pragma mark -
#pragma mark Popover delegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    //  执行 viewWillAppear事件
    [self viewWillAppear:YES];
    self.navigationItem.rightBarButtonItem.title = @"查询";
}

@end
