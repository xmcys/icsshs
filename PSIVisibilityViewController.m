//
//  进销存分析可视列视图控制器 类
//  PSIVisibilityViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-9.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "PSIVisibilityViewController.h"


@implementation PSIVisibilityViewController

@synthesize visibilityTable; //  可视列 表视图
@synthesize visibilityArray; //  可视列 标识数组

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [visibilityArray release];
    [visibilityTable release];
    [super dealloc];
}

- (void)viewDidUnload
{
    visibilityTable = nil;
    visibilityArray = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  初始化可视列 标识数组 1：可视 0 ：不可视
- (void)initVisibilityArray{
    visibilityArray = [[NSMutableArray alloc] init];
    NSMutableArray * item1 = [[NSMutableArray alloc] initWithObjects:@"期初库存", @"1", nil];
    [visibilityArray addObject:item1];
    [item1 release];
    NSMutableArray * item2 = [[NSMutableArray alloc] initWithObjects:@"购进入库", @"1", nil];
    [visibilityArray addObject:item2];
    [item2 release];
    NSMutableArray * item3 = [[NSMutableArray alloc] initWithObjects:@"领货出库", @"1", nil];
    [visibilityArray addObject:item3];
    [item3 release];
    NSMutableArray * item4 = [[NSMutableArray alloc] initWithObjects:@"期末库存", @"1", nil];
    [visibilityArray addObject:item4];
    [item4 release];
    NSMutableArray * item5 = [[NSMutableArray alloc] initWithObjects:@"退货入库", @"1", nil];
    [visibilityArray addObject:item5];
    [item5 release];
    NSMutableArray * item6 = [[NSMutableArray alloc] initWithObjects:@"销售出库", @"1", nil];
    [visibilityArray addObject:item6];
    [item6 release];
    NSMutableArray * item7 = [[NSMutableArray alloc] initWithObjects:@"移库出库", @"1", nil];
    [visibilityArray addObject:item7];
    [item7 release];
    NSMutableArray * item8 = [[NSMutableArray alloc] initWithObjects:@"本期溢耗", @"1", nil];
    [visibilityArray addObject:item8];
    [item8 release];
}

//  点击导航栏全选/全不选按钮
- (IBAction)clickRightButton:(id)sender{
    UIBarButtonItem * rightButton = (UIBarButtonItem *)sender;
    NSString * selection = nil;
    //  改变按钮文字
    if ([rightButton.title isEqualToString:@"全选"]) {
        rightButton.title = @"全不选";
        selection = @"1";
    } else {
        rightButton.title = @"全选";
        selection = @"0";
    }
    //  遍历可视列标识数组，并更改气质
    for (int i = 0; i < [visibilityArray count]; i++) {
        NSMutableArray * item = [visibilityArray objectAtIndex:i];
        [item replaceObjectAtIndex:1 withObject:selection];
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        //  获取每个单元格，并更改文字说明和开关状态
        UITableViewCell * cell = [visibilityTable cellForRowAtIndexPath:indexPath];
        UISwitch * visiSwitch = (UISwitch *)[cell viewWithTag:i + 1];
        if ([selection isEqualToString:@"1"]) {
            cell.detailTextLabel.text = @"可视";
            [visiSwitch setOn:YES animated:YES];
        }else{
            cell.detailTextLabel.text = @"隐藏";
            [visiSwitch setOn:NO animated:YES];
        }
    }
}

//  点击UISwitch开关
- (IBAction)clickSwitchButton:(id)sender{
    UISwitch * aSwitch = (UISwitch *)sender;
    //  计算点击的是哪一行的开关视图
    int row = aSwitch.tag - 1;
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    UITableViewCell * cell = [visibilityTable cellForRowAtIndexPath:indexPath];
    NSMutableArray * item = [visibilityArray objectAtIndex:row];
    //  改变单元格文字提示，改变相应标识数组值
    if (aSwitch.on) {
        cell.detailTextLabel.text = @"可视";
        [item replaceObjectAtIndex:1 withObject:@"1"];
    }else{
        cell.detailTextLabel.text = @"隐藏";
        [item replaceObjectAtIndex:1 withObject:@"0"];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"可视列";
    [self initVisibilityArray];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] 
                                     initWithTitle:@"全不选"
                                     style:UIBarButtonItemStylePlain
                                     target:self 
                                     action:@selector(clickRightButton:)];
    self.navigationItem.rightBarButtonItem = rightButton;
    [rightButton release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//  重写 UITableView 数据源
#pragma mark -
#pragma mark Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [visibilityArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"请选择可视列";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSUInteger row = [indexPath row];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier] autorelease];
        //  设置单元格样式
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:15.0];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:11.0];
        //  初始化开关控件
        UISwitch * visiSwitch = [[UISwitch alloc] init];
        [visiSwitch addTarget:self action:@selector(clickSwitchButton:) forControlEvents:UIControlEventValueChanged];
        visiSwitch.tag = row + 1;
        [visiSwitch setOn:YES animated:NO];

        //  将单元格的accessoryView改为开关控件
        cell.accessoryView = visiSwitch;
        [visiSwitch release];
    }
    
    //  根据标识数组中的值，改变单元格文字和开关状态
    NSMutableArray * item = [visibilityArray objectAtIndex:row];
    cell.textLabel.text = [item objectAtIndex:0];
    UISwitch * visiSwitch = (UISwitch *)[cell viewWithTag:row + 1];
    if ([[item objectAtIndex:1] intValue] == 1) {
        cell.detailTextLabel.text = @"可视";
        [visiSwitch setOn:YES animated:YES];
    }else{
        cell.detailTextLabel.text = @"隐藏";
        [visiSwitch setOn:NO animated:YES];
    }
    return cell;
}

//  重写 UITableView 协议
#pragma mark - 
#pragma mark Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //  改变选中单元的文字提示、开关状态和相应标识数组值
    NSUInteger row = [indexPath row];
    NSMutableArray * item = [visibilityArray objectAtIndex:row];
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    UISwitch * visiSwitch = (UISwitch *)[cell viewWithTag:row + 1];
    if ([[item objectAtIndex:1] intValue] == 1) {
        [item replaceObjectAtIndex:1 withObject:@"0"];
        [visiSwitch setOn:NO animated:YES];
        cell.detailTextLabel.text = @"隐藏";
    } else {
        [item replaceObjectAtIndex:1 withObject:@"1"];
        [visiSwitch setOn:YES animated:YES];
        cell.detailTextLabel.text = @"可视";
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
