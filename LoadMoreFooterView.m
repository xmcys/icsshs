//
//  上拉加载视图
//  LoadMoreFooterView.m
//  icss-hs
//
//  Created by hsit on 11-10-21.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "LoadMoreFooterView.h"
#import <QuartzCore/QuartzCore.h>

@implementation LoadMoreFooterView

@synthesize spinner;         //  加载指示器
@synthesize arrowImageView;  //  箭头
@synthesize displayLabel;    //  提示文本

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        //  初始化箭头视图
        self.arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_up.png"]];
        self.arrowImageView.frame = CGRectMake(30, 0, 32, 32);
        [self addSubview:self.arrowImageView];
        
        //  初始化加载指示器
        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        self.spinner.hidesWhenStopped = YES;
        self.spinner.frame = CGRectMake(36, 7, 20, 20);
        [self addSubview:self.spinner];
        
        //  初始化文本提示
        self.displayLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 0, self.frame.size.width - 70, 35)];
        self.displayLabel.backgroundColor = [UIColor clearColor];
        self.displayLabel.textAlignment = UITextAlignmentCenter;
        self.displayLabel.textColor = [UIColor orangeColor];
        self.displayLabel.font = [UIFont systemFontOfSize:14.0];
        self.displayLabel.text = @"向上拖动加载更多...";
        [self addSubview:self.displayLabel];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)dealloc
{
    [spinner release];
    [arrowImageView release];
    [displayLabel release];
    [super dealloc];
}

//  改变视图各控件状态。  0 正常 1 拖动 2 加载
- (void)changeArrowAndLabelState:(int)actionTag{
    [UIView beginAnimations:nil context:nil];
    self.arrowImageView.hidden = NO;
    [self.spinner stopAnimating];
    if (actionTag == 0) {
        self.arrowImageView.layer.transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
        self.displayLabel.text = @"向上拖动加载更多...";
    } else if (actionTag == 1){
        self.arrowImageView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
        self.displayLabel.text = @"释放加载数据...";
    } else if (actionTag == 2){
        self.arrowImageView.hidden = YES;
        self.displayLabel.text = @"正在刷新...";
        [self.spinner startAnimating];
    } else {
        self.arrowImageView.layer.transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
        self.displayLabel.text = @"数据已全部加载";

    }
    [UIView commitAnimations];
}

@end
