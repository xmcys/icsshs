//
//  应用程序委托类 
//  icss_hsAppDelegate.m
//  icss-hs
//
//  Created by hsit on 11-9-21.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "icss_hsAppDelegate.h"
#import "StartingViewController.h"
#import "MenuViewController.h"
#import "PSIViewController.h"
#import "Database.h"
#import "GlobalUtil.h"

@implementation icss_hsAppDelegate


@synthesize window=_window;           //  应用窗体
@synthesize startingViewController;   //  启动视图控制器
@synthesize menuViewController;       //  菜单视图控制器
@synthesize navController;            //  导航控制器
@synthesize database;                 //  数据库操作对象
@synthesize useInsideNet;             //  使用内网

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ([GlobalUtil isIphone]) {
        // iPhone
        // 初始化应用启动界面StartingViewController
        self.startingViewController = [[StartingViewController alloc] initWithNibName:@"StartingViewController" bundle:nil];
    } else {
        // iPad
        self.startingViewController = [[StartingViewController alloc] initWithNibName:@"StartingViewController-iPad" bundle:nil];
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:YES];
    
    // 并将应用根视图设置为StartingViewController
    self.window.rootViewController = self.startingViewController;
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    //  在应用程序即将进入后台时，关闭数据库。
    [database closeDatabase];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //  在应用程序进入后台时，关闭数据库。
    [database closeDatabase];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    //  在应用程序即将进入前台显示时，开启数据库。
    [database openDatabase];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //  在应用程序变成活动状态时，开启数据库。
    [database openDatabase];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    //  在应用程序即将中断时，关闭数据库。
    [database closeDatabase];
}

- (void)dealloc
{
    [_window release];
    [startingViewController release];
    [menuViewController release];
    [navController release];
    [database release];
    [super dealloc];
}

// 获取应用委托（静态方法）
+ (icss_hsAppDelegate *) appDelegate{
    return [[UIApplication sharedApplication] delegate];
}

// 移除启动画面，并进入菜单列表
- (void)removeStartViewAddMenuView{
    // 初始化导航控制器
    self.navController = [[UINavigationController alloc]init];
    self.navController.navigationBar.barStyle = UIBarStyleBlack;
    
    // 将应用程序的根视图控制器改为导航控制器
    self.window.rootViewController = self.navController;
    // 初始化菜单视图
    NSString * nibName = nil;
    if ([GlobalUtil isIphone]) {
        // iPhone
        nibName = @"MenuViewController";
    }else{
        // iPad
        nibName = @"MenuViewController-iPad";
    }
    MenuViewController * menuView = [[MenuViewController alloc] initWithNibName:nibName bundle:nil];
    self.menuViewController = menuView;
    [menuView release];
    // 将菜单视图压入导航控制器中
    [self.navController pushViewController:menuViewController animated:YES];
    // 释放启动画面
    [self.startingViewController release];
}

//  调用进销存分析视图控制器的数据查询方法（iPad）
- (void)rearchPurcheaseSalesInventoryData{
    [menuViewController.psiViewController clickSearchButton:nil];
}

@end
