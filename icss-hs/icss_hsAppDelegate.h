//  
//  应用程序委托类 头文件
//  icss_hsAppDelegate.h
//  icss-hs
//
//  Created by hsit on 11-9-21.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class StartingViewController; //  启动视图类
@class MenuViewController;     //  菜单视图类
@class Database;               //  基础数据库操作类

@interface icss_hsAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow               * window;             //  应用窗体
@property (nonatomic, retain) IBOutlet UINavigationController * navController;      //  导航控制器
// 启动视图控制器
@property (nonatomic, retain) IBOutlet StartingViewController * startingViewController; 
//  菜单视图控制器
@property (nonatomic, retain) IBOutlet MenuViewController     * menuViewController; 
@property (nonatomic, retain) Database                        * database;  //  数据库操作对象

@property (nonatomic) bool useInsideNet; //  使用内网

//  返回应用程序的委托（静态方法）
+ (icss_hsAppDelegate *) appDelegate;

//  永久性移除启动视图，导入菜单视图
- (void)removeStartViewAddMenuView;

//  调用进销存分析视图控制器的数据查询方法（iPad）
- (void)rearchPurcheaseSalesInventoryData;
@end
