//
//  周边搜索视图控制器
//  CircumSearchViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-18.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "CircumSearchViewController.h"
#import "GlobalUtil.h"
#import "Customer.h"
#import <QuartzCore/QuartzCore.h>
#import "RefreshHeaderView.h"
#import "icss_hsAppDelegate.h"

@implementation CircumSearchViewController

@synthesize spinner;        //  加载指示器
@synthesize contentView;    //  内容视图
@synthesize tabBar;         //  选项卡栏 
@synthesize searchTable;    //  搜索表视图
@synthesize locationMgr;    //  定位对象
@synthesize locatingView;   //  底部显示【正在定位】的视图
@synthesize aListView;      //  列表视图（iPad）
@synthesize aDetailView;    //  详情视图（iPad）
@synthesize aMapView;       //  地图视图（iPad）
@synthesize locSpinner;     //  定位指示器
@synthesize locationLabel;  //  当前位置

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [spinner release];
    [connection release];
    [customersData release];
    [mapViewController release];
    [listViewController release];
    [contentView release];
    [tabBar release];
    [searchTable release];
    [customersArray release];
    [locationMgr release];
    [customer release];
    [aValue release];
    [locatingView release];
    [reverseGeocode release];
    [lastIndexPath release];
    [detailViewController release];
    [aListView release];
    [aDetailView release];
    [aMapView release];
    [locationLabel release];
    [locSpinner release];
    [super dealloc];
}

- (void)viewDidUnload
{
    spinner = nil;
    connection = nil;
    customersData = nil;
    mapViewController = nil;
    listViewController = nil;
    contentView = nil;
    tabBar = nil;
    searchTable = nil;
    customersArray = nil;
    locationMgr = nil;
    customer = nil;
    aValue = nil;
    locatingView = nil;
    reverseGeocode = nil;
    lastIndexPath = nil;
    detailViewController = nil;
    aListView = nil;
    aMapView = nil;
    aDetailView = nil;
    locationLabel = nil;
    locSpinner = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  改变显示的视图 0 列表模式  1 地图模式
- (void)changeDisplayView:(int)tag{
    //  判断是否定位成功
    if (tag == 1 && coordinate.latitude == 0) {
        [GlobalUtil showAlertWithTitle:@"定位提示" andMessage:@"正在进行定位，请稍后..."];
        currentViewTag = 0;
        [tabBar setSelectedItem:[tabBar.items objectAtIndex:0]];
        return;
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:contentView cache:YES];
    [UIView setAnimationDuration:0.5];
    if (tag == 0) {
        //  列表模式
        [mapViewController.view removeFromSuperview];
        [contentView addSubview:listViewController.view];
    } else {
        //  地图模式
        [listViewController.view removeFromSuperview];
        [contentView addSubview:mapViewController.view];
        //  更新烟店大头针
        [mapViewController removeAllAnnotations];
        mapViewController.customersArray = customersArray;
        [mapViewController addAnnotations];
        //  更新地图显示的位置
        MKMapView * mapView = mapViewController.mapView;
        MKCoordinateRegion newRegion = mapView.region;
        newRegion.center.latitude = coordinate.latitude;
        newRegion.center.longitude = coordinate.longitude;
        if (distance == 0.5) {
            newRegion.span.latitudeDelta = 0.01f;
            newRegion.span.longitudeDelta = 0.01f;
        } else if (distance == 1.0){
            newRegion.span.latitudeDelta = 0.02f;
            newRegion.span.longitudeDelta = 0.02f;
        } else if (distance == 2.0){
            newRegion.span.latitudeDelta = 0.03f;
            newRegion.span.longitudeDelta = 0.03f;
        } else if (distance == 5.0){
            newRegion.span.latitudeDelta = 0.06f;
            newRegion.span.longitudeDelta = 0.06f;
        }
        [mapView setRegion:newRegion animated:YES];
    }
    [UIView commitAnimations];
}

//  显示或隐藏搜索表视图
- (IBAction)showOrHideSearchView{
    //  定义搜索表视图的显示动画
    CAKeyframeAnimation * animation; 
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"]; 
    animation.duration = 0.5; 
    animation.delegate = self;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    
    //  控制基于原尺寸 1.0 的放大缩小比例
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]]; 
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]]; 
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]]; 
    animation.values = values;
    
    [searchTable.layer addAnimation:animation forKey:nil];
    
    //  显示或隐藏搜索窗体
    if (searchTable.hidden) {
        searchTable.hidden = NO;
        self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStyleDone;
    }else{
        searchTable.hidden = YES;
        self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStylePlain;
    }
}

//  更新列表视图和地图视图
- (void)reloadTableAndMap{
    isLoading = NO;
    [spinner stopAnimating];
    //  更新烟店列表
    [listViewController setCustomersArray:customersArray loadCounts:loadCounts pageSize:pageSize];
    [listViewController setIsLoading:NO];
    if (isOverload) {
        listViewController.customersTable.contentOffset = CGPointZero;
    }
    //  地图模式
    //  更新烟店大头针
    [mapViewController removeAllAnnotations];
    mapViewController.customersArray = customersArray;
    [mapViewController addAnnotations];
    //  更新地图显示的位置
    MKMapView * mapView = mapViewController.mapView;
    MKCoordinateRegion newRegion = mapView.region;
    newRegion.center.latitude = coordinate.latitude;
    newRegion.center.longitude = coordinate.longitude;
    if (distance == 0.5) {
        newRegion.span.latitudeDelta = 0.01f;
        newRegion.span.longitudeDelta = 0.01f;
    } else if (distance == 1.0){
        newRegion.span.latitudeDelta = 0.02f;
        newRegion.span.longitudeDelta = 0.02f;
    } else if (distance == 2.0){
        newRegion.span.latitudeDelta = 0.03f;
        newRegion.span.longitudeDelta = 0.03f;
    } else if (distance == 5.0){
        newRegion.span.latitudeDelta = 0.06f;
        newRegion.span.longitudeDelta = 0.06f;
    }
    
    [mapView setRegion:newRegion animated:YES];
}

//  加载客户信息
- (void)loadCustomerData{
    if (isLoading) {
        return;
    }
    //  判断是否定位成功
    if (coordinate.latitude == 0) {
        [GlobalUtil showAlertWithTitle:@"定位提示" andMessage:@"正在进行定位，请稍后..."];
        return;
    }
    
    //  判断是否下拉刷新
    if (isOverload) {
        listViewController.customersTable.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
        [listViewController.headerView changeArrowAndLabelState:2];
    }
    [listViewController setIsLoading:YES];
    isLoading = YES;
    loadCounts = 0;
    [spinner startAnimating];
    
    float lat = coordinate.latitude;
    float lng = coordinate.longitude;
    NSString * url = nil;
    if ([icss_hsAppDelegate appDelegate].useInsideNet) {
        url = [NSString stringWithFormat:@"http://10.188.50.238:9088/tmi/ws/rs/mobile/moblieCustLatLng/lat/%f/lng/%f/distance/%f/startPage/%d/endPage/%d?licence=123456789&key=25f9e794323b453885f5181f1b624d0b", lat, lng, distance, startPage, startPage + pageSize - 1];
    } else {
        url = [NSString stringWithFormat:@"http://xm.fj-tobacco.com:9088/tmi/ws/rs/mobile/moblieCustLatLng/lat/%f/lng/%f/distance/%f/startPage/%d/endPage/%d?licence=123456789&key=25f9e794323b453885f5181f1b624d0b", lat, lng, distance, startPage, startPage + pageSize - 1];
    }
    NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

//  解析XML数据
- (void)parseXMLData{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:customersData];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
    [connection cancel];
    [connection release];
    connection = nil;
    [self reloadTableAndMap];
}

//  重新定位
- (IBAction)reLocation{
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell * cell = [searchTable cellForRowAtIndexPath:indexPath];
    cell.textLabel.text = @"正在定位，请稍后...";
    if (![GlobalUtil isIphone]) {
        //  iPad
        [locSpinner startAnimating];
        locationLabel.text = @"正在定位，请稍后...";
    }
    [locationMgr stopUpdatingLocation];
    [locationMgr startUpdatingLocation];
}

#pragma mark - View lifecycle

- (void)viewWillDisappear:(BOOL)animated{
    searchTable.hidden = YES;
    locatingView.hidden = YES;
    [spinner stopAnimating];
    [connection cancel];
    connection = nil;
    [reverseGeocode cancel];
    isLoading = NO;
    isOverload = NO;
    [spinner stopAnimating];
    //  更新烟店列表
    [listViewController setCustomersArray:customersArray loadCounts:loadCounts pageSize:pageSize];
    [listViewController setIsLoading:NO];
    if (isOverload) {
        listViewController.customersTable.contentOffset = CGPointZero;
    }
    [locationMgr stopUpdatingLocation];
    if (![GlobalUtil isIphone]) {
        //  iPad
        [locSpinner stopAnimating];
    }
    NSLog(@"结束定位");
}

- (void)viewWillAppear:(BOOL)animated{
    if (locationMgr != nil) {
        NSLog(@"开始定位");
        locatingView.hidden = NO;
        [locationMgr startUpdatingLocation];
        if (![GlobalUtil isIphone]) {
            //  iPad
            [locSpinner startAnimating];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"周边搜索";
    connectionTimes = 0;
    //  初始化列表模式视图控制器
    CircumSearchListViewController * listView = [[CircumSearchListViewController alloc] init];
    listView.delegate = self;
    listViewController = [listView retain];
    [listView release];
    
    //  初始化地图模式视图控制器 
    CircumSearchMapViewController * mapView = [[CircumSearchMapViewController alloc] init];
    mapView.delegate = self;
    mapViewController = [mapView retain];
    [mapView release];
    
    if ([GlobalUtil isIphone]) {
        
        //  设置显示的视图为列表模式
        currentViewTag = 0;
        [tabBar setSelectedItem:[tabBar.items objectAtIndex:0]];
        [contentView addSubview:listViewController.view];
        
        //  初始化导航栏右侧查询按钮
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"circum_location"] style:UIBarButtonItemStylePlain target:self action:@selector(showOrHideSearchView)];
        
        //  重定义搜索视图，并添加进主视图中
        searchTable.frame = CGRectMake(10, 20, 300, 330);
        searchTable.hidden = YES;
        searchTable.layer.cornerRadius = 10.0f;
        [self.view addSubview:searchTable];
    } else {
        listViewController.view.frame = CGRectMake(0, 44, 370, 391);
        listViewController.view.backgroundColor = [UIColor clearColor];
        [aListView addSubview:listViewController.view];

        mapViewController.view.frame = CGRectMake(0, 44, 750, 421);
        mapViewController.view.backgroundColor = [UIColor clearColor];
        [aMapView addSubview:mapViewController.view];
        
        [locSpinner startAnimating];
        
        //  重定义搜索视图，并添加进主视图中
        searchTable.frame = CGRectMake(30, 70, 300, 330);
        searchTable.hidden = YES;
        searchTable.layer.cornerRadius = 10.0f;
        [aListView addSubview:searchTable];
    }
    
    //  初始化定位对象，开始定位
    locationMgr = [[CLLocationManager alloc] init];
    locationMgr.delegate = self;
    locationMgr.distanceFilter = 100;
    locationMgr.desiredAccuracy = kCLLocationAccuracyBest;
    [locationMgr startUpdatingLocation];
    
    startPage = 1;
    pageSize = 30;
    isOverload = YES;
    distance = 0.5;
    
//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
//    coordinate.latitude = 24.488891047831327;
//    coordinate.longitude = 118.17620121110917;
//    reverseGeocode = [[MKReverseGeocoder alloc] initWithCoordinate:coordinate];
//    reverseGeocode.delegate = self;
//    [reverseGeocode start];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Tab Bar Delegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    //  切换视图
    if (currentViewTag != item.tag) {
        currentViewTag = item.tag;
        [self changeDisplayView:currentViewTag];
    }
}

#pragma mark -
#pragma mark CircumSearchListView Delegate
//  列表视图协议：显示客户信息详情
- (void)showCustomerDetailView:(int)index{
    if ([GlobalUtil isIphone]) {
        //  iPhone
        CustomerDetailViewController * detailView = [[CustomerDetailViewController alloc] init];
        [detailView setCustomer:[customersArray objectAtIndex:index]];
        [self.navigationController pushViewController:detailView animated:YES];
        [detailView release];
    } else {
        //  iPad
        if (detailViewController == nil) {
            CustomerDetailViewController * detailView = [[CustomerDetailViewController alloc] init];
            detailViewController = [detailView retain];
            [detailView release];
            detailViewController.view.frame = CGRectMake(0, 44, 370, 421);
            detailViewController.view.backgroundColor = [UIColor clearColor];
            [aDetailView addSubview:detailViewController.view];
        }
        [detailViewController setCustomer:[customersArray objectAtIndex:index]];
    }
}

//  列表视图协议：向上拖动加载更多
- (void)loadMoreCustomers{
    startPage += pageSize;
    isOverload = NO;
    [self loadCustomerData];
}

//  列表视图协议：下拉刷新
- (void)loadCustomers{
    startPage = 1;
    isOverload = YES;
    [self loadCustomerData];
}

#pragma mark -
#pragma mark CircumSearchMapView Delegate

//  显示客户信息详情
- (void)showCustomerDetail:(long long)index{
    if ([GlobalUtil isIphone]) {
        //  iPhone
        CustomerDetailViewController * detailView = [[CustomerDetailViewController alloc] init];
        [detailView setCustomer:[customersArray objectAtIndex:index]];
        [self.navigationController pushViewController:detailView animated:YES];
        [detailView release];
    } else {
        //  iPad
        if (detailViewController == nil) {
            CustomerDetailViewController * detailView = [[CustomerDetailViewController alloc] init];
            detailViewController = [detailView retain];
            [detailView release];
            detailViewController.view.frame = CGRectMake(0, 44, 370, 421);
            detailViewController.view.backgroundColor = [UIColor clearColor];
            [aDetailView addSubview:detailViewController.view];
        }
        [detailViewController setCustomer:[customersArray objectAtIndex:index]];
    }
}

#pragma mark -
#pragma mark Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    } else {
        return 4;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"当前位置";
    } else {
        return @"搜索精度";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * identifier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    int section = [indexPath section];
    int row = [indexPath row];
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
        if (section == 0) {
            //  显示用户位置
            cell.textLabel.minimumFontSize = 12.0f;
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            
            //  重新定位按钮
            UIButton * relocationButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
            relocationButton.frame = CGRectMake(0, 12, 32, 32);
            [relocationButton setBackgroundImage:[UIImage imageNamed:@"circum_refresh.png"] forState:UIControlStateNormal];
            [relocationButton addTarget:self action:@selector(reLocation) forControlEvents:UIControlStateHighlighted];
            cell.accessoryView = relocationButton;
            [relocationButton release];
        }
    }
    if (section == 0) {
        //  用户位置
        cell.textLabel.text = @"正在定位，请稍后...";
    } else {
        //  查询精度
        if (row == 0) {
            cell.textLabel.text = @"附近 500  米";
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            lastIndexPath = [indexPath retain];
        } else if (row == 1){
            cell.textLabel.text = @"附近 1000 米";
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else if (row == 2){
            cell.textLabel.text = @"附近 2000 米";
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else if (row == 3){
            cell.textLabel.text = @"附近 5000 米";
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    return cell;
}

#pragma mark -
#pragma mark Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    int section = [indexPath section];
    if (section != 0) {
        if (coordinate.latitude == 0.0) {
            [GlobalUtil showAlertWithTitle:@"定位提示" andMessage:@"正在获取您的位置，请稍后..."];
            return;
        }
        //  查询精度
        UITableViewCell * newCell = [tableView cellForRowAtIndexPath:indexPath];
        UITableViewCell * oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
        //  改变选中状态
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        [lastIndexPath release];
        lastIndexPath = [indexPath retain];
        
        //  查询数据
        startPage = 1;
        isOverload = YES;
        int row = [indexPath row];
        if (row == 0) {
            distance = 0.5;
        } else if (row == 1){
            distance = 1.0;
        } else if (row == 2){
            distance = 2.0;
        } else if (row == 3){
            distance = 5.0;
        }
        
        [self performSelector:@selector(showOrHideSearchView) withObject:nil afterDelay:0.3];
        [self loadCustomerData];
    }
}

#pragma mark -
#pragma mark CLLocationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    if (newLocation == oldLocation) {
        return;
    }
    
    //  更新用户位置
    coordinate = newLocation.coordinate;
    locatingView.hidden = YES;
    
    //  查询数据
    [self loadCustomerData];
    
    //  取消反向地理解析
    if (reverseGeocode != nil) {
        [reverseGeocode cancel];
        reverseGeocode = nil;
    }
    //  开始反向解析
    if (reverseGeocode != nil) {
        [reverseGeocode cancel];
        [reverseGeocode release];
        reverseGeocode = nil;
    }
    reverseGeocode = [[MKReverseGeocoder alloc] initWithCoordinate:coordinate];
    reverseGeocode.delegate = self;
    [reverseGeocode start];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    [GlobalUtil showAlertWithTitle:@"定位失败" andMessage:[NSString stringWithFormat:@"%@", error]];
    if (![GlobalUtil isIphone]) {
        //  iPad
        [locSpinner stopAnimating];
        locationLabel.text = @"定位失败，请重试...";
    }
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell * cell = [searchTable cellForRowAtIndexPath:indexPath];
    cell.textLabel.text = @"定位失败，请重试...";
}

#pragma mark -
#pragma mark MKReverseGeocode Delegate
- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error{
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell * cell = [searchTable cellForRowAtIndexPath:indexPath];
    cell.textLabel.text = @"正在获取您的街道地址..";
    if (![GlobalUtil isIphone]) {
        //  iPad
        [locSpinner stopAnimating];
        locationLabel.text = @"正在获取您的街道地址..";
    }
    [reverseGeocode cancel];
    [reverseGeocode start];
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark{
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell * cell = [searchTable cellForRowAtIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%@%@%@%@", placemark.locality, placemark.subLocality, placemark.thoroughfare, placemark.subThoroughfare];
    if (![GlobalUtil isIphone]) {
        //  iPad
        [locSpinner stopAnimating];
        locationLabel.text = cell.textLabel.text;
    }
}

#pragma mark - 
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"连接异常：%@", error);
    [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:@"请求数据发生异常。"];
    //  改变下拉刷新视图状态
    [listViewController.headerView changeArrowAndLabelState:0];
    [self reloadTableAndMap];
}

- (void)connection:(NSURLConnection *)conn didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *) response;
    if((httpURLResponse.statusCode / 100) != 2){
        if (httpURLResponse.statusCode == 500 && connectionTimes < 3) {
            [connection cancel];
            [connection release];
            connection = nil;
            connectionTimes++;
            NSLog(@"第 %d 次连接", connectionTimes);
            [self loadCustomerData];
        } else {
            connectionTimes = 0;
            [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:[NSString stringWithFormat:@"网络响应异常：%d", httpURLResponse.statusCode]];
            [self reloadTableAndMap];
        }
        NSLog(@"网络响应异常: %@", [[httpURLResponse allHeaderFields] description]);
        //  改变下拉刷新视图状态
        [listViewController.headerView changeArrowAndLabelState:0];
    }else{
        //  释放原先的data
        if (customersData != nil) {
            [customersData release];
            customersData = nil;
        }
        //  接受到响应时，初始化salesAnalyseData
        customersData = [[NSMutableData alloc] init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [customersData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [self parseXMLData];
    //  改变下拉刷新视图状态
    [listViewController.headerView changeArrowAndLabelState:0];
    listViewController.customersTable.contentInset = UIEdgeInsetsZero;
}

//  重写 NSXMLParser 协议
#pragma mark -
#pragma mark XMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    //  xml 全局key开始
    if([elementName isEqualToString:@"MoblieCustLatLngs"]){
        //  初始化数组
        if(customersArray == nil){
            customersArray = [[NSMutableArray alloc] init];
        }else if (isOverload){
            [customersArray removeAllObjects];
        }
    }
    //  xml 每个对象key开始
    if([elementName isEqualToString:@"MoblieCustLatLng"]){
        //  初始化销售分析对象
        customer = [[Customer alloc]init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    //  保存各值
    aValue = [NSString stringWithFormat:@"%@", string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    //  根据值key，保存值
    if([elementName isEqualToString:@"custAddress"]){
        customer.address = aValue;
    }else if([elementName isEqualToString:@"custCode"]){
        customer.customerId = aValue;
    }else if([elementName isEqualToString:@"custLevel"]){
        customer.level = aValue;
    }else if([elementName isEqualToString:@"custLicenceCode"]){
        customer.licenceCode = aValue;
    }else if([elementName isEqualToString:@"custMgrName"]){
        customer.mgrName = aValue;
    }else if([elementName isEqualToString:@"custName"]){
        customer.name = aValue;
    } else if([elementName isEqualToString:@"custOrderDate"]){
        customer.orderDate = aValue;
    }else if([elementName isEqualToString:@"custPhoneNumber"]){
        customer.phone = aValue;
    }else if([elementName isEqualToString:@"distance"]){
        customer.distance = aValue;
    }else if([elementName isEqualToString:@"lat"]){
        customer.latitude = aValue;
    }else if([elementName isEqualToString:@"lng"]){
        customer.longitude = aValue;
        NSLog(@"%@", [customer toString]);
        //  将解析好的销售分析对象添加进数组中，在xml接口格式中，此key是最后一项，因此在这里添加
        customer.index = [customersArray count];
        [customersArray addObject:customer];
        [customer release];
        customer = nil;
        loadCounts++;
    }
    aValue = nil;
}

@end
