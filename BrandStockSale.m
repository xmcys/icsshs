//
//  BrandStockSale.m
//  icss-hs
//
//  Created by hsit on 11-10-10.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "BrandStockSale.h"


@implementation BrandStockSale

@synthesize brandId;                   // 卷烟ID
@synthesize brandName;                 // 卷烟名称
@synthesize qtyStockBuy;               // 期初库存
@synthesize qtyStockEnd;               // 购进入库
@synthesize qtyStockDraw;              // 领货出库
@synthesize qtyStockSale;              // 期末库存
@synthesize qtyStockBegin;             // 销售出库
@synthesize qtyStockWaste;             // 本期溢耗
@synthesize qtyStockTranOut;           // 移库出库
@synthesize qtyStockWithdraw;          // 退货入库
@synthesize qtyStockBeginOriginal;     // 原始期初
@synthesize qtyStockBuyOriginal;       // 原始购进
@synthesize qtyStockDrawOriginal;      // 原始领货
@synthesize qtyStockEndOriginal;       // 原始期末
@synthesize qtyStockSaleOriginal;      // 原始销售
@synthesize qtyStockTranOutOriginal;   // 原始移库
@synthesize qtyStockWasteOriginal;     // 原始溢耗
@synthesize qtyStockWithdrawOriginal;  // 原始退货

- (void)dealloc{
    [brandId release];
    [brandName release];
    [qtyStockBuy release];
    [qtyStockBuyOriginal release];
    [qtyStockBegin release];
    [qtyStockBeginOriginal release];
    [qtyStockDraw release];
    [qtyStockDrawOriginal release];
    [qtyStockEnd release];
    [qtyStockEndOriginal release];
    [qtyStockSale release];
    [qtyStockSaleOriginal release];
    [qtyStockTranOut release];
    [qtyStockTranOutOriginal release];
    [qtyStockWaste release];
    [qtyStockWasteOriginal release];
    [qtyStockWithdraw release];
    [qtyStockWithdrawOriginal release];
    [super dealloc];
}

//  将所有属性封装成NSString输出
- (NSString *)toString{
    return [NSString stringWithFormat:@"卷烟ID：%@\n卷烟名称：%@\n期初库存：%@\n购进入库：%@\n领货出库：%@\n期末库存：%@\n退货入库：%@\n销售出库：%@\n移库出库：%@\n本期溢耗：%@\n", brandId, brandName, qtyStockBeginOriginal, qtyStockBuyOriginal, qtyStockDrawOriginal, qtyStockEndOriginal, qtyStockWithdrawOriginal, qtyStockSaleOriginal, qtyStockTranOutOriginal, qtyStockWasteOriginal];
}

@end
