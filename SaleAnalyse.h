//
//  销售分析类
//  SaleAnalyse.h
//  icss-hs
//
//  Created by hsit on 11-9-26.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SaleAnalyse : NSObject {
    
}

@property (nonatomic, retain) NSString * brandId;   //  卷烟id
@property (nonatomic, retain) NSString * date;      //  日期
@property (nonatomic, retain) NSString * brandName; //  卷烟名称
@property (nonatomic, retain) NSString * require;   //  需求量
@property (nonatomic, retain) NSString * order;     //  订单量
@property (nonatomic, retain) NSString * rate;      //  满足率
@property (nonatomic, retain) NSString * cash;      //  金额

// 将所有属性封装成NSString输出
- (NSString *)toString;
@end
