//
//  地图模式视图控制器
//  CircumSearchMapViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-19.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

//  自定义地图模式协议
@protocol CircumSearchMapViewDelegate <NSObject>

@optional

//  显示客户详情
-(void)showCustomerDetail:(long long)index;

@end
@interface CircumSearchMapViewController : UIViewController <MKMapViewDelegate> {
}

@property (nonatomic, assign) id<CircumSearchMapViewDelegate>     delegate;        //  地图模式协议
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView  * mainSpinner;     //  加载指示器
@property (nonatomic, retain) IBOutlet MKMapView                * mapView;         //  地图
@property (nonatomic, assign) NSArray                           * customersArray;  //  客户信息数组

//  添加大头针
- (void)addAnnotations;

//  移除所有大头针
- (void)removeAllAnnotations;

//  大头针右侧详情按钮事件
- (void)clickDetailButton:(id)sender;

@end
