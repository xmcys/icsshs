//
//  全局工具类
//  GlobalUtil.m
//  icss-hs
//
//  Created by hsit on 11-9-21.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "GlobalUtil.h"


@implementation GlobalUtil

//  判断网络是否可用
+ (BOOL) isReachable{
    Reachability * reachability = [Reachability reachabilityForInternetConnection];
    if([reachability currentReachabilityStatus] != NotReachable){
        return YES;
    }else{
        return NO;
    }
}

//  判断字符串是否由纯数字组成
+ (BOOL)isHasNumberOnly:(NSString *)string{
    if ([string length] == 0) {
        return NO;
    }
    BOOL only = YES;
    for(int i = 0; i < [string length]; i++){
        unichar a = [string characterAtIndex:i];
        if(a < '0' || a > '9'){
            only = NO;
            break;
        }
    }
    return only;
}

//  判断是否时闰年
+ (BOOL) isLeapYear:(int)year{
    if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
        return YES;
    }else{
        return NO;
    }
}

//  判断是否为小月
+ (BOOL) isSmallMonth:(int)month{
    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        return NO;
    } else {
        return YES;
    }
}

// 弹出消息框，参数为标题和内容
+ (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message{
    UIAlertView * alert = [[UIAlertView alloc] 
                           initWithTitle:title
                           message:message 
                           delegate:nil
                           cancelButtonTitle:@"确定"
                           otherButtonTitles: nil];
    [alert show];
    [alert release];
}

//  初始化搜索周期：tag 0为开始日期，1为结束日期
+ (NSString *)initSearchDate:(NSInteger)tag{
    NSDate * date = [NSDate date];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    [dateFormatter setFormatterBehavior:NSDateFormatterFullStyle];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString * dateString = [dateFormatter stringFromDate:date];
    [dateFormatter release];
    int year = [[dateString substringToIndex:4] intValue];
    NSRange range = NSRangeFromString(dateString);
    range.location = 4;
    range.length = 2;
    int month = [[dateString substringWithRange:range] intValue];
    int day = [[dateString substringFromIndex:6] intValue];
    if (day == 1 && month == 1) {
        year -= 1;
        day = 31;
        month = 12;
    } else if (day == 1) {
        month -= 1;
        if (month == 2 && [GlobalUtil isLeapYear:year]) {
            day = 29;
        } else if (month == 2){
            day = 28;
        } else if ([GlobalUtil isSmallMonth:month]){
            day = 30;
        } else {
            day = 31;
        }
    } else {
        day -= 1;
    }
    NSMutableString * newDate = [[[NSMutableString alloc] init] autorelease];
    [newDate appendString:[NSString stringWithFormat:@"%d", year]];
    if (month < 10) {
        [newDate appendString:[NSString stringWithFormat:@"0%d", month]];
    } else {
        [newDate appendString:[NSString stringWithFormat:@"%d", month]];
    }
    if (day < 10) {
        [newDate appendString:[NSString stringWithFormat:@"0%d", day]];
    } else {
        [newDate appendString:[NSString stringWithFormat:@"%d", day]];
    }
    if(tag == 1){
        return newDate;
    }else{
        return [NSString stringWithFormat:@"%@01", [newDate substringToIndex:6]];
    }
}

//  获取数据库文件路径
+ (NSString *)getApplicationFilePath{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory = [paths objectAtIndex:0];
    NSString * dbName = @"icss_hs.sqlite3";  //  数据库名称
    return [documentDirectory stringByAppendingPathComponent:dbName];
}

//  判断是否是iPhone
+ (BOOL) isIphone{
    return [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone;
}

//  获取当前时间： 2011-10-21 15:40:32
+ (NSString *)getCurrentFullTime{
    NSDate * date = [NSDate date];
    NSDateFormatter * dateFormatter = [[[NSDateFormatter alloc]init] autorelease];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormatter setFormatterBehavior:NSDateFormatterFullStyle];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    return [dateFormatter stringFromDate:date];
}

@end
