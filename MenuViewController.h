//
//  菜单视图控制器
//  MenuViewController.h
//  icss-hs
//
//  Created by hsit on 11-9-21.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SalesAnalyseViewController;
@class PSIViewController;
@class CircumSearchViewController;
@class RTSalesViewController;
@class SettingsViewController;

@interface MenuViewController : UIViewController {
    
}
//  销售分析视图控制器
@property (nonatomic, retain) SalesAnalyseViewController * salesAnalyseViewController;
//  进销存分析视图控制器
@property (nonatomic, retain) PSIViewController          * psiViewController;
//  周边搜索视图控制器
@property (nonatomic, retain) CircumSearchViewController * circumSearchViewController;
//  实时订单视图控制器
@property (nonatomic, retain) RTSalesViewController      * rtSalesViewController;

@property (nonatomic, retain) SettingsViewController     * settingsViewController;

- (IBAction)clickSalesAnalyse;             //  点击销售分析按钮
- (IBAction)clickPurcheaseSalesInventory;  //  点击进销存分析按钮
- (IBAction)clickCircumSearch;             //  点击周边搜索按钮
- (IBAction)clickRealTimeSales;            //  点击实时订单按钮
- (IBAction)clickSettings;                 //  点击系统设置按钮

@end
