//
//  地图模式视图控制器
//  CircumSearchMapViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-19.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "CircumSearchMapViewController.h"
#import "GlobalUtil.h"
#import "Customer.h"
#import "CustomerAnnotation.h"

@implementation CircumSearchMapViewController

@synthesize delegate;        //  地图模式协议
@synthesize mainSpinner;     //  加载指示器
@synthesize mapView;         //  地图
@synthesize customersArray;  //  客户信息数组

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [mainSpinner release];
    [mapView release];
    [delegate release];
    [customersArray release];
    [super dealloc];
}

- (void)viewDidUnload
{
    mainSpinner = nil;
    mapView = nil;
    delegate = nil;
    customersArray = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  移除所有大头针
- (void)removeAllAnnotations{
    [mapView removeAnnotations:mapView.annotations];
}

//  添加大头针
- (void)addAnnotations{

    //  遍历客户数组，定义大头针，添加
    for (int i = 0; i < [customersArray count]; i++) {
        Customer * customer = [customersArray objectAtIndex:i];
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [customer.latitude doubleValue];
        coordinate.longitude = [customer.longitude doubleValue];
        CustomerAnnotation * cusAnn = [[CustomerAnnotation alloc] initWithCoordinate2D:coordinate];
        [cusAnn setIndex:customer.index];
        [cusAnn setTitle:customer.name];
        [cusAnn setSubtitle:customer.address];
        [mapView addAnnotation:cusAnn];
        [cusAnn release];
    }
}

//  大头针右侧详情按钮事件
- (void)clickDetailButton:(id)sender{
    UIButton * aButton = (UIButton *)sender;
    //  判断协议是否被实现
    if ([delegate respondsToSelector:@selector(showCustomerDetail:)]) {
        //  显示客户详情
        [delegate showCustomerDetail:aButton.tag];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"周边搜索";
    
    //  显示用户位置
    mapView.showsUserLocation = YES;
    mapView.userLocation.title = @"当前位置";
    //  设置地图中心
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = mapView.userLocation.location.coordinate.latitude;
    newRegion.center.longitude = mapView.userLocation.location.coordinate.longitude;
    //  设置显示范围，越小越精确
    newRegion.span.latitudeDelta = 0.01f;
    newRegion.span.longitudeDelta = 0.01f;
    [mapView setRegion:newRegion animated:YES];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - 
#pragma mark MKMapView Delegate
- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id<MKAnnotation>)annotation{
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        //  让显示用户的大头针使用默认样式：蓝点
        return nil;
    } else {
        //  定义大头针样式
        static NSString * identifier = @"pinAnn";
        MKPinAnnotationView * pinAnnView = (MKPinAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (pinAnnView == nil) {
            pinAnnView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier] autorelease];
            pinAnnView.animatesDrop = YES;
            pinAnnView.canShowCallout = YES;
            pinAnnView.pinColor = MKPinAnnotationColorGreen;
            
            //  定义大头针的CalloutView右侧按钮
            UIButton * rightCallOutAccessoryView = [[UIButton buttonWithType:UIButtonTypeDetailDisclosure] retain];
            CustomerAnnotation * cusAnn = (CustomerAnnotation *)annotation;
            rightCallOutAccessoryView.tag = cusAnn.index;
            [rightCallOutAccessoryView addTarget:self action:@selector(clickDetailButton:) forControlEvents:UIControlStateHighlighted];
            [pinAnnView setRightCalloutAccessoryView:rightCallOutAccessoryView];
            [rightCallOutAccessoryView release];
        }
        return pinAnnView; 
    }
    
}

- (void)mapView:(MKMapView *)aMapView didSelectAnnotationView:(MKAnnotationView *)view{
    //  如果点击的是用户大头针，则返回
    if ([view.annotation isKindOfClass:[MKUserLocation class]]) {
        return;
    }
    //  设置大头针为红色
    MKPinAnnotationView * pinAnnView = (MKPinAnnotationView *)view;
    pinAnnView.pinColor = MKPinAnnotationColorRed;
}

- (void)mapView:(MKMapView *)aMapView didDeselectAnnotationView:(MKAnnotationView *)view{
    //  如果取消选中的是用户大头针，则返回
    if ([view.annotation isKindOfClass:[MKUserLocation class]]) {
        return;
    }
    //  设置大头针为绿色
    MKPinAnnotationView * pinAnnView = (MKPinAnnotationView *)view;
    pinAnnView.pinColor = MKPinAnnotationColorGreen;
}

- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error{
    NSLog(@"%@", error);
}

- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView{
    [mainSpinner startAnimating];
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView{
    [mainSpinner stopAnimating];
}

- (void)mapViewDidStopLocatingUser:(MKMapView *)mapView{
    NSLog(@"结束定位用户");
}

- (void)mapViewWillStartLocatingUser:(MKMapView *)mapView{
    NSLog(@"开始定位用户");
}



@end
