//
//  全局工具类
//  GlobalUtil.h
//  icss-hs
//
//  Created by hsit on 11-9-21.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface GlobalUtil : NSObject {
    
}

//  判断网络是否可用
+ (BOOL) isReachable;

//  判断字符串是否由纯数字组成
+ (BOOL) isHasNumberOnly:(NSString *)string;

//  判断是否时闰年 
+ (BOOL) isLeapYear:(int)year;

//  判断是否为小月
+ (BOOL) isSmallMonth:(int)month;

//  弹出提示框，传入参数为标题和内容
+ (void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message;

//  初始化搜索周期：tag 0为开始日期，1为结束日期
+ (NSString *)initSearchDate:(NSInteger)tag;

//  获取数据库文件路径
+ (NSString *)getApplicationFilePath;

//  判断是否是iPhone
+ (BOOL) isIphone;

//  获取当前时间： 2011-10-21 15:40:32
+ (NSString *)getCurrentFullTime;

@end
