//
//  销售分析数据库操作表
//  SalesAnalyseDatabase.m
//  icss-hs
//
//  Created by hsit on 11-10-11.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "SalesAnalyseDatabase.h"
#import "GlobalUtil.h"
#import "icss_hsAppDelegate.h"
#import "SaleAnalyse.h"
#import "Database.h"


@implementation SalesAnalyseDatabase

//  创建销售分析表
- (BOOL)createTable{
    //  用 if not exists 判断是否要创建新表
    NSString * createSQL = @"create table if not exists SALES_ANALYSE (id integer primary key, brand_id text, brand_name text, aDate text, require text, rate text, aOrder text, cash text)";
    return [[icss_hsAppDelegate appDelegate].database createTable:createSQL tableNameWithCHN:@"销售分析"];
}

//  保存数据
//  array : SalesAnalyse对象数组  date : 最后一次查询周期
- (BOOL)saveData:(NSMutableArray *)array searchDate:(NSString *)date{
    BOOL succeed = NO;
    //  遍历数组对象，并保存到数据库中
    for (int i = 0; i < [array count]; i++) {
        SaleAnalyse * item = [array objectAtIndex:i];
        NSString * saveSql = [[NSString alloc] initWithFormat:@"insert into SALES_ANALYSE values(%d, '%@', '%@', '%@', '%@', '%@', '%@', '%@')", i, item.brandId, item.brandName, item.date, item.require, item.rate, item.order, item.cash];
        succeed = [[icss_hsAppDelegate appDelegate].database saveData:saveSql tableNameWithCHN:@"销售分析"]; 
        [saveSql release];
    }
    //  创建一条用于保存最后一次查询周期的SalesAnalyse数据（节省一张表）
    NSString * saveSql = [[NSString alloc] initWithFormat:@"insert into SALES_ANALYSE values(%d, '%@', '', '', '', '', '', '')", [array count], date];
    succeed = [[icss_hsAppDelegate appDelegate].database saveData:saveSql tableNameWithCHN:@"销售分析"]; 
    [saveSql release];
    return succeed;
}

//  删除所有数据
- (BOOL)deleteAllData{
    NSString * deleteSql = @"delete from SALES_ANALYSE";
    return [[icss_hsAppDelegate appDelegate].database deleteData:deleteSql tableNameWithCHN:@"销售分析"];
}

//  查询所有数据 
//  返回包含一条brandId为“最后一次查询周期”的记录
- (NSMutableArray *)queryAllData{
    NSString * querySql = @"select * from SALES_ANALYSE";
    NSMutableArray * array = [[[[icss_hsAppDelegate appDelegate].database queryData:querySql tableNameWithCHN:@"销售分析" columnsCount:8] copy] autorelease];
    //  将 NSArray对象数组重新封装成 SalesAnalyse对象数组
    NSMutableArray * returnArray = [[[NSMutableArray alloc] init] autorelease];
    for (int i = 0; i < [array count]; i++) {
        SaleAnalyse * sa = [[SaleAnalyse alloc] init];
        NSArray * item = [array objectAtIndex:i];
        sa.brandId = [item objectAtIndex:0];
        sa.brandName = [item objectAtIndex:1];
        sa.date = [item objectAtIndex:2];
        sa.require = [item objectAtIndex:3];
        sa.rate = [item objectAtIndex:4];
        sa.order = [item objectAtIndex:5];
        sa.cash = [item objectAtIndex:6];
        [returnArray addObject:sa];
        [sa release];
    }
    return returnArray;
}

@end
