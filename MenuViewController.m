//
//  菜单视图控制器
//  MenuViewController.m
//  icss-hs
//
//  Created by hsit on 11-9-21.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "MenuViewController.h"
#import "icss_hsAppDelegate.h"
#import "SalesAnalyseViewController.h"
#import "PSIViewController.h"
#import "CircumSearchViewController.h"
#import "RTSalesViewController.h"
#import "SettingsViewController.h"
#import "GlobalUtil.h"


@implementation MenuViewController

@synthesize salesAnalyseViewController;  //  销售分析视图控制器
@synthesize psiViewController;           //  进销存分析视图控制器
@synthesize circumSearchViewController;  //  周边搜索视图控制器
@synthesize rtSalesViewController;       //  实时订单视图控制器
@synthesize settingsViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [salesAnalyseViewController release];
    [psiViewController release];
    [circumSearchViewController release];
    [rtSalesViewController release];
    [settingsViewController release];
    [super dealloc];
}

- (void)viewDidUnload
{
    self.salesAnalyseViewController = nil;
    self.psiViewController = nil;
    self.circumSearchViewController = nil;
    self.rtSalesViewController = nil;
    self.settingsViewController = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  点击进销存分析按钮
- (IBAction)clickPurcheaseSalesInventory{
    if (self.psiViewController == nil) {
        PSIViewController * psiView = nil;
        if ([GlobalUtil isIphone]) {
            // iPhone
            psiView = [[PSIViewController alloc] initWithNibName:@"PSIViewController" bundle:nil];
        } else {
            // iPad
            psiView = [[PSIViewController alloc] initWithNibName:@"PSIViewController-iPad" bundle:nil];
        }
        self.psiViewController = psiView;
        [psiView release];
    }
    [self.navigationController pushViewController:psiViewController animated:YES];
}

//  点击销售分析按钮
- (IBAction)clickSalesAnalyse{
    if(self.salesAnalyseViewController == nil){
        SalesAnalyseViewController * salesAnalyseView  = nil;
        if ([GlobalUtil isIphone]) {
            // iPhone
            salesAnalyseView = [[SalesAnalyseViewController alloc] initWithNibName:@"SalesAnalyseViewController" bundle:nil];
        } else {
            // iPad
            salesAnalyseView = [[SalesAnalyseViewController alloc] initWithNibName:@"SalesAnalyseViewController-iPad" bundle:nil];
        }
        self.salesAnalyseViewController = salesAnalyseView;
        [salesAnalyseView release];
    }
    [self.navigationController pushViewController:self.salesAnalyseViewController animated:YES];
}

//  点击周边搜索按钮
- (IBAction)clickCircumSearch{
    if (self.circumSearchViewController == nil) {
        CircumSearchViewController * circumSearch = nil;
        if ([GlobalUtil isIphone]) {
            // iPhone
            circumSearch = [[CircumSearchViewController alloc] initWithNibName:@"CircumSearchViewController" bundle:nil];
        }else{
            // iPad
            circumSearch = [[CircumSearchViewController alloc] initWithNibName:@"CircumSearchViewController-iPad" bundle:nil];
        }
        self.circumSearchViewController = circumSearch;
        [circumSearch release];
    }
    [self.navigationController pushViewController:self.circumSearchViewController animated:YES];
}

//  点击实时订单按钮
- (void)clickRealTimeSales{
    if (!self.rtSalesViewController) {
        RTSalesViewController * salesView = nil;
        if ([GlobalUtil isIphone]) {
            // iPhone
            salesView = [[RTSalesViewController alloc] initWithNibName:@"RTSalesViewController" bundle:nil];
        } else {
            // iPad
            salesView = [[RTSalesViewController alloc] initWithNibName:@"RTSalesViewController-iPad" bundle:nil];
        }
        self.rtSalesViewController = salesView;
        [salesView release];
    }
    [self.navigationController pushViewController:self.rtSalesViewController animated:YES];
}

- (IBAction)clickSettings{
    if (settingsViewController == nil) {
        if ([GlobalUtil isIphone]) {
            settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
        } else {
            settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController_iPad" bundle:nil];
        }
        
    }
    [self.navigationController pushViewController:self.settingsViewController animated:YES];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    self.navigationItem.title = @"掌上烟草";
    if ([GlobalUtil isIphone]) {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iphone_bg.jpg"]];
    } else {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ipad_bg.jpg"]];
    }
    [super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
