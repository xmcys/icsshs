//
//  类别烟信息（目前有5类）
//  ClassifySales.h
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ClassifySales : NSObject {
    
}

@property (nonatomic, retain) NSString * typeCode;       //  类别代码 1-5
@property (nonatomic, retain) NSString * classifyName;   //  类别名称 一类、二类 …… 五类
@property (nonatomic, retain) NSString * demand;         //  需求量
@property (nonatomic, retain) NSString * demandPercent;  //  需求量百分比
@property (nonatomic, retain) NSString * order;          //  订单量
@property (nonatomic, retain) NSString * orderPercent;   //  订单量百分比
@property (nonatomic, retain) NSString * cash;           //  金额
@property (nonatomic, retain) NSString * cashPercent;    //  金额百分比
@property (nonatomic, retain) NSString * rate;           //  满足率

//  参数化类对象为字符串输出
- (NSString *)toString;

@end
