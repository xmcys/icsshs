//
//  类别烟信息（目前有5类）
//  ClassifySales.m
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "ClassifySales.h"


@implementation ClassifySales

@synthesize typeCode;       //  类别代码 1-5
@synthesize classifyName;   //  类别名称 一类、二类 …… 五类
@synthesize demand;         //  需求量
@synthesize demandPercent;  //  需求量百分比
@synthesize order;          //  订单量
@synthesize orderPercent;   //  订单量百分比
@synthesize cash;           //  金额
@synthesize cashPercent;    //  金额百分比
@synthesize rate;           //  满足率

- (void)dealloc{
    [typeCode release];
    [classifyName release];
    [demand release];
    [demandPercent release];
    [order release];
    [orderPercent release];
    [cash release];
    [cashPercent release];
    [rate release];
    [super dealloc];
}

//  参数化类对象为字符串输出
- (NSString *)toString{
    return [NSString stringWithFormat:@"\nClassify Info:\nTypeCode: %@\nClassifyName: %@\nDemand: %@\nDemandPercent: %@\nOrder: %@\nOrderPercent: %@\nCash: %@\nCashPercent: %@\nRate: %@", typeCode, classifyName, demand, demandPercent, order, orderPercent, cash, cashPercent, rate];
}

@end
