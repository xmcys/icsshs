//
//  上拉加载视图
//  LoadMoreFooterView.h
//  icss-hs
//
//  Created by hsit on 11-10-21.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoadMoreFooterView : UIView {
    
}

@property (nonatomic, retain) UIActivityIndicatorView * spinner;         //  加载指示器
@property (nonatomic, retain) UIImageView             * arrowImageView;  //  箭头
@property (nonatomic, retain) UILabel                 * displayLabel;    //  提示文本

//  改变视图各控件状态。  0 正常 1 拖动 2 加载 3 已加载全部数据
- (void)changeArrowAndLabelState:(int)actionTag;

@end
