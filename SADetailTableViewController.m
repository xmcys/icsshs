//
//  销售分析 单条卷烟详情 视图控制器
//  SADetailTableViewController.m
//  icss-hs
//
//  Created by hsit on 11-9-23.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "SADetailTableViewController.h"
#import "SaleAnalyse.h"
#import "icss_hsAppDelegate.h"
#import "GlobalUtil.h"
#import "SADetailCurveViewController.h"

@implementation SADetailTableViewController

@synthesize tableView, array, data, saleAnalyse, spinner;
@synthesize totalCashLabel, totalOrderLabel, totalRateLabel, totalRequireLabel;
@synthesize request, connection, curveViewController;
@synthesize tmpValue, tmpSaleAnalyse, startDate, endDate;
@synthesize bottomView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [tableView release];
    [array release];
    [totalRateLabel release];
    [totalRequireLabel release];
    [totalCashLabel release];
    [totalOrderLabel release];
    [request release];
    [connection release];
    [data release];
    [tmpValue release];
    [tmpSaleAnalyse release];
    [spinner release];
    [startDate release];
    [endDate release];
    [curveViewController release];
    [bottomView release];
    [super dealloc];
}

- (void)viewDidUnload
{
    self.tableView = nil;
    self.array = nil;
    self.totalCashLabel = nil;
    self.totalOrderLabel = nil;
    self.totalRateLabel = nil;
    self.totalRequireLabel = nil;
    self.request = nil;
    self.connection = nil;
    self.data = nil;
    self.tmpSaleAnalyse = nil;
    self.tmpValue = nil;
    self.spinner = nil;
    self.startDate = nil;
    self.endDate = nil;
    self.curveViewController = nil;
    bottomView = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

//  初始化卷烟信息
- (void)initCigaretteInfo:(SaleAnalyse *)sa startDate:(NSString *)start endDate:(NSString *)end curveViewController:(SADetailCurveViewController * )curveView{
    self.saleAnalyse = sa;
    self.startDate = start;
    self.endDate = end;
    self.curveViewController = curveView;
    //  设置视图上的总计值
    self.totalRequireLabel.text = [NSString stringWithFormat:@"%.1f", [self.saleAnalyse.require floatValue]];
    self.totalCashLabel.text = [NSString stringWithFormat:@"%.1f", [self.saleAnalyse.cash floatValue]];
    self.totalOrderLabel.text = [NSString stringWithFormat:@"%.1f", [self.saleAnalyse.order floatValue]];
    self.totalRateLabel.text = [NSString stringWithFormat:@"%@%%", self.saleAnalyse.rate];
    
    //  设置曲线视图上的总计值
    [self.curveViewController 
     initLabelsWithDate:[NSString stringWithFormat:@"%@ - %@", start, end]
     require:[NSString stringWithFormat:@"%.1f", [self.saleAnalyse.require floatValue]] 
     order:[NSString stringWithFormat:@"%.1f", [self.saleAnalyse.order floatValue]]
     cash:[NSString stringWithFormat:@"%.1f", [self.saleAnalyse.cash floatValue]]
     rate:[NSString stringWithFormat:@"%@%%", self.saleAnalyse.rate]];
}

//  加载数据
- (void)loadData{
    BOOL isReachable = [GlobalUtil isReachable];
    if(!isReachable){
        [GlobalUtil showAlertWithTitle:@"网络故障" andMessage:@"当前网络不可用"];
    }else{
        [spinner startAnimating];
        NSString * url = nil;
        if ([icss_hsAppDelegate appDelegate].useInsideNet) {
            url = [[NSString alloc]initWithFormat:@"http://10.188.50.238:9088/tmi/ws/rs/mobile/queryBrandDetail/brandId/%@/startMonth/%@/endMonth/%@?licence=123456789&key=25f9e794323b453885f5181f1b624d0b", saleAnalyse.brandId, startDate, endDate];
        } else {
            url = [[NSString alloc]initWithFormat:@"http://xm.fj-tobacco.com:9088/tmi/ws/rs/mobile/queryBrandDetail/brandId/%@/startMonth/%@/endMonth/%@?licence=123456789&key=25f9e794323b453885f5181f1b624d0b", saleAnalyse.brandId, startDate, endDate];
        }
        NSLog(@"%@", url);
        request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
        connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [url release];
    }
}

//  取消加载
- (void)cancelLoading{
    if (self.connection != nil) {
        [connection cancel];
    }
    self.connection = nil;
    self.request = nil;
}

// 解析XML
- (void)parserXML{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:self.data];
    [parser setDelegate:self];
    [parser parse];
    //  延迟1秒，调用重载表视图方法
    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:1.0];
    [parser release];
}

//  重载表视图
- (void)reloadTable{
    [self cancelLoading];
    [self.tableView reloadData];
    [spinner stopAnimating];
    //  绘制曲线视图
    [curveViewController setAxisDatas:array];
    [curveViewController drawGraph];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    connectionTimes = 0;
    self.tableView.separatorColor = [UIColor darkGrayColor];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark TabelView Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSUInteger count = [self.array count];
    if(count > 0){
        return count;
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifier = @"cell";
    
    UITableViewCell * cell = [tableView1 dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        //  定义单元格样式
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        
        cell.backgroundColor = [UIColor clearColor];
        
        //  定义 日期 列
        UILabel * dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
        dateLabel.textAlignment = UITextAlignmentCenter;
        dateLabel.font = [UIFont systemFontOfSize:14.0];
        dateLabel.tag = 1;
        dateLabel.backgroundColor = [UIColor clearColor];
        dateLabel.textColor = [UIColor lightTextColor];
        [cell.contentView addSubview:dateLabel];
        [dateLabel release];
        
        //  定义 需求量 列
        UILabel * requireLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 60, 30)];
        requireLabel.textAlignment = UITextAlignmentCenter;
        requireLabel.font = [UIFont systemFontOfSize:14.0];
        requireLabel.tag = 2;
        requireLabel.backgroundColor = [UIColor clearColor];
        requireLabel.textColor = [UIColor lightTextColor];
        [cell.contentView addSubview:requireLabel];
        [requireLabel release];
        
        //  定义 订单量 列
        UILabel * orderLabel = [[UILabel alloc] initWithFrame:CGRectMake(120, 0, 60, 30)];
        orderLabel.textAlignment = UITextAlignmentCenter;
        orderLabel.font = [UIFont systemFontOfSize:14.0];
        orderLabel.tag = 3;
        orderLabel.backgroundColor = [UIColor clearColor];
        orderLabel.textColor = [UIColor lightTextColor];
        [cell.contentView addSubview:orderLabel];
        [orderLabel release];
        
        //  定义 金额 列
        UILabel * cashLabel = [[UILabel alloc] initWithFrame:CGRectMake(180, 0, 80, 30)];
        cashLabel.textAlignment = UITextAlignmentCenter;
        cashLabel.font = [UIFont systemFontOfSize:14.0];
        cashLabel.tag = 4;
        cashLabel.backgroundColor = [UIColor clearColor];
        cashLabel.textColor = [UIColor lightTextColor];
        [cell.contentView addSubview:cashLabel];
        [cashLabel release];
        
        // 定义 满足率 列
        UILabel * rateLabel = [[UILabel alloc] initWithFrame:CGRectMake(260, 0, 60, 30)];
        rateLabel.textAlignment = UITextAlignmentCenter;
        rateLabel.font = [UIFont systemFontOfSize:14.0];
        rateLabel.tag = 5;
        rateLabel.backgroundColor = [UIColor clearColor];
        rateLabel.textColor = [UIColor lightTextColor];
        [cell.contentView addSubview:rateLabel];
        [rateLabel release];
    }
    
    //  设置各列值
    NSInteger row = [indexPath row];
    
    SaleAnalyse * item = [self.array objectAtIndex:row];
    
    UILabel * dateLabel = (UILabel *)[cell viewWithTag:1];
    dateLabel.text = item.date;
    
    UILabel * requireLabel = (UILabel *)[cell viewWithTag:2];
    requireLabel.text = [NSString stringWithFormat:@"%.1f", [item.require floatValue]];
    
    UILabel * orderLabel = (UILabel *)[cell viewWithTag:3];
    orderLabel.text = [NSString stringWithFormat:@"%.1f", [item.order floatValue]];
    
    UILabel * cashLabel = (UILabel *)[cell viewWithTag:4];
    cashLabel.text = [NSString stringWithFormat:@"%.1f", [item.cash floatValue]];
    
    UILabel * rateLabel = (UILabel *)[cell viewWithTag:5];
    rateLabel.text = [NSString stringWithFormat:@"%@%%", item.rate];
    
    return cell;
}

#pragma mark - 
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"%@", error);
    [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:@"请求数据发生异常."];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *)response;
    if(([httpURLResponse statusCode] / 100) != 2){
        if (httpURLResponse.statusCode == 500 && connectionTimes < 3) {
            [self.connection cancel];
            [self.connection release];
            self.connection = nil;
            connectionTimes++;
            NSLog(@"第 %d 次连接", connectionTimes);
            [self loadData];
        } else {
            connectionTimes = 0;
            [GlobalUtil showAlertWithTitle:@"请求失败" andMessage:[NSString stringWithFormat:@"网络响应异常：%d", httpURLResponse.statusCode]];
            [self reloadTable];
        }
        NSLog(@"网络响应异常: %@", [[httpURLResponse allHeaderFields] description]);
    }else{
        self.data = [[NSMutableData alloc]init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)aData{
    [self.data appendData:aData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [self parserXML];
}

////  下面两个方法用于降低网络安全性，可以使用https协议
//- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
//    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]; 
//}
//
//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge { 
//    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
//        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge]; 
//    }
//    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge]; 
//}

#pragma mark - 
#pragma mark NSXMLParser Delegate Methods
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    //  xml对象数组开头
    if([elementName isEqualToString:@"Mobiles"]){
        if(self.array == nil){
            self.array = [[NSMutableArray alloc] init];
        }
    }
    //  xml对象
    if([elementName isEqualToString:@"Mobile"]){
        self.tmpSaleAnalyse = [[SaleAnalyse alloc]init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    self.tmpValue = string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    //  根据xml值Key设置各值
    if([elementName isEqualToString:@"amtOrder"]){
        self.tmpSaleAnalyse.cash = self.tmpValue;
    }else if([elementName isEqualToString:@"bkrTime"]){
        self.tmpSaleAnalyse.date = self.tmpValue;
    }else if([elementName isEqualToString:@"brandId"]){
        self.tmpSaleAnalyse.brandId = self.tmpValue;
    }else if([elementName isEqualToString:@"brandName"]){
        self.tmpSaleAnalyse.brandName = self.tmpValue;
    }else if([elementName isEqualToString:@"qtyDemand"]){
        self.tmpSaleAnalyse.require = self.tmpValue;
    }else if([elementName isEqualToString:@"qtyOrder"]){
        self.tmpSaleAnalyse.order = self.tmpValue;
    }else if([elementName isEqualToString:@"satisfaction"]){
        self.tmpSaleAnalyse.rate = self.tmpValue;
        //  添加对象进数组，由于xml传回的数组格式中satisfaction是最后一项，因此在这里添加
        [self.array addObject:self.tmpSaleAnalyse];
        self.tmpValue = nil;
    }
    self.tmpValue = nil;
}


@end
