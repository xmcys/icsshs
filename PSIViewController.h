//
//  进销存分析视图控制器 头文件
//  PSIViewController.h
//  icss-hs
//
//  Created by hsit on 11-9-30.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "BrandStockSale.h"

@class PSIVisibilityViewController;   //  进销存分析 可视列表 视图控制器 类
@class PSISearchViewController;       //  进销存分析 搜索列表 视图控制器 类
@class BrandStockSaleDatabase;        //  进销存分析 数据库操作 类

@interface PSIViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, NSXMLParserDelegate, UIPopoverControllerDelegate> {
    UIView                  * headerFirstView;    //  下拉刷新 图标视图      
    UIView                  * headerSecondView;   //  下拉刷新 文字视图
    UIImageView             * headerImageView;    //  下拉刷新 图片
    UIActivityIndicatorView * headerSpinner;      //  下拉刷新 加载指示器
    UILabel                 * headerLabel;        //  下拉刷新 文字
    UIView                  * footerFirstView;    //  上拉加载 图标视图
    UIView                  * footerSecondView;   //  上拉加载 文字视图
    UIImageView             * footerImageView;    //  上拉加载 图片
    UILabel                 * footerLabel;        //  上拉加载 文字
    UIActivityIndicatorView * footerSpinner;      //  上拉加载 加载指示器
    UIPopoverController     * popoverController;  //  弹出搜索视图控制器（iPad）
    
    PSIVisibilityViewController * visibilityViewController; //  可视列视图控制器 类
    PSISearchViewController     * searchViewController;     //  搜索视图控制器 类
    BrandStockSaleDatabase      * db;                       //  进销存分析数据库操作 类
    
    NSURLConnection * connection;  //  网络连接对象
    NSMutableData   * psiData;     //  网络数据
    NSMutableArray  * psiArray;    //  进销存分析对象数组
    BrandStockSale  * aBrand;      //  进销存分析对象（解析网络数据时用）
    NSString        * aValue;      //  字符串值（解析网络数据时用）
    BOOL    isLoading;             //  标识是否正在加载
    BOOL    isLoadMore;            //  标识是否加载更多
    int     loadRecords;           //  每次实际加载的记录数
    int     pageCount;             //  每次最大加载的记录数
    int     startPage;             //  开始记录 （初始为0或1）
    float   fontSize;              //  字体大小
    int     columnWidth;           //  列宽
    int     columnHeight;          //  列高
    int     connectionTimes;       //  连接次数
    CGFloat scrollStart;           //  开始滚动的位置（用于控制每次滚动的最小值：至少为1列）
    CGRect  unitViewRectShow;      //  单位选择视图大小
    CGRect  unitViewRectHide;      //  单位选择视图大小(隐藏)
    int     unit;                  //  单位标记
}

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * mainSpinner; //  主加载指示器
@property (nonatomic, retain) IBOutlet UIView       * titleView;        //  可滚动列视图
@property (nonatomic, retain) IBOutlet UIView       * unitView;         //  单位选择视图
@property (nonatomic, retain) IBOutlet UILabel      * bottomView;       //  底部周期视图
@property (nonatomic, retain) IBOutlet UIButton     * settingsButton;   //  可视列视图按钮
@property (nonatomic, retain) IBOutlet UIButton     * unitButton;       //  单位选择按钮
@property (nonatomic, retain) IBOutlet UIScrollView * firstScrollView;  //  固定列滚动视图
@property (nonatomic, retain) IBOutlet UIScrollView * secondScrollView; //  可滚动列滚动视图
@property (nonatomic, retain) IBOutlet UITableView  * firstTableView;   //  固定列 表视图
@property (nonatomic, retain) IBOutlet UITableView  * secondTableView;  //  可滚动列 表视图

- (void)loadData;      // 加载数据
- (void)reloadTable;   // 刷新表格
- (void)cancelLoading; // 取消加载
- (void)parseXMLData;  // 解析XML网络数据
- (void)changeRightButtonState:(int)tag;    //  设置导航栏右侧按钮状态 ：1 为取消加载  0 为查询
- (IBAction)clickSettingsButton:(id)sender; //  可视列视图按钮点击事件
- (IBAction)clickSearchButton:(id)sender;   //  导航栏右侧按钮点击事件
- (IBAction)showOrHideUnitView:(id)sender;  //  展开 / 隐藏 单位切换视图
- (IBAction)changeUnit:(id)sender;          //  改变卷烟数量单位


@end
