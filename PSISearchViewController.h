//
//  进销存分析搜索视图控制器
//  PSISearchViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-9.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSIViewController;

@interface PSISearchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource> {
    
    int fieldIndex;  //  记录选中的是哪一行
    NSMutableArray * dateArray;  //  保存年月日数组的数组
}

@property (nonatomic, retain) IBOutlet UIView       * bottomView;  //  表视图的footerView 
@property (nonatomic, retain) IBOutlet UITableView  * searchTable; //  显示搜索框的表视图
@property (nonatomic, retain) IBOutlet UIButton     * btnReset;    //  重置按钮
@property (nonatomic, retain) IBOutlet UIButton     * btnSearch;   //  查询按钮
@property (nonatomic, retain) IBOutlet UIPickerView * datePicker;  //  自定义选取器（日期）
@property (nonatomic, retain) UITextField * startDateField;        //  开始日期控件
@property (nonatomic, retain) UITextField * endDateField;          //  结束日期控件
@property (nonatomic, retain) UITextField * nameField;             //  卷烟名称控件
 
- (IBAction)clickReset;     //  点击重置按钮
- (IBAction)clickSearch;    //  点击搜索按钮
- (void)closeKeyBoard;      //  关闭键盘
- (void)initDateArray;      //  初始化年月日数组
- (void)setDateFieldColor;  //  设置日期的颜色：黑色：普通状态  蓝色：正在编辑   红色：非法日期
- (void)setDateField;       //  根据日期选取器选择的值和年月日数组，设置开始和结束日期
- (void)setPickerValue:(NSString *)string;   //  当点击开始（结束）日期控件时，设置选取器的值

@end
