//
//  进销存分析 类
//  BrandStockSale.h
//  icss-hs
//
//  Created by hsit on 11-10-10.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BrandStockSale : NSObject {
    
}

@property (nonatomic, retain) NSString * brandId;          // 卷烟ID
@property (nonatomic, retain) NSString * brandName;        // 卷烟名称
@property (nonatomic, retain) NSString * qtyStockBegin;    // 期初库存
@property (nonatomic, retain) NSString * qtyStockBeginOriginal; // 原始期初
@property (nonatomic, retain) NSString * qtyStockBuy;      // 购进入库
@property (nonatomic, retain) NSString * qtyStockBuyOriginal; // 原始购进
@property (nonatomic, retain) NSString * qtyStockDraw;     // 领货出库
@property (nonatomic, retain) NSString * qtyStockDrawOriginal; // 原始领货
@property (nonatomic, retain) NSString * qtyStockEnd;      // 期末库存
@property (nonatomic, retain) NSString * qtyStockEndOriginal; // 原始期末
@property (nonatomic, retain) NSString * qtyStockSale;     // 销售出库
@property (nonatomic, retain) NSString * qtyStockSaleOriginal; // 原始销售
@property (nonatomic, retain) NSString * qtyStockTranOut;  // 移库出库
@property (nonatomic, retain) NSString * qtyStockTranOutOriginal; // 原始移库
@property (nonatomic, retain) NSString * qtyStockWaste;    // 本期溢耗
@property (nonatomic, retain) NSString * qtyStockWasteOriginal; // 原始溢耗
@property (nonatomic, retain) NSString * qtyStockWithdraw; // 退货入库
@property (nonatomic, retain) NSString * qtyStockWithdrawOriginal; // 原始退货

//  将所有属性封装成NSString输出
-(NSString *)toString;

@end
