//
//  分类柱图视图控制器
//  RTBarChartViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "RTBarChartViewController.h"
#import "ClassifySales.h"

@implementation RTBarChartViewController

@synthesize graphView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [requireAnnotation release];
    [orderAnnotation release];
    [graph release];
    [requireAxis release];
    [orderAxis release];
    [graphView release];
    [super dealloc];
}


- (void)viewDidUnload
{
    requireAnnotation = nil;
    orderAnnotation = nil;
    graph = nil;
    requireAxis = nil;
    orderAxis = nil;
    graphView = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  设置坐标数组  参数 ： 销售分析对象数组
- (void)setAxisDatas:(NSArray *)array{
    //  初始化各数组
    if (requireAxis == nil) {
        requireAxis = [[NSMutableArray alloc] init];
    } else {
        [requireAxis removeAllObjects];
    }
    
    if (orderAxis == nil) {
        orderAxis = [[NSMutableArray alloc] init];
    } else {
        [orderAxis removeAllObjects];
    }
    
    double maxY = 0.0;
    
    for(int i = 1; i < [array count] - 1; i++){
        ClassifySales * classify = [array objectAtIndex:i];
        
        //  设置需求量坐标数组
        id x1 = [NSDecimalNumber numberWithDouble:i];
        id y1 = [NSDecimalNumber numberWithDouble:[classify.demand doubleValue] / 50];
        [requireAxis addObject:[NSDictionary dictionaryWithObjectsAndKeys:x1, @"x", y1, @"y", nil]];
        
        //  设置订单量坐标数组
        id x2 = [NSDecimalNumber numberWithDouble:i];
        id y2 = [NSDecimalNumber numberWithDouble:[classify.order doubleValue] / 50];
        [orderAxis addObject:[NSDictionary dictionaryWithObjectsAndKeys:x2, @"x", y2, @"y", nil]];
        
        double aMax = [classify.demand doubleValue] / 50 > [classify.order doubleValue] / 50 ? [classify.demand doubleValue] / 50 : [classify.order doubleValue] / 50;
        maxY = aMax > maxY ? aMax : maxY;
    }
    
    //  设置X、Y轴最大值
    yFloat = maxY * 1.15;
    xFloat = 5;
}

- (void)drawBarChart{
    
    if (graph != nil) {
        //  刷新数据
        [graph release];
        graph = nil;
        
        if (requireAnnotation != nil) {
            [requireAnnotation release];
            requireAnnotation = nil;
        }
        
        if (orderAnnotation != nil) {
            [orderAnnotation release];
            orderAnnotation = nil;
        }
        
        for (UIView * subView in self.graphView.subviews) {
            [subView removeFromSuperview];
        }
    }
    
    // 创建CPTXYGraph对象，窗体大小为0
    graph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    
    // 为graph应用暗色主题
    CPTTheme * theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [graph applyTheme:theme];
    
    // 将视图上用于显示图形的UIView控件转换为CPTGraphHostingView类型，并将graph加入其中
    CPTGraphHostingView * hostingView = [[[CPTGraphHostingView alloc] initWithFrame:self.graphView.bounds] autorelease];
    [hostingView setHostedGraph:graph];
    [self.graphView addSubview:hostingView];
    
    // 设置绘图区边框及（坐标）边界
    //    graph.plotAreaFrame.borderLineStyle = nil;
    graph.plotAreaFrame.cornerRadius = 10.0f;
    graph.plotAreaFrame.paddingTop = 20.0f;
    graph.plotAreaFrame.paddingBottom = 60.0f;
    graph.plotAreaFrame.paddingRight = 10.0f;
    graph.plotAreaFrame.paddingLeft = 60.0f;
    
    // 设置graph距离hostingView的边界
    graph.paddingTop = 5.0f;
    graph.paddingBottom = 5.0f;
    graph.paddingLeft = 5.0f;
    graph.paddingRight = 5.0f;
    
    // 设置标题样式
    CPTMutableTextStyle * textStyle = [CPTMutableTextStyle textStyle];
    textStyle.fontSize = 16.0f;
    textStyle.color = [CPTColor cyanColor];
    graph.title = @"需求/订单量柱形图 (件)";
    graph.titleTextStyle = textStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0f, -20.0f); // 以TOP而言, -20f
    
    // 设置绘图空间
    CPTXYPlotSpace * plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(xFloat + 1)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(yFloat)];
    
    // 设置线样式
    CPTMutableLineStyle * requirePlotLineStyle = [CPTMutableLineStyle lineStyle];
    requirePlotLineStyle.lineWidth = 2.0f;
    requirePlotLineStyle.lineColor = [[CPTColor blueColor] colorWithAlphaComponent:1];
    CPTMutableLineStyle * orderPlotLineStyle = [CPTMutableLineStyle lineStyle];
    orderPlotLineStyle.lineWidth = 2.0f;
    orderPlotLineStyle.lineColor = [[CPTColor redColor] colorWithAlphaComponent:1];
    
    // 设置坐标轴
    CPTXYAxisSet * axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    CPTXYAxis * xAxis = axisSet.xAxis;
    // 设定x轴坐标显示的文字为空（为了自定义）
    xAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
    // x轴标题显示位置，以PlotSpace的xRange值为基础
    xAxis.titleLocation = CPTDecimalFromFloat(xFloat / 2);
    xAxis.titleOffset = 20;
    // x轴大刻度的长度
    xAxis.majorTickLength = 5;
    // 大刻度的间隔
    xAxis.majorIntervalLength = CPTDecimalFromFloat(xFloat);
    // 不显示小刻度
    xAxis.minorTickLineStyle = nil;
    
    
    // 设置y轴样式
    CPTXYAxis * yAxis = axisSet.yAxis;
    yAxis.titleLocation = CPTDecimalFromFloat(yFloat / 2);
    yAxis.majorIntervalLength = CPTDecimalFromFloat(yFloat / 5);
    yAxis.majorTickLength = 5;
    yAxis.minorTickLineStyle = nil;
    
    //  定义x轴文字样式
    CPTMutableTextStyle * axisTextStyle = [[[CPTMutableTextStyle alloc] init] autorelease];
    axisTextStyle.color = [CPTColor whiteColor];
    axisTextStyle.fontSize = 10.0;
    
    //  定义x轴各坐标点文字
    NSMutableSet * newAxisSet = [[[NSMutableSet alloc] init] autorelease];
    CPTAxisLabel * axisLabel1 = [[[CPTAxisLabel alloc] initWithText:@"一类烟" textStyle:axisTextStyle] autorelease];
    //  设置标签对应的x轴值
    axisLabel1.tickLocation = CPTDecimalFromInt(1);
    //  设置偏移量
    axisLabel1.offset = 5;
    [newAxisSet addObject:axisLabel1];
    CPTAxisLabel * axisLabel2 = [[[CPTAxisLabel alloc] initWithText:@"二类烟" textStyle:axisTextStyle] autorelease];
    axisLabel2.tickLocation = CPTDecimalFromInt(2);
    axisLabel2.offset = 5;
    [newAxisSet addObject:axisLabel2];
    CPTAxisLabel * axisLabel3 = [[[CPTAxisLabel alloc] initWithText:@"三类烟" textStyle:axisTextStyle] autorelease];
    axisLabel3.tickLocation = CPTDecimalFromInt(3);
    axisLabel3.offset = 5;
    [newAxisSet addObject:axisLabel3];
    CPTAxisLabel * axisLabel4 = [[[CPTAxisLabel alloc] initWithText:@"四类烟" textStyle:axisTextStyle] autorelease];
    axisLabel4.tickLocation = CPTDecimalFromInt(4);
    axisLabel4.offset = 5;
    [newAxisSet addObject:axisLabel4];
    CPTAxisLabel * axisLabel5 = [[[CPTAxisLabel alloc] initWithText:@"五类烟" textStyle:axisTextStyle] autorelease];
    axisLabel5.tickLocation = CPTDecimalFromInt(5);
    axisLabel5.offset = 5;
    [newAxisSet addObject:axisLabel5];
    xAxis.axisLabels = newAxisSet;
    
    //  定义需求量柱图
    CPTBarPlot * requireBarPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
    requireBarPlot.identifier = @"需求量";
    requireBarPlot.barWidth = CPTDecimalFromFloat(0.5f);
    requireBarPlot.dataSource = self;
    requireBarPlot.delegate = self;
    requireBarPlot.barCornerRadius = 5.0f;
    [graph addPlot:requireBarPlot];
    
    //  定义订单量柱图
    CPTBarPlot * orderBarPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor greenColor] horizontalBars:NO];
    orderBarPlot.identifier = @"订单量";
    orderBarPlot.barWidth = CPTDecimalFromFloat(0.5f);
    orderBarPlot.dataSource = self;
    orderBarPlot.delegate = self;
    //  设置柱图偏移量
    orderBarPlot.barOffset = CPTDecimalFromFloat(0.25);
    orderBarPlot.barCornerRadius = 5.0f;
    [graph addPlot:orderBarPlot];
    
    //  定义图注边框样式
    CPTMutableLineStyle * legendLineStyle = [[[CPTMutableLineStyle alloc] init] autorelease];
    legendLineStyle.lineWidth = 2.0;
    legendLineStyle.lineColor = [CPTColor blueColor];
    
    //  定义图注文字样式
    CPTMutableTextStyle * legendTextStyle = [[[CPTMutableTextStyle alloc] init] autorelease];
    legendTextStyle.color = [CPTColor whiteColor];
    legendTextStyle.fontSize = 14.0;
    
    //  定义图注
    CPTLegend * legend = [CPTLegend legendWithGraph:graph];
    //  图注列数
    legend.numberOfColumns = 2;
    legend.cornerRadius = 5.0f;
    //  右侧图示大小
    legend.swatchSize = CGSizeMake(20, 20);
    legend.borderLineStyle = legendLineStyle;
    legend.textStyle = legendTextStyle;
    
    //  图注位置
    NSArray * legendPoint = [NSArray arrayWithObjects: [NSNumber numberWithInt:0], [NSNumber numberWithInt:80], nil];
    //  图注注释
    CPTPlotSpaceAnnotation * legendAnnotation = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:plotSpace anchorPlotPoint:legendPoint];
    legendAnnotation.contentLayer = legend;
    //  显示位置 ， 相对于坐标轴
    legendAnnotation.displacement = CGPointMake(90, -45);
    
    [graph.plotAreaFrame.plotArea addAnnotation:legendAnnotation];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Plot Data Source Methods
//  x轴数据量
- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot{
    return 5;
}

//  绘制曲线各点
- (NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index{
    NSNumber *num = nil;
	if (fieldEnum == CPTBarPlotFieldBarLocation) {
		// location  x轴值
        num = [[requireAxis objectAtIndex:index] objectForKey:@"x"];
	}
	else if (fieldEnum == CPTBarPlotFieldBarTip) {
		// length   y轴值
		if ([plot.identifier isEqual:@"需求量"]) {
			num = [[requireAxis objectAtIndex:index] objectForKey:@"y"];
		}
		else {
			num = [[orderAxis objectAtIndex:index] objectForKey:@"y"];
		}
        
	}
    return num;
}

#pragma mark - 
#pragma mark CPTBarPlot Delegate Methods
- (void)barPlot:(CPTBarPlot *)plot barWasSelectedAtRecordIndex:(NSUInteger)index{
    //  需求量 移除上一个被点击的折点的点标签
    if (requireAnnotation) {
        [graph.plotAreaFrame.plotArea removeAnnotation:requireAnnotation];
        [requireAnnotation release];
        requireAnnotation = nil;
    }
    //  订单量 移除上一个被点击的折点的点标签
    if(orderAnnotation){
        [graph.plotAreaFrame.plotArea removeAnnotation:orderAnnotation];
        [orderAnnotation release];
        orderAnnotation = nil;
    }
    
    //  定义文字标签样式
    CPTMutableTextStyle * annotationTextStyle = [CPTMutableTextStyle textStyle];
    annotationTextStyle.color = [CPTColor yellowColor];
    annotationTextStyle.fontSize = 14.0f;

    //  需求量 获取点击的折点的坐标值
    NSNumber * x1 = [[requireAxis objectAtIndex:index] valueForKey:@"x"];
    NSNumber * y1 = [[requireAxis objectAtIndex:index] valueForKey:@"y"];
    //  订单量 获取点击的折点的坐标值
    NSNumber * x2 = [[orderAxis objectAtIndex:index] valueForKey:@"x"];
    NSNumber * y2 = [[orderAxis objectAtIndex:index] valueForKey:@"y"];
    //  将坐标值封装成NSArray格式
    NSArray  * array1 = [NSArray arrayWithObjects:x1, y1, nil];
    NSArray  * array2 = [NSArray arrayWithObjects:x2, y2, nil];
    
    //  将y值转为字符串
    NSNumberFormatter * formater = [[[NSNumberFormatter alloc] init] autorelease];
    [formater setMaximumFractionDigits:2];
    NSString * y1String  = [formater stringFromNumber:y1];
    NSString * y2String  = [formater stringFromNumber:y2];
    
    //  需求量 定义文字标签
    CPTTextLayer * textLayer1 = [[[CPTTextLayer alloc] initWithText:y1String style:annotationTextStyle] autorelease];
    //  订单量 定义文字标签
    CPTTextLayer * textLayer2 = [[[CPTTextLayer alloc] initWithText:y2String style:annotationTextStyle] autorelease];
    //  需求量 定义点标签，将文字标签加入点标签
    requireAnnotation = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:graph.defaultPlotSpace anchorPlotPoint:array1];
    requireAnnotation.contentLayer = textLayer1;
    //  设置点标签相对于改坐标点的偏移量
    requireAnnotation.displacement = CGPointMake(0.0f, 15.0f);
    [graph.plotAreaFrame.plotArea addAnnotation:requireAnnotation];
    //  订单量 定义点标签，将文字标签加入点标签 
    orderAnnotation = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:graph.defaultPlotSpace anchorPlotPoint:array2];
    orderAnnotation.contentLayer = textLayer2;
    //  设置点标签相对于改坐标点的偏移量
    orderAnnotation.displacement = CGPointMake(0.0f, -15.0f);
    [graph.plotAreaFrame.plotArea addAnnotation:orderAnnotation];
}

@end
