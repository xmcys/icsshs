//
//  周边搜索视图控制器
//  CircumSearchViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-18.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CircumSearchListViewController.h"
#import "CircumSearchMapViewController.h"
#import "CustomerDetailViewController.h"

@class Customer;   //  客户信息

@interface CircumSearchViewController : UIViewController <UITabBarDelegate, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, NSXMLParserDelegate, MKReverseGeocoderDelegate, CircumSearchListViewDelegate, CircumSearchMapViewDelegate>{
    NSMutableData                   * customersData;           //  客户信息网络数据
    NSMutableArray                  * customersArray;          //  客户信息数组
    NSURLConnection                 * connection;              //  网络连接对象
    CircumSearchListViewController  * listViewController;      //  客户列表视图  
    CircumSearchMapViewController   * mapViewController;       //  地图模式视图
    Customer                        * customer;                //  客户信息对象
    NSString                        * aValue;                  //  临时字符串（解析XML用）
    MKReverseGeocoder               * reverseGeocode;          //  反向解析地理位置
    NSIndexPath                     * lastIndexPath;           //  最后选中的精度范围
    CustomerDetailViewController    * detailViewController;    //  客户信息详情视图控制器(用于iPad)
    
    CLLocationCoordinate2D  coordinate;         //  用户坐标
    int                     currentViewTag;     //  标识显示的是列表模式还是地图模式
    int                     startPage;          //  开始数据位置
    int                     pageSize;           //  每次加载最大数据量
    int                     loadCounts;         //  每次加载实际数据量
    int                     connectionTimes;    //  连接次数
    float                   distance;           //  搜索距离
    BOOL                    isLoading;          //  判断是否正在加载数据
    BOOL                    isOverload;         //  判断是否下拉刷新
}

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView  * spinner;    //  加载指示器
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView  * locSpinner; //  定位加载指示器
@property (nonatomic, retain) IBOutlet UIView           * locatingView;    //  底部显示【正在定位】的视图
@property (nonatomic, retain) IBOutlet UIView           * contentView;     //  内容视图
@property (nonatomic, retain) IBOutlet UIView           * aListView;       //  列表视图（iPad）
@property (nonatomic, retain) IBOutlet UIView           * aDetailView;     //  详情视图（iPad）
@property (nonatomic, retain) IBOutlet UIView           * aMapView;        //  地图视图（iPad）
@property (nonatomic, retain) IBOutlet UITabBar         * tabBar;          //  选项卡栏
@property (nonatomic, retain) IBOutlet UITableView      * searchTable;     //  搜索表视图
@property (nonatomic, retain) IBOutlet UILabel          * locationLabel;   //  当前位置
@property (nonatomic, retain) CLLocationManager         * locationMgr;     //  定位对象

//  改变显示的视图 0 列表模式  1 地图模式
- (void)changeDisplayView:(int)tag;

//  显示或隐藏搜索表视图
- (IBAction)showOrHideSearchView;

//  更新列表视图和地图视图
- (void)reloadTableAndMap;

//  加载客户信息
- (void)loadCustomerData;

//  解析XML数据
- (void)parseXMLData;

//  重新定位
- (IBAction)reLocation;

@end
