//
//  销售分析视图控制器 类
//  SalesAnalyseViewController.h
//  icss-hs
//  Created by hsit on 11-9-21.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SaleAnalyse.h"


@class SalesAnalyseDetailViewController;  //  单个卷烟销售分析详情视图控制器 类
@class SalesAnalyseDatabase;              //  销售分析数据库操作 类

@interface SalesAnalyseViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIScrollViewDelegate, NSXMLParserDelegate> {
    CGRect searchViewPopUpRect;   //  显示查询窗体的大小
    CGRect searchViewHideRect;    //  隐藏查询窗体的大小
    BOOL searchViewIsHide;        //  标识查询窗体是否隐藏
    BOOL isLoading;               //  标识是否正在加载
    BOOL isOverLoad;              //  标识是否重载数据
    int searchTableRow;           //  标识选中的查询窗体中的列
    int startRecord;              //  开始记录
    int loadRecord;               //  每次加载记录数
    int pageRecords;              //  每次最大加载记录数
    SalesAnalyseDatabase * db;    //  数据库操作类
    int columnWidth;              //  列宽
    int columnHeight;             //  列高
    int connectionTimes;          //  连接次数
    float fontSize;               //  字体大小
}

@property (nonatomic, retain) IBOutlet UILabel        * bottomLabel;          //  底部显示查询周期
@property (nonatomic, retain) IBOutlet UITableView    * salesAnalyseTable;    //  数据表视图
@property (nonatomic, retain) IBOutlet UITableView    * searchTable;          //  查询表视图
@property (nonatomic, retain) IBOutlet UIView         * searchView;           //  查询窗体
@property (nonatomic, retain) IBOutlet UIView         * searchButtonView;     //  查询表视图的footer
@property (nonatomic, retain) IBOutlet UIPickerView   * datePickerView;       //  自定义日期选取器
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * mainSpinner; //  主加载指示器
@property (nonatomic, retain) UIView                  * headerView;           //  下拉刷新 视图
@property (nonatomic, retain) UILabel                 * headerLabel;          //  下拉刷新 文本
@property (nonatomic, retain) UIImageView             * headerImage;          //  下拉刷新 箭头
@property (nonatomic, retain) UIActivityIndicatorView * headerSpinner;        //  下拉刷新 指示器
@property (nonatomic, retain) UIView                  * footerView;           //  上拉加载 视图
@property (nonatomic, retain) UILabel                 * footerLabel;          //  上拉加载 文本
@property (nonatomic, retain) UIImageView             * footerImage;          //  上拉加载 箭头
@property (nonatomic, retain) UIActivityIndicatorView * footerSpinner;        //  上拉加载 指示器
@property (nonatomic, retain) NSMutableData           * salesAnalyseData;     //  网络数据
@property (nonatomic, retain) NSMutableArray          * salesAnalyseArray;    //  销售分析对象数组
@property (nonatomic, retain) NSArray                 * yearArray;            //  年份数组
@property (nonatomic, retain) NSArray                 * monthArray;           //  月份数组
@property (nonatomic, retain) NSString                * tmpValue;             //  临时值（解析XML）
@property (nonatomic, retain) SaleAnalyse             * salesAnalyse;         //  销售分析 对象
@property (nonatomic, retain) UITextField             * nameField;            //  卷烟名称 控件
@property (nonatomic, retain) UITextField             * startDateField;       //  开始日期 控件
@property (nonatomic, retain) UITextField             * endDateField;         //  结束日期 控件
@property (nonatomic, retain) NSMutableURLRequest     * request;              //  请求对象
@property (nonatomic, retain) NSURLConnection         * connection;           //  连接对象
@property (nonatomic, retain) SalesAnalyseDetailViewController * detailController; //  详情视图
@property (nonatomic, retain) IBOutlet UILabel        * firstTitleLabel;      //  第一个标题
@property (nonatomic, retain) IBOutlet UILabel        * brandNameTitleLabel;  //  卷烟名称标题(iPad)
@property (nonatomic, retain) IBOutlet UIView         * firstBottomView;      //  销售分析底层(iPad)
@property (nonatomic, retain) IBOutlet UIView         * secondBottomView;     //  单个卷烟底层(iPad)

//  显示或隐藏查询窗体
- (IBAction)showSearchView;   

//  重置查询窗体各控件
- (IBAction)resetSearchView;  

//  点击搜索按钮
- (IBAction)clickSearch;      

//  关闭键盘
- (void)closeKeyBoard;        

//  初始化查询周期  tag : 0 开始日期 1 结束日期 
- (NSString *)initSearchDate:(NSInteger)tag;

//  设置日期选取器的值
- (void)setPickerSelectionWithDate:(NSString *)dateString;

//  初始化下拉刷新视图
- (void)initHeaderView;

//  初始化上拉加载视图
- (void)initFooterView;

//  加载数据
- (void)loadData;

//  重载表视图
- (void)reloadTable;

//  解析xml
- (void)parserXMLData;

//  重设导航栏右侧按钮状态  tag : 0 显示查询窗体  1  取消加载
- (void)changeButtonState:(int)tag;

//  错误日期格式提示
- (BOOL)showDateAlert;

//  设置日期的颜色：黑色：普通状态  蓝色：正在编辑   红色：非法日期
- (void)setDateFieldColor;  

//  取消加载
- (void)cancelLoading;

@end
