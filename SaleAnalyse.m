//
//  销售分析类
//  SaleAnalyse.m
//  icss-hs
//
//  Created by hsit on 11-9-26.
//  Copyright 2011年 厦门中软海晟信息技术有限公司. All rights reserved.
//

#import "SaleAnalyse.h"

@implementation SaleAnalyse

@synthesize brandId;    //  卷烟id
@synthesize brandName;  //  卷烟名称
@synthesize date;       //  日期
@synthesize require;    //  需求量
@synthesize rate;       //  满足率
@synthesize order;      //  订单量
@synthesize cash;       //  金额

- (void)dealloc{
    [brandId release];
    [brandName release];
    [date release];
    [require release];
    [rate release];
    [order release];
    [cash release];
    [super dealloc];
}

// 将所有属性封装成NSString输出
- (NSString *)toString{
    return [NSString stringWithFormat:@"BrandId: %@\nBrandName: %@\nDate: %@\nRequire: %@\nrate: %@\nOrder: %@\nCash: %@", brandId, brandName, date, require, rate, order, cash];
}

@end
