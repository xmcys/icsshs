//
//  客户列表视图控制器
//  CircumSearchListViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-19.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CircumSearchViewController;    //  周边搜索视图控制器
@class LoadMoreFooterView;            //  下拉刷新视图
@class RefreshHeaderView;             //  上拖加载视图

//  自定义客户信息列表协议
@protocol CircumSearchListViewDelegate <NSObject>

@optional

//  显示客户信息详情
- (void)showCustomerDetailView:(int)index; 

//  上拖加载更多数据
- (void)loadMoreCustomers;

//  下拉刷新
- (void)loadCustomers;

@end
@interface CircumSearchListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate> {
    NSArray * customersArray;
    LoadMoreFooterView * footerView;   //  上拖加载视图
    
    int                  loadCounts;   //  每次实际加载数据量
    int                  pageSize;     //  每次最大加载数据量
    BOOL                 isLoading;    //  判断是否正在加载
}

@property (nonatomic, assign) id<CircumSearchListViewDelegate>   delegate;         //  客户信息列表协议
@property (nonatomic, retain) IBOutlet UITableView             * customersTable;   //  客户表视图
@property (nonatomic, retain) RefreshHeaderView                * headerView;       //  下拉刷新视图

//  设置客户信息并更新表视图
- (void)setCustomersArray:(NSArray *)array loadCounts:(int)counts pageSize:(int)size;

//  设置是否正在加载
- (void)setIsLoading:(BOOL)loading;

@end
