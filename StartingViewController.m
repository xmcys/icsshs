//
//  启动视图控制器
//  StartingViewController.m
//  icss-hs
//
//  Created by hsit on 11-9-21.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "StartingViewController.h"
#import "icss_hsAppDelegate.h"
#import "GlobalUtil.h"
#import "Database.h"

@implementation StartingViewController

@synthesize activityIndicatorView;  // 指示器
@synthesize checkButton;            //  重新检测 按钮

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    [activityIndicatorView release];
    [checkButton release];
    [super dealloc];
}

- (void)viewDidUnload
{
    self.activityIndicatorView = nil;
    self.checkButton = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

// 重新检测 按钮事件
- (IBAction)checkAgain:(id)sender{
    self.checkButton.hidden = YES;
    [self.activityIndicatorView startAnimating];
    //  延迟1.5秒执行prepareToStart，为了有个动画过渡效果
    [self performSelector:@selector(prepareToStart) withObject:nil afterDelay:1.5];
}

// 检测网络并加载各项数据
- (void) prepareToStart{
    //  判断网络
    BOOL isReachable = [GlobalUtil isReachable];
    if(!isReachable){
        [GlobalUtil showAlertWithTitle:@"网络故障" andMessage:@"当前网络不可用"];
        self.checkButton.hidden = NO;
        [self.activityIndicatorView stopAnimating];
        return;
    }
    
    //  定义数据库对象
    Database * database = [[Database alloc] init];
    //  打开数据库
    [database openDatabase];
    //  将全局应用程序协议中的数据库对象设为改对象
    [icss_hsAppDelegate appDelegate].database = database;
    [database release];
    
    [self.activityIndicatorView stopAnimating];
    //  移除启动视图控制器，进入菜单视图控制器
    [[icss_hsAppDelegate appDelegate] removeStartViewAddMenuView];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([GlobalUtil isIphone]) {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iphone_bg.jpg"]];
    } else {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ipad_bg.jpg"]];
    }
    
    [self.activityIndicatorView startAnimating];
    self.checkButton.hidden = YES;
    // 延迟1秒后，执行prepareToStart事件
    [self performSelector:@selector(prepareToStart) withObject:nil afterDelay:1.5];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
