//
//  客户信息视图控制器
//  CustomerDetailViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-19.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "CustomerDetailViewController.h"
#import "Customer.h"
#import "GlobalUtil.h"


@implementation CustomerDetailViewController

@synthesize customerTable;  //  客户表视图

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [customerTable release];
    [super dealloc];
}

- (void)viewDidUnload
{
    customerTable = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  设置客户信息
- (void)setCustomer:(Customer *)aCustomer{
    customer = aCustomer;
    [customerTable reloadData];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"客户信息";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        //  除距离
        return [Customer propertyCount] - 1;
    } else {
        //  距离
        return 1;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"基本信息";
    } else {
        return @"位置信息";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * identier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identier];
    NSUInteger row = [indexPath row];
    NSUInteger section = [indexPath section];
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identier] autorelease];
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
        if (section == 0) {
            if (row == 0) {
                //  许可证号
                UILabel * licenceLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 80, 35)];
                licenceLabel.text = @"许可证号:";
                licenceLabel.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:licenceLabel];
                [licenceLabel release];
                
                UILabel * licenceValue = [[UILabel alloc] initWithFrame:CGRectMake(100, 2, 190, 35)];
                licenceValue.tag = 1;
                licenceValue.font = [UIFont systemFontOfSize:14.0];
                licenceValue.minimumFontSize = 10.0;
                licenceValue.adjustsFontSizeToFitWidth = YES;
                licenceValue.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:licenceValue];
                [licenceValue release];
            } else if (row == 1) {
                //  客户名称
                UILabel * nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 80, 35)];
                nameLabel.text = @"客户名称:";
                nameLabel.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:nameLabel];
                [nameLabel release];
                
                UILabel * nameValue = [[UILabel alloc] initWithFrame:CGRectMake(100, 2, 190, 35)];
                nameValue.tag = 1;
                nameValue.font = [UIFont systemFontOfSize:14.0];
                nameValue.minimumFontSize = 10.0;
                nameValue.adjustsFontSizeToFitWidth = YES;
                nameValue.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:nameValue];
                [nameValue release];
            } else if (row == 2) {
                //  客户地址
                UILabel * addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 80, 35)];
                addressLabel.text = @"客户地址:";
                addressLabel.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:addressLabel];
                [addressLabel release];
                
                UILabel * addressValue = [[UILabel alloc] initWithFrame:CGRectMake(100, 2, 190, 35)];
                addressValue.tag = 1;
                addressValue.font = [UIFont systemFontOfSize:14.0];
                addressValue.minimumFontSize = 10.0;
                addressValue.adjustsFontSizeToFitWidth = YES;
                addressValue.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:addressValue];
                [addressValue release];
            } else if (row == 3) {
                //  客户电话
                UILabel * phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 80, 35)];
                phoneLabel.text = @"客户电话:";
                phoneLabel.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:phoneLabel];
                [phoneLabel release];
                
                UILabel * phoneValue = [[UILabel alloc] initWithFrame:CGRectMake(100, 2, 190, 35)];
                phoneValue.tag = 1;
                phoneValue.font = [UIFont systemFontOfSize:14.0];
                phoneValue.minimumFontSize = 10.0;
                phoneValue.adjustsFontSizeToFitWidth = YES;
                phoneValue.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:phoneValue];
                [phoneValue release];
            } else if (row == 4) {
                //  订货日
                UILabel * orderDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 80, 35)]; 
                orderDateLabel.text = @"订  货  日:";
                orderDateLabel.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:orderDateLabel];
                [orderDateLabel release];
                
                UILabel * orderDateValue = [[UILabel alloc] initWithFrame:CGRectMake(100, 2, 190, 35)];
                orderDateValue.tag = 1;
                orderDateValue.font = [UIFont systemFontOfSize:14.0];
                orderDateValue.minimumFontSize = 10.0;
                orderDateValue.adjustsFontSizeToFitWidth = YES;
                orderDateValue.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:orderDateValue];
                [orderDateValue release];
            } else if (row == 5) {
                //  客户经理
                UILabel * mgrNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 80, 35)];
                mgrNameLabel.text = @"客户经理:";
                mgrNameLabel.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:mgrNameLabel];
                [mgrNameLabel release];
                
                UILabel * mgrNameValue = [[UILabel alloc] initWithFrame:CGRectMake(100, 2, 190, 35)];
                mgrNameValue.tag = 1;
                mgrNameValue.font = [UIFont systemFontOfSize:14.0];
                mgrNameValue.minimumFontSize = 10.0;
                mgrNameValue.adjustsFontSizeToFitWidth = YES;
                mgrNameValue.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:mgrNameValue];
                [mgrNameValue release];
            } else if (row == 6) {
                //  客户星级
                UILabel * levelLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 80, 35)];
                levelLabel.text = @"客户星级:";
                levelLabel.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:levelLabel];
                [levelLabel release];
                //  设置星级为5颗灰色星
                for (int i = 0; i < 5; i++) {
                    UIImageView * levelStarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30 * i + 100, 4, 30, 30)];
                    [levelStarImageView setImage:[UIImage imageNamed:@"star_empty.png"]];
                    levelStarImageView.tag = i + 1;
                    [cell.contentView addSubview:levelStarImageView];
                    [levelStarImageView release];
                }
                //  星级类别。 4星和5星都分成2类。
                UILabel * classifyValue = [[UILabel alloc] initWithFrame:CGRectMake(260, 2, 30, 35)];
                classifyValue.tag = 6;
                classifyValue.font = [UIFont systemFontOfSize:14.0];
                classifyValue.minimumFontSize = 10.0;
                classifyValue.adjustsFontSizeToFitWidth = YES;
                classifyValue.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:classifyValue];
                [classifyValue release];
            }
        } else {
            //  距离当前
            UILabel * distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 80, 35)];
            distanceLabel.text = @"距离当前:";
            distanceLabel.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:distanceLabel];
            [distanceLabel release];
            
            UILabel * distanceValue = [[UILabel alloc] initWithFrame:CGRectMake(100, 2, 190, 35)];
            distanceValue.tag = 1;
            distanceValue.font = [UIFont systemFontOfSize:14.0];
            distanceValue.minimumFontSize = 10.0;
            distanceValue.adjustsFontSizeToFitWidth = YES;
            distanceValue.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:distanceValue];
            [distanceValue release];
        }
    }
    if (section == 0) {
        if (row == 0) {
            UILabel * licenceValue = (UILabel *)[cell viewWithTag:1];
            licenceValue.text = customer.licenceCode;
        } else if (row == 1) {
            UILabel * nameValue = (UILabel *)[cell viewWithTag:1];
            nameValue.text = customer.name;
        } else if (row == 2) {
            UILabel * addressValue = (UILabel *)[cell viewWithTag:1];
            addressValue.text = customer.address;
        } else if (row == 3) {
            UILabel * phoneValue = (UILabel *)[cell viewWithTag:1];
            phoneValue.text = customer.phone;
        } else if (row == 4) {
            UILabel * orderDateValue = (UILabel *)[cell viewWithTag:1];
            switch ([customer.orderDate intValue]) {
                case 1:
                    orderDateValue.text = @"星期一";
                    break;
                    
                case 2:
                    orderDateValue.text = @"星期二";
                    break;
                    
                case 3:
                    orderDateValue.text = @"星期三";
                    break;
                    
                case 4:
                    orderDateValue.text = @"星期四";
                    break;
                    
                case 5:
                    orderDateValue.text = @"星期五";
                    break;
                    
                case 6:
                    orderDateValue.text = @"星期六";
                    break;
                    
                case 7:
                    orderDateValue.text = @"星期日";
                    break;
                    
                default:
                    break;
            }
        } else if (row == 5) {
            UILabel * mgrNameValue = (UILabel *)[cell viewWithTag:1];
            mgrNameValue.text = customer.mgrName;
        } else if (row == 6) {
            int level = [[customer.level substringToIndex:1] intValue];
            int classify = [[customer.level substringFromIndex:1] intValue];
            for (int i = 0; i < level; i++) {
                UIImageView * levelStarImageView = (UIImageView *)[cell viewWithTag:(i + 1)];
                [levelStarImageView setImage:[UIImage imageNamed:@"star_full.png"]];
            }
            UILabel * classifyValue = (UILabel *)[cell viewWithTag:6];
            if (classify == 1) {
                classifyValue.text = @"一类";
            } else if (classify == 2){
                classifyValue.text = @"二类";
            }
        }
    } else {
        UILabel * distanceValue = (UILabel *)[cell viewWithTag:1];
        distanceValue.text = [NSString stringWithFormat:@"%@m", customer.distance];
    }
    return cell;
}

#pragma mark - 
#pragma mark Table View Delegate

@end
