//
//  客户地图大头针类
//  CustomerAnnotation.m
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "CustomerAnnotation.h"


@implementation CustomerAnnotation

@synthesize coordinate;  //  大头针经纬度
@synthesize title;       //  大头针标题
@synthesize subtitle;    //  大头针副标题
@synthesize index;       //  在数组中的位置

- (void)dealloc{
    [title release];
    [subtitle release];
    [super dealloc];
}

//  初始化大头针， 参数：大头针经纬度
- (id)initWithCoordinate2D:(CLLocationCoordinate2D) coordinate2D{
    if (self = [super init]) {
        coordinate = coordinate2D;
    }
    return self;
}

@end
