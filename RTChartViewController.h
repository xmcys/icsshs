//
//  分类视图控制器
//  RTChartViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTClassifySalesViewController.h"

@class RTBarChartViewController;   //  柱图视图控制器
@class RTPieChartViewController;   //  饼图视图控制器

@interface RTChartViewController : UIViewController <UIScrollViewDelegate, RTClassifySalesViewDelegate> {
    
}

@property (nonatomic, retain) IBOutlet UIScrollView         * scrollView;     //  可滚动视图
@property (nonatomic, retain) IBOutlet UIPageControl        * pageControl;    //  分页控制器
//  卷烟分类列表视图控制器
@property (nonatomic, retain) RTClassifySalesViewController * classifySalesViewController;
//  柱图视图控制器
@property (nonatomic, retain) RTBarChartViewController      * barChartViewController;
//  饼图视图控制器
@property (nonatomic, retain) RTPieChartViewController      * pieChartViewController;

//  改变显示页
- (IBAction)pageControlChangePage;


@end
