//
//  客户信息视图控制器
//  CustomerDetailViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-19.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Customer;

@interface CustomerDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    Customer * customer;   //  客户信息类
}

@property (nonatomic, retain) IBOutlet UITableView * customerTable;  //  客户表视图

//  设置客户信息
- (void)setCustomer:(Customer *)aCustomer;

@end
