//
//  SettingsViewController.m
//  icss-hs
//
//  Created by hsit on 11-12-12.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "SettingsViewController.h"
#import "icss_hsAppDelegate.h"
#import <QuartzCore/QuartzCore.h>


@implementation SettingsViewController
@synthesize insideNetSiwtch;
@synthesize insideNetBottomView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [self.insideNetSiwtch release];
    [self.insideNetBottomView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)selectInsideNet{
    if (self.insideNetSiwtch.on == YES) {
        [[icss_hsAppDelegate appDelegate] setUseInsideNet:YES];
    } else {
        [[icss_hsAppDelegate appDelegate] setUseInsideNet:NO];
    }
    if ([icss_hsAppDelegate appDelegate].useInsideNet == YES) {
        NSLog(@"使用内网");
    }else {
        NSLog(@"不用内网");
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    self.navigationItem.title = @"系统设置";
    
    self.insideNetBottomView.layer.cornerRadius = 10;
    [self.insideNetSiwtch setOn:[icss_hsAppDelegate appDelegate].useInsideNet];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.insideNetBottomView = nil;
    self.insideNetSiwtch = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
