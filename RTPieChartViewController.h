//
//  分类饼图视图控制器
//  RTPieChartViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface RTPieChartViewController : UIViewController <CPTPlotDataSource, CPTPlotSpaceDelegate, CPTPieChartDelegate> {
    
    CPTGraph         * graph;       //  绘图对象
    NSMutableArray   * orderArray;  //  订单量数组
    float  maxValue;
}

@property (nonatomic, retain) IBOutlet UIView * graphView;  //  绘图区底层视图

//  设置订单量数组
- (void)setOrderArray:(NSArray *)array; 

//  绘制饼图
- (void)drawGraph;

@end
