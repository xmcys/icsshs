//
//  客户列表视图控制器
//  CircumSearchListViewController.m
//  icss-hs
//
//  Created by hsit on 11-10-19.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "CircumSearchListViewController.h"
#import "Customer.h"
#import "LoadMoreFooterView.h"
#import "RefreshHeaderView.h"


@implementation CircumSearchListViewController

@synthesize delegate;         //  客户信息列表协议
@synthesize customersTable;   //  客户表视图
@synthesize headerView;       //  下拉刷新视图

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [customersTable release];
    [customersArray release];
    [footerView release];
    [headerView release];
    [super dealloc];
}

- (void)viewDidUnload
{
    customersArray = nil;
    customersTable = nil;
    footerView = nil;
    headerView = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//  设置客户信息并更新表视图
- (void)setCustomersArray:(NSArray *)array loadCounts:(int)counts pageSize:(int)size{
    customersArray = array;
    [customersTable reloadData];
    
    loadCounts = counts;
    pageSize = size;
    
    if (loadCounts < pageSize) {
        //  已加载全部数据
        [footerView changeArrowAndLabelState:3];
    } else {
        //  上拉加载数据
        [footerView changeArrowAndLabelState:0];
    }
}

//  设置是否正在加载
- (void)setIsLoading:(BOOL)loading{
    isLoading = loading;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    //  初始化下拉刷新视图
    headerView = [[RefreshHeaderView alloc] initWithFrame:CGRectMake(0, -35, self.customersTable.frame.size.width, 35)];
    [self.customersTable addSubview:headerView];
    
    //  初始化上拖加载视图
    footerView = [[LoadMoreFooterView alloc] initWithFrame:CGRectMake(0, 0, self.customersTable.frame.size.width, 35)];
    [self.customersTable setTableFooterView:footerView];
    self.customersTable.separatorColor = [UIColor darkGrayColor];
    
    self.customersTable.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - 
#pragma mark Table View DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [customersArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * identifer = @"cell";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifer];
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifer] autorelease];
        
        //  店名样式
        cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
        cell.textLabel.minimumFontSize = 10.0f;
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.textLabel.textColor = [UIColor orangeColor];
        
        //  地址样式
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12.0f];
        cell.detailTextLabel.minimumFontSize = 10.0f;
        cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
        cell.detailTextLabel.textColor = [UIColor lightTextColor];
        
        //  距离
        UILabel * distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, cell.frame.size.height, cell.frame.size.height - 4)];
        distanceLabel.backgroundColor = [UIColor clearColor];
        distanceLabel.font = [UIFont systemFontOfSize:12.0];
        distanceLabel.textColor = [UIColor lightTextColor];
        distanceLabel.textAlignment = UITextAlignmentRight;
        cell.accessoryView = distanceLabel;
        [distanceLabel release];
    }
    
    NSUInteger row = [indexPath row];
    Customer * customer = [customersArray objectAtIndex:row];
    
    cell.textLabel.text = customer.name;
    cell.detailTextLabel.text = customer.address;
    ((UILabel *)cell.accessoryView).text = [NSString stringWithFormat:@"%@m ", customer.distance];
    
    return cell;
}

#pragma mark - 
#pragma mark Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //  判断协议是否被实现
    if ([delegate respondsToSelector:@selector(showCustomerDetailView:)]) {
        //  显示客户详情
        [delegate showCustomerDetailView:[indexPath row]];
    }
}

#pragma mark - 
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (isLoading) {
        return;
    }
    //  改变下拉刷新视图
    if (scrollView.contentOffset.y < -35) {
        [headerView changeArrowAndLabelState:1];
    } else if (scrollView.contentOffset.y < 0){
        [headerView changeArrowAndLabelState:0];
    }
    CGFloat offset = self.customersTable.contentSize.height - self.customersTable.frame.size.height;
    if (offset < 0) {
        offset = 0;
    }
    //  改变上拉加载视图
    if (loadCounts < pageSize) {
        [footerView changeArrowAndLabelState:3];
    } else if (scrollView.contentOffset.y > 35 + offset) {
        [footerView changeArrowAndLabelState:1];
    } else {
        [footerView changeArrowAndLabelState:0];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (isLoading) {
        return;
    }
    //  上拉加载
    if (scrollView.contentOffset.y < -35) {
        if ([delegate respondsToSelector:@selector(loadCustomers)]) {
            [headerView changeArrowAndLabelState:2];
            self.customersTable.contentInset = UIEdgeInsetsMake(35, 0, 0, 0);
            [delegate loadCustomers];
        }
        return;
    }
    CGFloat offset = self.customersTable.contentSize.height - self.customersTable.frame.size.height;
    if (offset < 0) {
        offset = 0;
    }
    if (loadCounts < pageSize) {
        return;
    }
    //  下拉刷新
    if (scrollView.contentOffset.y > 35 + offset) {
        if ([delegate respondsToSelector:@selector(loadMoreCustomers)]) {
            [footerView changeArrowAndLabelState:2];
            [delegate loadMoreCustomers];
        }
    }
}

@end
