//
//  分类列表视图控制器
//  RTClassifySalesViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RefreshHeaderView;  //  下拉刷新视图
@class ClassifySales;      //  卷烟分类信息类

//  自定义分类协议
@protocol RTClassifySalesViewDelegate <NSObject>

@optional

//  显示柱图
- (void)showBarChartView;

//  显示饼图
- (void)showPieChartView;

@end
@interface RTClassifySalesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate, UIScrollViewDelegate> {
    NSMutableData       * classifyData;  //  网络数据
    NSURLConnection     * connection;    //  网络连接对象
    RefreshHeaderView   * headerView;    //  下拉刷新视图
    ClassifySales       * classify;      //  卷烟分类信息类对象
    NSString            * aValue;        //  临时字符串，用于解析XML
    
    BOOL isLoading;        //  判断是否正在加载
    int  connectionTimes;  //  连接次数
}

@property (nonatomic, assign) id<RTClassifySalesViewDelegate>     delegate;       //  分类视图协议
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView  * spinner;        //  加载指示器
@property (nonatomic, retain) IBOutlet UITableView              * classifyTable;  //  分类表视图
@property (nonatomic, retain) IBOutlet UILabel                  * timeLabel;      //  显示查询时间
@property (nonatomic, retain) NSMutableArray                    * classifyArray;  //  分类数组

//  加载分类数据
- (void)loadClassifyData;

//  取消加载
- (void)cancelLoaing;

//  刷新表视图
- (void)reloadTable;

//  解析XML数据
- (void)parseXMLData;

//  计算各字段总量
- (void)sumClassifyArray;

@end
