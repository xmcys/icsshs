//
//  SettingsViewController.h
//  icss-hs
//
//  Created by hsit on 11-12-12.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SettingsViewController : UIViewController {
    
}

@property (nonatomic, retain) IBOutlet UIView * insideNetBottomView;
@property (nonatomic, retain) IBOutlet UISwitch * insideNetSiwtch;

- (IBAction)selectInsideNet;
@end
