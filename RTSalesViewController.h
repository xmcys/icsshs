//
//  实时订单视图控制器
//  RTSalesViewController.h
//  icss-hs
//
//  Created by hsit on 11-10-20.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTBrandListViewController.h"
#import "RTClassifySalesViewController.h"
#import "RTBarChartViewController.h"
#import "RTPieChartViewController.h"

//  分类控制器
@class RTChartViewController;

@interface RTSalesViewController : UIViewController <UITabBarDelegate, RTBrandListViewDelegate, RTClassifySalesViewDelegate> {
    UIBarButtonItem * rightBarButton;   //  导航栏右侧 查询按钮
    int               currentViewIndex; //  当前选中的tabBar选项卡
}

@property (nonatomic, retain) IBOutlet UIView   * contentView;     //  内容视图
@property (nonatomic, retain) IBOutlet UITabBar * tabBar;          //  选项卡工具条
@property (nonatomic, retain) NSMutableArray    * brandListArray;  //  卷烟对象数组
@property (nonatomic, retain) NSMutableArray    * classifyArray;   //  分类对象数组
//  卷烟列表视图控制器
@property (nonatomic, retain) RTBrandListViewController         *brandListViewController;
//  分类视图控制器
@property (nonatomic, retain) RTChartViewController             * chartViewController;
//  分类信息视图控制器（用于iPad）
@property (nonatomic, retain) RTClassifySalesViewController     * classifyViewController; 
//  柱图视图控制器
@property (nonatomic, retain) RTBarChartViewController          * barChartViewController;
//  饼图视图控制器
@property (nonatomic, retain) RTPieChartViewController          * pieChartViewController;
@property (nonatomic, retain) IBOutlet UIView   * brandView;       //  卷烟视图 （用于iPad）
@property (nonatomic, retain) IBOutlet UIView   * classifyView;    //  分类视图 （用于iPad）
@property (nonatomic, retain) IBOutlet UIView   * barView;         //  柱图视图 （用于iPad）
@property (nonatomic, retain) IBOutlet UIView   * pieView;         //  饼图视图 （用于iPad）

//  更改显示的视图 ： 卷烟列表  分类列表
- (void)changeDisplayView:(int)index;

//  显示或隐藏查询框
- (IBAction)showSearchView;

@end
